using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PerkCategory { Common, Heal, Summon, Magic, BattlePlace, Totem, 
    Warrior,    // Physical
    Order,      // Tank
    Dark,       // Debuff
    Bard,       // Buff
    Wild,       // Survival
    Master      // Crit
}

public enum PerkType { Attribute, Global, Targeted }

public enum PerkTargetType { AllAllyHeroes, AllyHero }

public class Perk
{
    public List<PerkCategory> categories { get; set; }
    public PerkType type { get; set; }
    public PerkTargetType targetType { get; set; }
    public int targetCount { get; set; }

    public List<PerkCondition> conditions { get; set; }

    public BasicHero targetHero { get; set; }

    public string name { get; set; }

    // Attribute perks

    public int value { get; set; }
    public int absoluteValue { get; set; }
    public ValueType valueType { get; set; }
    public CalculateFrom valueFrom { get; set; }
    public Attr attribute { get; set; }

    // -----------------------------------------------------------

    public Perk() {
        categories = new List<PerkCategory>();
        conditions = new List<PerkCondition>();
    }

    public void addCategory(PerkCategory category) {
        categories.Add(category);
    }

    public void addCondition(PerkCondition condition) {
        conditions.Add(condition);
    }

    public Perk copy() {
        Perk copy = new Perk();
        copy.type = type;
        copy.targetType = targetType;
        copy.targetCount = targetCount;
        copy.targetHero = targetHero;
        copy.name = name;
        copy.value = value;
        copy.absoluteValue = absoluteValue;
        copy.valueType = valueType;
        copy.valueFrom = valueFrom;
        copy.attribute = attribute;

        foreach(PerkCategory category in categories) {
            copy.addCategory(category);
        }

        foreach(PerkCondition condition in conditions) {
            copy.addCondition(condition);
        }
        return copy;
    }
}

public enum PerkConditionType { }

public class PerkCondition {


}
