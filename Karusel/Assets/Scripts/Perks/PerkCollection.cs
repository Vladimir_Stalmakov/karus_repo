using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PerkCollection {

    private static List<Perk> perks;
    private static List<PerkCategory> categories;
    private static List<Perk> perksWithCat;

    public static Perk getRandomPerk() {
        // Debug.Log("getRandomPerk perks>C: " + perksWithCat.Count);
        float random = Random.Range(0.0f, (float)perksWithCat.Count);
        // Debug.Log(">>>> getRandomPerk random: " + random);
        return perksWithCat[(int)random];
    }

    public static Perk getRandomPerk(List<Perk> _perks) {
        float random = Random.Range(0.0f, (float)_perks.Count);
        return _perks[(int)random];
    }

    public static PerkCategory getCategory(BasicClass basicClass) {
        PerkCategory category;
        switch(basicClass) {
            case BasicClass.Bard: category = PerkCategory.Bard;
            break;
            case BasicClass.Dark: category = PerkCategory.Dark;
            break;
            case BasicClass.Magic: category = PerkCategory.Magic;
            break;
            case BasicClass.Master: category = PerkCategory.Master;
            break;
            case BasicClass.Mystic: category = PerkCategory.Summon;
            break;
            case BasicClass.Order: category = PerkCategory.Order;
            break;
            case BasicClass.Priest: category = PerkCategory.Heal;
            break;
            case BasicClass.War: category = PerkCategory.Warrior;
            break;
            case BasicClass.Wild: category = PerkCategory.Wild;
            break;
            default: category = PerkCategory.Bard;
            break;
            // TODO: all cases
        }
        return category;
    }

    public static void addCategory(BasicClass basicClass) {
        addCategory(getCategory(basicClass));
    }

    public static void addCategory(PerkCategory category) {
        if (!hasCategory(category)) {
            categories.Add(category);
            perksWithCat = generatePerkWithCategories(categories);
        }
    }

    private static bool hasCategory(PerkCategory category) {
        foreach(PerkCategory cat in categories) {
            if (category == cat) {
                return true;
            }
        }
        return false;
    }

    public static List<Perk> generatePerkWithCategories(List<PerkCategory> _categories) {
        List<Perk> perksWithCategoires = new List<Perk>();
        foreach (Perk perk in perks) {
            // Debug.Log("CHECK PERK " + perk.name);
            bool matchAllCategories = true;
            foreach (PerkCategory cat in perk.categories) {
                if (cat == PerkCategory.Common) {
                    // Debug.Log("COMMON CAT FOUND!");
                    break;
                } else if (!_categories.Contains(cat)) {
                    // Debug.Log("DONT CONTAIN CAT! " + cat);
                    matchAllCategories = false;
                    break;
                }
            }
            if (matchAllCategories) {
                // Debug.Log("ADD PErkS TO ACTIVE -> " + perk.name);
                perksWithCategoires.Add(perk);
            }
        }
        return perksWithCategoires;
    }

    public static void init() {

        perks = new List<Perk>();
        categories = new List<PerkCategory>();
        

        // ---------------------------------------------------------
        // +1 PR to all

        Perk perk1 = new Perk();
        perk1.name = "+1 PR to all";
        perk1.addCategory(PerkCategory.Common);
        perk1.type = PerkType.Attribute;
        perk1.targetType = PerkTargetType.AllAllyHeroes;

        perk1.valueType = ValueType.Absolute;
        perk1.value = 1;
        perk1.attribute = Attr.PhysResist;

        perks.Add(perk1);

        // ---------------------------------------------------------
        // +1 MR to all

        Perk perk2 = new Perk();
        perk2.name = "+1 MR to all";
        perk2.addCategory(PerkCategory.Common);
        perk2.type = PerkType.Attribute;
        perk2.targetType = PerkTargetType.AllAllyHeroes;

        perk2.valueType = ValueType.Absolute;
        perk2.value = 1;
        perk2.attribute = Attr.MagicResist;

        perks.Add(perk2);

        // ---------------------------------------------------------

        //add x armor/shield/hp/mana/evasion/hp regen/mana regen/etc (1 target)

        Perk perk3 = new Perk();
        perk3.name = "+1 armor";
        perk3.addCategory(PerkCategory.Common);
        perk3.type = PerkType.Attribute;
        perk3.targetType = PerkTargetType.AllyHero;

        perk3.valueType = ValueType.Absolute;
        perk3.value = 1;
        perk3.attribute = Attr.Armor;

        perks.Add(perk3);

        // ---------------------------------------------------------

        Perk perk4 = new Perk();
        perk4.name = "+10 shield";
        perk4.addCategory(PerkCategory.Common);
        perk4.type = PerkType.Attribute;
        perk4.targetType = PerkTargetType.AllyHero;

        perk4.valueType = ValueType.Absolute;
        perk4.value = 10;
        perk4.attribute = Attr.Shield;

        perks.Add(perk4);

        // ---------------------------------------------------------

        Perk perk5 = new Perk();
        perk5.name = "+2 mana regen";
        perk5.addCategory(PerkCategory.Common);
        perk5.type = PerkType.Attribute;
        perk5.targetType = PerkTargetType.AllyHero;

        perk5.valueType = ValueType.Absolute;
        perk5.value = 2;
        perk5.attribute = Attr.ManaRegen;

        perks.Add(perk5);

        // ---------------------------------------------------------

        Perk perk6= new Perk();
        perk6.name = "+10 evasion power";
        perk6.addCategory(PerkCategory.Bard);
        perk6.type = PerkType.Attribute;
        perk6.targetType = PerkTargetType.AllyHero;

        perk6.valueType = ValueType.Absolute;
        perk6.value = 10;
        perk6.attribute = Attr.EvasionPower;

        perks.Add(perk6);

        // ---------------------------------------------------------


        perksWithCat = generatePerkWithCategories(categories);

        //Debug.Log("PERK COLLECTION INITIATED");
        //Debug.Log("ALL PERKS: " + perks.Count);
        //Debug.Log("ACTIVE PERKS: " + perksWithCat.Count);
    }
}
