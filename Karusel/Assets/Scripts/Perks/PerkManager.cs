using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PerkManager
{
    public static void applyPerks(List<Perk> globalPerks, List<Hero> heroes) {

        // apply global perks
        foreach (Perk perk in globalPerks) {
            switch (perk.type) {
                case PerkType.Attribute: applyAttrPerk(perk, heroes);
                break;
            }
            // TODO: all cases
        }

        // apply one target perks
        foreach (Hero hero in heroes) {
            foreach(Perk perk in hero.perks) {
                switch (perk.type) {
                    case PerkType.Attribute: applyAttrPerk(perk, hero);
                    break;
                }
                // TODO: all cases
            }
        }


    } 

    private static void applyAttrPerk(Perk perk, List<Hero> heroes) {
        switch (perk.targetType) {
            case PerkTargetType.AllAllyHeroes: {
                foreach(Hero hero in heroes) {
                    applyAttrPerk(perk, hero);
                }
            }
            break;
            // case PerkTargetType.AllyHero: {
            //     Hero hero = findHero(perk, heroes);
            //     calculateValue(perk, hero);
            //     hero.addAttribute(perk.attribute, perk.absoluteValue);
            // }
            // break;
            // TODO: all cases
        }
    }

    private static void applyAttrPerk(Perk perk, Hero hero) {
        calculateValue(perk, hero);
        hero.addAttribute(perk.attribute, perk.absoluteValue);
    }

    private static void calculateValue(Perk perk, Hero hero) {
        if (perk.valueType == ValueType.Absolute) {
            perk.absoluteValue = perk.value;
        } else if (perk.valueType == ValueType.Percent) {
            // TODO: handle calculateFrom
        }
    }

    // private static Hero findHero(Perk perk, List<Hero> heroes) {
    //     foreach(Hero hero in heroes) {
    //         if (hero.name == perk.targetHero.name) {
    //             return hero;
    //         }
    //     }
    //     Debug.Log("ERROR! PerkManager.findHero not found with name " + perk.targetHero.name);
    //     return null;
    // }
}
