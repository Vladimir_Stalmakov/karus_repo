using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player {
    
    public int health { get; set; }
    public string name { get; set; }

    public string opponentName { get; set; }
    public int opponentIndex { get; set; }
   
    // ----------- heroes, perks ------------------------------------------
   
    public int heroCount { get; set; }
    public int packHeroPlaceCount { get; set; }
    public int heroPlaceCount { get; set; }          // count of places for heroes on the circle
   
    public List<BasicHero> heroes { get; set; }           // active heroes
    public List<BasicHero> packHeroes { get; set; }       // additional heroes

    public int perkCount { get; set; }
    public List<Perk> perks { get; set; }            // active perks
    public List<Perk> packPerks { get; set; }        // additional perks

    // ---- upgrades 
    public List<SelectUpgrade> upgrades { get; set; }
    public int selectHeroCount { get; set; }
    public int selectAttrCount { get; set; }
    public int selectPerkCount { get; set; }

    public int packHeroCount { get; set; }
    public int packPerkCount { get; set; }

    public bool isMulticlassEnabled { get; set; } = false;

    // -------------  

    public Player() {
        
        health = Constants.PLAYER_START_HEALTH;
        
        heroCount = 0;
        heroPlaceCount = Constants.START_HERO_PLACE_COUNT;
        packHeroPlaceCount = Constants.START_PACK_HERO_PLACE_COUNT;

        perkCount = Constants.START_PERK_COUNT;

        heroes = new List<BasicHero>();
        packHeroes = new List<BasicHero>();
        perks = new List<Perk>();
        packPerks = new List<Perk>();
        upgrades = new List<SelectUpgrade>();

        selectHeroCount = Constants.START_SELECT_HERO_COUNT;
        selectAttrCount = Constants.START_SELECT_ATTR_COUNT;
        selectPerkCount = Constants.START_SELECT_PERK_COUNT;
    }

    public void addHero(BasicHero hero) {
        if (heroes.Count == heroCount) {
            heroes.Add(hero);
        } else {
            heroes[heroCount] = hero;
        }
        heroCount++;
    }

    public void addPerk(Perk perk) {
        perks.Add(perk);
    }

    public void addPackHero(BasicHero hero) {
        if (packHeroes.Count == packHeroCount) {
            packHeroes.Add(hero);
        } else {
            packHeroes[packHeroCount] = hero;
        }
        packHeroCount++;
    }

    public void addUpgrade(SelectUpgrade upgrade) {
       upgrades.Add(upgrade);
    }

    public void moveHeroForward(int index) {
        //Debug.Log("moveHeroForward");
        //int index = 0;
        // for(int i=0; i<heroCount; i++) {
        //     if (hero.name == heroes[i].name) {
        //         index = i;
        //         break;
        //     }
        // }
        //Debug.Log("index: " + index);
        if (index < heroCount - 1) {
            exchangeHeroes(index, index+1);
        } else {
            exchangeHeroes(index, 0);
        }
        //Debug.Log("after exchange");
        // for(int i=0; i<heroCount; i++) {
        //     Debug.Log(i + ": " + heroes[i].name);
        // }
    }

    public void moveHeroToPack(int index) {
        BasicHero hero1 = heroes[index];
        BasicHero hero2;
        if (packHeroCount > 0) {
            hero2 = packHeroes[0];
        } else {
            hero2 = null;
            heroCount--;
        }
        
        heroes[index] = hero2;
         if (packHeroCount > 0) {
            packHeroes[0] = hero1;
        } else {
            addPackHero(hero1);
        }
    }

    public void moveHeroToActive(int index) {
        Debug.Log("moveHeroToActive " + index + "heroCount: " + heroCount);
        BasicHero hero1 = packHeroes[index];
        BasicHero hero2;
        if (heroCount > 0) {
            hero2 = heroes[0];
        } else {
            hero2 = null;
            packHeroCount--;
        }
        
        packHeroes[index] = hero2;
         if (heroCount > 0) {
            heroes[0] = hero1;
        } else {
            addHero(hero1);
        }
    }

    public void changeHero(int index, bool isActivePanel, BasicHero hero) {
        if (isActivePanel) {
            changeActiveHero(index, hero);
        } else {
            changePackHero(index, hero);
        }
    }

    private void changeActiveHero(int index, BasicHero hero) {
        Debug.Log("changeActiveHero heroCount: " + heroCount);
        heroes[index] = hero;
        if (hero == null) {
            Debug.Log("hero NULL");
            heroCount--; 
        }
    }

    private void changePackHero(int index, BasicHero hero) {
        packHeroes[index] = hero;
        if (hero == null) {
            packHeroCount--; 
        }
    }

    private void exchangeHeroes(int index1, int index2) {
        BasicHero hero1 = heroes[index1];
        BasicHero hero2 = heroes[index2];
        heroes[index1] = hero2;
        heroes[index2] = hero1;
    }
}
