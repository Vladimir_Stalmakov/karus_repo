using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAttribute
{
    public Attr attribute { get; set; }
    public int value { get; set; }

    public HeroAttribute(Attr _attribute, int _value) {
        attribute = _attribute;
        value = _value;
    }
}
