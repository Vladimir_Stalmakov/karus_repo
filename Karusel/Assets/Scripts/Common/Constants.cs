public static class Constants
{
    public static readonly int DEFAULT_CRIT_POWER = 50;
    public static readonly int DEFAULT_EVASION_POWER = 50;

    public static readonly int PLAYER_START_HEALTH = 100;
    public static readonly int START_HERO_PLACE_COUNT = 3;
    public static readonly int START_PERK_COUNT = 5;
    public static readonly int START_PACK_HERO_PLACE_COUNT = 1;

    public static readonly int START_SELECT_HERO_COUNT = 2;
    public static readonly int START_SELECT_PERK_COUNT = 2;
    public static readonly int START_SELECT_ATTR_COUNT = 2;

    public static readonly int HERO_CLASS_COST = 3;
    public static readonly int PERK_COST = 2;
    public static readonly int ATTRIBUTE_COST = 1;
    public static readonly int ROLL_COST = 1;
    public static readonly int UPGRADE_COST = 4;

    public static readonly int START_GOLD = 3;

    public static readonly string HERO_NAME_TOTEM = "Totem";
    public static readonly string HERO_NAME_NONE = "None";
}
