using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum TotemType { }

public class Totem
{
    public Skill skill { get; set; }

    public string name { get; set; }
    public int duration { get; set; }
    public DurationType durationType { get; set; }

    public Totem copy() {
        Totem copy = new Totem();
        copy.name = name;
        copy.skill = skill;
        copy.duration = duration;
        copy.durationType = durationType;
        return copy;
    }
}
