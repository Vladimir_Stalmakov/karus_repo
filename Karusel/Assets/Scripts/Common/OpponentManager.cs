using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class OpponentManager
{
    public static Player getPlayer(int roundNumber) {
        Player player = new Player();
        getPlayer(player, roundNumber);
        return player;
    }
    
    public static void getPlayer(Player player, int roundNumber) {

        // for (int i=0; i<player1.heroCount; i++) {
        //     float random = Random.Range(0.0f, 0.9f);
        //     int heroClassNumber = (int)(random * 10);
        //     BasicHero hero = HeroFactory.createHero((BasicClass)heroClassNumber);
        //     player2.addHero(hero);
        // }
        
        
        switch(roundNumber) {
        // ROUND 1 (3 gold): 1 hero
            case 1: {
                int heroClassNumber = getRandomBasicClass();
                BasicHero hero = HeroFactory.createHero((BasicClass)heroClassNumber);
                //BasicHero hero = HeroFactory.createHero(BasicClass.Dark);
                player.addHero(hero);
            }
            break;
        // ROUND 2 (4 gold): 2 heroes + 1 attr
            case 2: {
                // heroes
                int heroClassNumber1 = getRandomBasicClass();
                BasicHero hero1 = HeroFactory.createHero((BasicClass)heroClassNumber1);
                int heroClassNumber2 = getRandomBasicClass();
                while(heroClassNumber2 == heroClassNumber1) {
                    heroClassNumber2 = getRandomBasicClass();
                }
                BasicHero hero2 = HeroFactory.createHero((BasicClass)heroClassNumber2);
                player.addHero(hero1);
                player.addHero(hero2);
                BasicHero[] heroes = new BasicHero[] { hero1, hero2 };
                // attrs
                addAttributes(heroes, 1);
            }
            break;
        // ROUND 3 (5 gold): 3 heroes + [1 attr + 1 perk OR 2 attr]
            case 3: {
                // heroes
                int heroClassNumber1 = getRandomBasicClass();
                BasicHero hero1 = HeroFactory.createHero((BasicClass)heroClassNumber1);
                int heroClassNumber2 = getRandomBasicClass();
                while(heroClassNumber2 == heroClassNumber1) {
                    heroClassNumber2 = getRandomBasicClass();
                }
                BasicHero hero2 = HeroFactory.createHero((BasicClass)heroClassNumber2);
                int heroClassNumber3 = getRandomBasicClass();
                while(heroClassNumber3 == heroClassNumber1 || heroClassNumber3 == heroClassNumber2) {
                    heroClassNumber3 = getRandomBasicClass();
                }
                BasicHero hero3 = HeroFactory.createHero((BasicClass)heroClassNumber3);
                player.addHero(hero1);
                player.addHero(hero2);
                player.addHero(hero3);
                BasicHero[] heroes = new BasicHero[] { hero1, hero2, hero3 };
                // attrs
                addAttributes(heroes, 1);
                // perks
                addPerks(player, heroes, 1);
            }
            break;
        // ROUND 4 (6 gold): 3 heroes + [2 attr + 2 perk]
            case 4: {
                // heroes
                int heroClassNumber1 = getRandomBasicClass();
                BasicHero hero1 = HeroFactory.createHero((BasicClass)heroClassNumber1);
                int heroClassNumber2 = getRandomBasicClass();
                while(heroClassNumber2 == heroClassNumber1) {
                    heroClassNumber2 = getRandomBasicClass();
                }
                BasicHero hero2 = HeroFactory.createHero((BasicClass)heroClassNumber2);
                int heroClassNumber3 = getRandomBasicClass();
                while(heroClassNumber3 == heroClassNumber1 || heroClassNumber3 == heroClassNumber2) {
                    heroClassNumber3 = getRandomBasicClass();
                }
                BasicHero hero3 = HeroFactory.createHero((BasicClass)heroClassNumber3);
                player.addHero(hero1);
                player.addHero(hero2);
                player.addHero(hero3);
                BasicHero[] heroes = new BasicHero[] { hero1, hero2, hero3 };
                // attrs
                addAttributes(heroes, 2);
                // perks
                addPerks(player, heroes, 2);
            }
            break;
        // ROUND 5 (7 gold): 2 heroes + 1 MC hero + [3 attr + 3 perk]
            case 5: {
                // heroes
                int heroClassNumber1 = getRandomBasicClass();
                BasicHero hero1 = HeroFactory.createHero((BasicClass)heroClassNumber1);
                int heroClassNumber2 = getRandomBasicClass();
                while(heroClassNumber2 == heroClassNumber1) {
                    heroClassNumber2 = getRandomBasicClass();
                }
                BasicHero hero2 = HeroFactory.createHero((BasicClass)heroClassNumber2);
                
                int heroClassNumber3 = getRandomMultiClass();
                BasicHero hero3 = HeroFactory.createMultiClassHero((MultiClass)heroClassNumber3);
                player.addHero(hero3);
                player.addHero(hero1);
                player.addHero(hero2);
                BasicHero[] heroes = new BasicHero[] { hero1, hero2, hero3 };
                // attrs
                addAttributes(heroes, 3);
                // perks
                addPerks(player, heroes, 3);
            }
            break;
        // ROUND 6 (8 gold): 1 hero + 2 MC heroes + [5 attr + 4 perk]
            case 6: {
                // heroes
                // hero 1
                int heroClassNumber1 = getRandomBasicClass();
                BasicHero hero1 = HeroFactory.createHero((BasicClass)heroClassNumber1);
                // hero 2
                int heroClassNumber2 = getRandomMultiClass();
                
                BasicHero hero2 = HeroFactory.createMultiClassHero((MultiClass)heroClassNumber2);
                // hero 3
                int heroClassNumber3 = getRandomMultiClass();
                while(heroClassNumber2 == heroClassNumber3) {
                     heroClassNumber3 = getRandomMultiClass();
                }
                BasicHero hero3 = HeroFactory.createMultiClassHero((MultiClass)heroClassNumber3);
                player.addHero(hero2);
                player.addHero(hero3);
                player.addHero(hero1);
                BasicHero[] heroes = new BasicHero[] { hero1, hero2, hero3 };
                // attrs
                addAttributes(heroes, 5);
                // perks
                addPerks(player, heroes, 4);
            }
            break;
        // ROUND 7 (9 gold): 2 heroes + 2 MC heroes + [5 attr + 5 perk]
            case 7: {
                // heroes
                // hero 1
                int heroClassNumber1 = getRandomBasicClass();
                BasicHero hero1 = HeroFactory.createHero((BasicClass)heroClassNumber1);
                // hero 2
                int heroClassNumber2 = getRandomMultiClass();
                
                BasicHero hero2 = HeroFactory.createMultiClassHero((MultiClass)heroClassNumber2);
                // hero 3
                int heroClassNumber3 = getRandomMultiClass();
                while(heroClassNumber2 == heroClassNumber3) {
                     heroClassNumber3 = getRandomMultiClass();
                }
                BasicHero hero3 = HeroFactory.createMultiClassHero((MultiClass)heroClassNumber3);
                // hero 4
                int heroClassNumber4 = getRandomBasicClass();
                while(heroClassNumber4 == heroClassNumber1) {
                     heroClassNumber4 = getRandomBasicClass();
                }
                BasicHero hero4 = HeroFactory.createHero((BasicClass)heroClassNumber4);
                //
                player.addHero(hero2);
                player.addHero(hero3);
                player.addHero(hero1);
                player.addHero(hero4);
                BasicHero[] heroes = new BasicHero[] { hero1, hero2, hero3, hero4 };
                // attrs
                addAttributes(heroes, 5);
                // perks
                addPerks(player, heroes, 5);
            }
            break;
        }
    }

    public static void select(Player player, int roundNumber) {   
       
        switch(roundNumber) {
        // ROUND 1 (3 gold): +1 hero
            case 1: {
                int heroClassNumber = getRandomBasicClass();
                BasicHero hero = HeroFactory.createHero((BasicClass)heroClassNumber);
                //BasicHero hero = HeroFactory.createHero(BasicClass.Dark);
                player.addHero(hero);
            }
            break;
        // ROUND 2 (4 gold): + 1 hero + 1 attr
            case 2: {
                // heroes
                BasicHero hero1 = player.heroes[0];
                int heroClassNumber1 = (int)hero1.heroClass.basicClass;
                
                int heroClassNumber2 = getRandomBasicClass();
                while(heroClassNumber2 == heroClassNumber1) {
                    heroClassNumber2 = getRandomBasicClass();
                }
                BasicHero hero2 = HeroFactory.createHero((BasicClass)heroClassNumber2);
                //player.addHero(hero1);
                player.addHero(hero2);
                BasicHero[] heroes = new BasicHero[] { hero1, hero2 };
                // attrs
                addAttributes(heroes, 1);
            }
            break;
        // ROUND 3 (5 gold): +1 hero + 1 perk
            case 3: {
                // heroes
                BasicHero hero1 = player.heroes[0];
                int heroClassNumber1 = (int)hero1.heroClass.basicClass;
                BasicHero hero2 = player.heroes[1];
                int heroClassNumber2 = (int)hero2.heroClass.basicClass;
                //
                // int heroClassNumber1 = getRandomBasicClass();
                // BasicHero hero1 = HeroFactory.createHero((BasicClass)heroClassNumber1);
                // int heroClassNumber2 = getRandomBasicClass();
                // while(heroClassNumber2 == heroClassNumber1) {
                //     heroClassNumber2 = getRandomBasicClass();
                // }
                // BasicHero hero2 = HeroFactory.createHero((BasicClass)heroClassNumber2);
                int heroClassNumber3 = getRandomBasicClass();
                while(heroClassNumber3 == heroClassNumber1 || heroClassNumber3 == heroClassNumber2) {
                    heroClassNumber3 = getRandomBasicClass();
                }
                BasicHero hero3 = HeroFactory.createHero((BasicClass)heroClassNumber3);
                //player.addHero(hero1);
                //player.addHero(hero2);
                player.addHero(hero3);
                BasicHero[] heroes = new BasicHero[] { hero1, hero2, hero3 };
                // attrs
                //addAttributes(heroes, 1);
                // perks
                addPerks(player, heroes, 1);
            }
            break;
        // ROUND 4 (6 gold): +1 hero(pack) +1 attr +1 perk
            case 4: {
                // heroes
                BasicHero hero1 = player.heroes[0];
                BasicHero hero2 = player.heroes[1];
                BasicHero hero3 = player.heroes[2];
                // int heroClassNumber1 = getRandomBasicClass();
                // BasicHero hero1 = HeroFactory.createHero((BasicClass)heroClassNumber1);
                // int heroClassNumber2 = getRandomBasicClass();
                // while(heroClassNumber2 == heroClassNumber1) {
                //     heroClassNumber2 = getRandomBasicClass();
                // }
                // BasicHero hero2 = HeroFactory.createHero((BasicClass)heroClassNumber2);
                // int heroClassNumber3 = getRandomBasicClass();
                // while(heroClassNumber3 == heroClassNumber1 || heroClassNumber3 == heroClassNumber2) {
                //     heroClassNumber3 = getRandomBasicClass();
                // }
                // BasicHero hero3 = HeroFactory.createHero((BasicClass)heroClassNumber3);
                //player.addHero(hero1);
                //player.addHero(hero2);
                //player.addHero(hero3);
                BasicHero[] heroes = new BasicHero[] { hero1, hero2, hero3 };
                // attrs
                addAttributes(heroes, 1);
                // perks
                addPerks(player, heroes, 1);
            }
            break;
        // ROUND 5 (7 gold): +MC upgrade +1 MC hero transform +1 attr +1 perk
            case 5: {
                // heroes
                BasicHero hero1 = player.heroes[0];
                BasicHero hero2 = player.heroes[1];
                BasicHero hero3 = player.heroes[2];
                int heroClassNumber3 = (int)hero3.heroClass.basicClass;
                
                MultiClass mcClass = MulticlassCollection.getMultiClass(hero1.heroClass.basicClass, hero2.heroClass.basicClass);
                BasicHero hero12 = HeroFactory.createMultiClassHero(mcClass);

                player.heroes = new List<BasicHero>();
                player.heroCount = 0;
                player.addHero(hero12);
                player.addHero(hero3);
                

                int heroClassNumber4 = getRandomBasicClass();
                while(heroClassNumber4 == heroClassNumber3) {
                    heroClassNumber4 = getRandomBasicClass();
                }
                BasicHero hero4 = HeroFactory.createHero((BasicClass)heroClassNumber4);
                player.addHero(hero4);

                BasicHero[] heroes = new BasicHero[] { hero12, hero3, hero4 };
                // attrs
                addAttributes(heroes, 1);
                // perks
                addPerks(player, heroes, 1);
            }
            break;
        // ROUND 6 (8 gold): +1 hero buy +1 MC hero transform +2 perk
            case 6: {
                // heroes
                BasicHero hero1 = player.heroes[0]; //mc
                BasicHero hero2 = player.heroes[1];
                BasicHero hero3 = player.heroes[2];
                //

                MultiClass mcClass = MulticlassCollection.getMultiClass(hero2.heroClass.basicClass, hero3.heroClass.basicClass);
                BasicHero hero23 = HeroFactory.createMultiClassHero(mcClass);
                
                int heroClassNumber4 = getRandomBasicClass();
                BasicHero hero4 = HeroFactory.createHero((BasicClass)heroClassNumber4);
                
                player.heroes = new List<BasicHero>();
                player.heroCount = 0;
                player.addHero(hero1);
                player.addHero(hero23);
                player.addHero(hero4);
                
                BasicHero[] heroes = new BasicHero[] { hero1, hero23, hero4 };
                // attrs
                //addAttributes(heroes, 5);
                // perks
                addPerks(player, heroes, 2);
            }
            break;
        // ROUND 7 (9 gold): 2 heroes + 2 MC heroes + [5 attr + 5 perk]
            case 7: {
                // heroes
                // hero 1
                int heroClassNumber1 = getRandomBasicClass();
                BasicHero hero1 = HeroFactory.createHero((BasicClass)heroClassNumber1);
                // hero 2
                int heroClassNumber2 = getRandomMultiClass();
                
                BasicHero hero2 = HeroFactory.createMultiClassHero((MultiClass)heroClassNumber2);
                // hero 3
                int heroClassNumber3 = getRandomMultiClass();
                while(heroClassNumber2 == heroClassNumber3) {
                     heroClassNumber3 = getRandomMultiClass();
                }
                BasicHero hero3 = HeroFactory.createMultiClassHero((MultiClass)heroClassNumber3);
                // hero 4
                int heroClassNumber4 = getRandomBasicClass();
                while(heroClassNumber4 == heroClassNumber1) {
                     heroClassNumber4 = getRandomBasicClass();
                }
                BasicHero hero4 = HeroFactory.createHero((BasicClass)heroClassNumber4);
                //
                player.addHero(hero2);
                player.addHero(hero3);
                player.addHero(hero1);
                player.addHero(hero4);
                BasicHero[] heroes = new BasicHero[] { hero1, hero2, hero3, hero4 };
                // attrs
                addAttributes(heroes, 5);
                // perks
                addPerks(player, heroes, 5);
            }
            break;
        }
    }

    private static int getRandomBasicClass() {
        float random = UnityEngine.Random.Range(0.0f, 0.9f);
        int heroClassNumber = (int)(random * 10);
        return heroClassNumber;
    }

    private static int getRandomMultiClass() {
        int mcCount = Enum.GetNames(typeof(MultiClass)).Length;
        float random = UnityEngine.Random.Range(1.0f, (float)mcCount);
        Debug.Log("getRandomMultiClass mcCount: " + mcCount + " random: " + random);
        int heroClassNumber = (int)random;
        return heroClassNumber;
    }

    private static List<PerkCategory> getPerkCategories(BasicHero[] heroes) {
        List<PerkCategory> perkCategories = new List<PerkCategory>();
        perkCategories.Add(PerkCategory.Common);
        for (int i=0; i < heroes.Length; i++) {
            BasicHero hero = heroes[i];
            perkCategories.Add(PerkCollection.getCategory(heroes[i].heroClass.basicClass));
            if (hero.heroClass.isMulticlass) {
                perkCategories.Add(PerkCollection.getCategory(heroes[i].heroClass.basicClass2));
            }
        }
        return perkCategories;
    }

    private static void addPerks(Player player, BasicHero[] heroes, int count) {
        List<PerkCategory> perkCategories = getPerkCategories(heroes);
        List<Perk> perks = PerkCollection.generatePerkWithCategories(perkCategories);
        
        for(int i=0; i<count; i++) {
            Perk perk = PerkCollection.getRandomPerk(perks);
            if (perk.type == PerkType.Global) {
                player.addPerk(perk);
            } else if (perk.type == PerkType.Attribute) {
                getRandomHero(heroes).addPerk(perk);
            }
        }
    }

    private static void addAttributes(BasicHero[] heroes, int count) {
        for(int i=0; i<count; i++) {
            HeroAttribute attr = HeroAttributeCollection.getRandomHeroAttr();
            getRandomHero(heroes).addHeroAttribute(attr);
        }
    }


    private static BasicHero getRandomHero(BasicHero[] heroes) {
        int random = (int)(UnityEngine.Random.Range(0.0f, (float)heroes.Length));
        if (random == heroes.Length) {
            random = heroes.Length - 1;
        }
        return heroes[random];
    }
}
