using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HeroAttributeCollection {

    private static List<HeroAttribute> attributes = new List<HeroAttribute>();

    public static HeroAttribute getRandomHeroAttr() {
        float random = Random.Range(0.0f, (float)attributes.Count);
        // Debug.Log(">>>> getRandomHeroAttr random: " + random);
        return attributes[(int)random];
    }

    public static void init() {
        // add all heroAttr here

        HeroAttribute health5 = new HeroAttribute(Attr.Health, 5);
        attributes.Add(health5);

        HeroAttribute health10 = new HeroAttribute(Attr.Health, 10);
        attributes.Add(health10);

        HeroAttribute shield5 = new HeroAttribute(Attr.Shield, 5);
        attributes.Add(shield5);

        HeroAttribute shield10 = new HeroAttribute(Attr.Shield, 10);
        attributes.Add(shield10);

        HeroAttribute armor1 = new HeroAttribute(Attr.Armor, 1);
        attributes.Add(armor1);

        HeroAttribute armor2 = new HeroAttribute(Attr.Armor, 2);
        attributes.Add(armor2);

        HeroAttribute armor3 = new HeroAttribute(Attr.Armor, 3);
        attributes.Add(armor3);

        HeroAttribute pp1 = new HeroAttribute(Attr.PhysPower, 1);
        attributes.Add(pp1);

        HeroAttribute pp2 = new HeroAttribute(Attr.PhysPower, 2);
        attributes.Add(pp2);

        HeroAttribute mp1 = new HeroAttribute(Attr.MagicPower, 1);
        attributes.Add(mp1);

        HeroAttribute mp2 = new HeroAttribute(Attr.MagicPower, 2);
        attributes.Add(mp2);

        HeroAttribute mana1 = new HeroAttribute(Attr.Mana, 1);
        attributes.Add(mana1);

        HeroAttribute mana2 = new HeroAttribute(Attr.Mana, 2);
        attributes.Add(mana2);

        HeroAttribute mana3 = new HeroAttribute(Attr.Mana, 3);
        attributes.Add(mana3);

        HeroAttribute evasion1 = new HeroAttribute(Attr.EvasionChance, 1);
        attributes.Add(evasion1);

        HeroAttribute evasion2 = new HeroAttribute(Attr.EvasionChance, 2);
        attributes.Add(evasion2);

        HeroAttribute evasion3 = new HeroAttribute(Attr.EvasionChance, 3);
        attributes.Add(evasion3);

        HeroAttribute crit2 = new HeroAttribute(Attr.CritChance, 2);
        attributes.Add(crit2);

        HeroAttribute crit3 = new HeroAttribute(Attr.CritChance, 3);
        attributes.Add(crit3);

        HeroAttribute crit4 = new HeroAttribute(Attr.CritChance, 4);
        attributes.Add(crit4);

        HeroAttribute pr1 = new HeroAttribute(Attr.PhysResist, 1);
        attributes.Add(pr1);

        HeroAttribute pr2 = new HeroAttribute(Attr.PhysResist, 2);
        attributes.Add(pr2);

        HeroAttribute mr1 = new HeroAttribute(Attr.MagicResist, 1);
        attributes.Add(mr1);

        HeroAttribute mr2 = new HeroAttribute(Attr.MagicResist, 2);
        attributes.Add(mr2);

        HeroAttribute healthRegen1 = new HeroAttribute(Attr.HealthRegen, 1);
        attributes.Add(healthRegen1);

        HeroAttribute healthRegen2 = new HeroAttribute(Attr.HealthRegen, 2);
        attributes.Add(healthRegen2);

        HeroAttribute manaRegen1 = new HeroAttribute(Attr.ManaRegen, 1);
        attributes.Add(manaRegen1);

        HeroAttribute manaRegen2 = new HeroAttribute(Attr.ManaRegen, 2);
        attributes.Add(manaRegen2);

        HeroAttribute critPower5 = new HeroAttribute(Attr.CritPower, 5);
        attributes.Add(critPower5);

        HeroAttribute critPower10 = new HeroAttribute(Attr.CritPower, 10);
        attributes.Add(critPower10);
    }
}
