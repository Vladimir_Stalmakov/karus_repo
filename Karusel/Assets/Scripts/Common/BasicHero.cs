using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicHero
{
    public HeroClass heroClass { get; set; }
    public string name { get; set; }

    // --------------------------------------------------

    public int health { get; set; }
    public int mana { get; set; }
    public int healthRegen { get; set; }    // restore health each round
    public int manaRegen { get; set; }      // restore mana each circle

    public int magicPower { get; set; }
    public int physPower { get; set; }
    public int magicResist { get; set; }
    public int physResist { get; set; }

    public HeroDamageType attackType { get; set; }
    public int damage { get; set; }           // default attack damage

    public int shield { get; set; }
    public int armor { get; set; }

    public int evasionChance { get; set; }
    public int evasionPower { get; set; }
    public int critChance { get; set; }
    public int critPower { get; set; }

    // --------------------------------------------------

    public Skill activeSkill { get; set; }
    public Skill battleSkill { get; set; }
    public List<PassiveSkill> passiveSkills { get; set; }

    // --------------------------------------------------

    public List<HeroAttribute> attributes { get; set; }
     public List<Perk> perks { get; set; }

    // --------------------------------------------------


    public BasicHero()
    {
        evasionPower = Constants.DEFAULT_EVASION_POWER;
        critPower = Constants.DEFAULT_CRIT_POWER;

        attributes = new List<HeroAttribute>();
        perks = new List<Perk>();
        passiveSkills = new List<PassiveSkill>();
    }

    public void addHeroAttribute(HeroAttribute heroAttr) {
        changeAttribute(heroAttr.attribute, heroAttr.value, true);
        attributes.Add(heroAttr);
    }

    public void addPerk(Perk perk) {
        perks.Add(perk);
    }

    public void addPassiveSkill(PassiveSkill passiveSkill) {
        passiveSkills.Add(passiveSkill);
    }

    public PassiveSkill getPassiveSkill(PassiveSkillType passiveSkillType) {
        return passiveSkills.Find(skill => skill.type == passiveSkillType);
    }

    public bool hasPassiveSkill(PassiveSkillType passiveSkillType) {
        return getPassiveSkill(passiveSkillType) != null;
    }

    // public void upgradeToMultiClass(BasicHero heroToCombine) {
    //     heroClass.upgradeToMultiClass(heroToCombine.heroClass.basicClass);
    //     name = heroClass.name;
    // }

    public void changeAttribute(Attr attr, int value, bool usePreviousValue) {
        switch(attr) {
            case Attr.Health: {
                health = (usePreviousValue ? health : 0) + value;
            }
            break;
            case Attr.Mana: {
                mana = (usePreviousValue ? mana : 0) + value;
            }
            break;
            case Attr.HealthRegen: {
                healthRegen = (usePreviousValue ? healthRegen : 0) + value;
            }
            break;
            case Attr.ManaRegen: {
                manaRegen = (usePreviousValue ? manaRegen : 0) + value;
            }
            break;
            case Attr.Damage: {
                damage = (usePreviousValue ? damage : 0) + value;
            }
            break;
            case Attr.Shield: {
                shield = (usePreviousValue ? shield : 0) + value;
            }
            break;
            case Attr.Armor: {
                armor = (usePreviousValue ? armor : 0) + value;
            }
            break;
            case Attr.MagicResist: {
                magicResist = (usePreviousValue ? magicResist : 0) + value;
            }
            break;
            case Attr.PhysResist: {
                physResist = (usePreviousValue ? physResist : 0) + value;
            }
            break;
            case Attr.CritChance: {
                critChance = (usePreviousValue ? critChance : 0) + value;
            }
            break;
            case Attr.CritPower: {
                critPower = (usePreviousValue ? critPower : 0) + value;
            }
            break;
            case Attr.EvasionChance: {
                evasionChance = (usePreviousValue ? evasionChance : 0) + value;
            }
            break;
            case Attr.EvasionPower: {
                evasionPower = (usePreviousValue ? evasionPower : 0) + value;
            }
            break;
            // TODO: all cases
        }
    }
}
