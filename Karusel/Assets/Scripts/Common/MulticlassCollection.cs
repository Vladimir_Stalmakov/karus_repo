using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MulticlassCollection
{
    public static MultiClass getMultiClass(BasicClass class1, BasicClass class2) {
        Debug.Log("getMultiClass >> " + class1 + " + " + class2);
        switch (class1) {
            case BasicClass.Bard: return getBardMultiClass(class2);
            case BasicClass.War: return getWarriorMultiClass(class2);
            case BasicClass.Wild: return getWildMultiClass(class2);
            case BasicClass.Dark: return getDarkMultiClass(class2);
            case BasicClass.Mystic: return getMysticMultiClass(class2);
            case BasicClass.Priest: return getPriestMultiClass(class2);
            case BasicClass.Magic: return getMagicMultiClass(class2);
            case BasicClass.Order: return getOrderMultiClass(class2);
            case BasicClass.Master: return getMasterMultiClass(class2);
        }
        // TODO: all cases
        return MultiClass.ERROR;
    }

    public static List<BasicClass> getBasicClasses(MultiClass multiClass) {
        List<BasicClass> classes = new List<BasicClass>();
        switch (multiClass) {
            case MultiClass.Alchemist: {
                classes.Add(BasicClass.Priest);
                classes.Add(BasicClass.Magic);
            }
            break;
             case MultiClass.Assasin: {
                classes.Add(BasicClass.Dark);
                classes.Add(BasicClass.Master);
            }
            break;
            case MultiClass.Barbarian: {
                classes.Add(BasicClass.War);
                classes.Add(BasicClass.Wild);
            }
            break;
            case MultiClass.BattleMage: {
                classes.Add(BasicClass.War);
                classes.Add(BasicClass.Magic);
            }
            break;
            case MultiClass.Beastmaster: {
                classes.Add(BasicClass.Wild);
                classes.Add(BasicClass.Mystic);
            }
            break;
            case MultiClass.BlackKnight: {
                classes.Add(BasicClass.Dark);
                classes.Add(BasicClass.War);
            }
            break;
             case MultiClass.Bladedancer: {
                classes.Add(BasicClass.War);
                classes.Add(BasicClass.Bard);
            }
            break;
            case MultiClass.Commander: {
                classes.Add(BasicClass.War);
                classes.Add(BasicClass.Master);
            }
            break;
            case MultiClass.Doomsayer: {
                classes.Add(BasicClass.Dark);
                classes.Add(BasicClass.Bard);
            }
            break;
            case MultiClass.Druid: {
                classes.Add(BasicClass.Magic);
                classes.Add(BasicClass.Wild);
            }
            break;
            case MultiClass.Exorcist: {
                classes.Add(BasicClass.Priest);
                classes.Add(BasicClass.Mystic);
            }
            break;
            case MultiClass.ForestProtector: {
                classes.Add(BasicClass.Wild);
                classes.Add(BasicClass.Order);
            }
            break;
            case MultiClass.Gladiator: {
                classes.Add(BasicClass.War);
                classes.Add(BasicClass.Mystic);
            }
            break;
            case MultiClass.Hermit: {
                classes.Add(BasicClass.Wild);
                classes.Add(BasicClass.Priest);
            }
            break;
            case MultiClass.Herald: {
                classes.Add(BasicClass.Bard);
                classes.Add(BasicClass.Order);
            }
            break;
            case MultiClass.Hunter: {
                classes.Add(BasicClass.Wild);
                classes.Add(BasicClass.Master);
            }
            break;
            case MultiClass.Illusionist: {
                classes.Add(BasicClass.Magic);
                classes.Add(BasicClass.Mystic);
            }
            break;
            case MultiClass.Inquisitor: {
                classes.Add(BasicClass.Priest);
                classes.Add(BasicClass.Order);
            }
            break;
            case MultiClass.Knight: {
                classes.Add(BasicClass.War);
                classes.Add(BasicClass.Order);
            }
            break;
            case MultiClass.MagicBard: {
                classes.Add(BasicClass.Magic);
                classes.Add(BasicClass.Bard);
            }
            break;
            case MultiClass.Mimic: {
                classes.Add(BasicClass.Mystic);
                classes.Add(BasicClass.Master);
            }
            break;
            case MultiClass.Necromancer: {
                classes.Add(BasicClass.Dark);
                classes.Add(BasicClass.Mystic);
            }
            break;
            case MultiClass.Oracle: {
                classes.Add(BasicClass.Order);
                classes.Add(BasicClass.Mystic);
            }
            break;
            case MultiClass.Paladin: {
                classes.Add(BasicClass.War);
                classes.Add(BasicClass.Priest);
            }
            break;
            case MultiClass.Predator: {
                classes.Add(BasicClass.Wild);
                classes.Add(BasicClass.Dark);
            }
            break;
            case MultiClass.PriestMaster: {
                classes.Add(BasicClass.Master);
                classes.Add(BasicClass.Priest);
            }
            break;
             case MultiClass.Runecaster: {
                classes.Add(BasicClass.Master);
                classes.Add(BasicClass.Priest);
            }
            break;
            case MultiClass.Samurai: {
                classes.Add(BasicClass.Order);
                classes.Add(BasicClass.Master);
            }
            break;
            case MultiClass.ShadowMaster: {
                classes.Add(BasicClass.Magic);
                classes.Add(BasicClass.Order);
            }
            break;
            case MultiClass.Shaman: {
                classes.Add(BasicClass.Wild);
                classes.Add(BasicClass.Bard);
            }
            break;
            case MultiClass.Sorcerer: {
                classes.Add(BasicClass.Magic);
                classes.Add(BasicClass.Master);
            }
            break;
            case MultiClass.SpiritMinstrel: {
                classes.Add(BasicClass.Bard);
                classes.Add(BasicClass.Priest);
            }
            break;
            case MultiClass.Warlock: {
                classes.Add(BasicClass.Magic);
                classes.Add(BasicClass.Dark);
            }
            break;
            case MultiClass.Witch: {
                classes.Add(BasicClass.Mystic);
                classes.Add(BasicClass.Bard);
            }
            break;
            case MultiClass.Zealot: {
                classes.Add(BasicClass.Order);
                classes.Add(BasicClass.Dark);
            }
            break;
        }
        return classes;
    }
    
    public static string getMultiClassName(MultiClass multiClass) {
        switch (multiClass) {
            case MultiClass.ERROR: return "ERROR!!!";
            
            case MultiClass.Alchemist: return "Alchemist";
            case MultiClass.Assasin: return "Assasin";
            case MultiClass.Barbarian: return "Barbarian";
            case MultiClass.BardMaster: return "BardMaster";
            case MultiClass.BattleMage: return "Battle Mage";
            case MultiClass.Beastmaster: return "Beastmaster";
            case MultiClass.BlackKnight: return "Black Knight";
            case MultiClass.Bladedancer: return "Bladedancer";
            case MultiClass.Commander: return "Commander";
            case MultiClass.Doomsayer: return "Doomsayer";
            case MultiClass.Druid: return "Druid";
            case MultiClass.Exorcist: return "Exorcist";
            case MultiClass.ForestProtector: return "Forest Protector";
            case MultiClass.Gladiator: return "Gladiator";
            case MultiClass.Illusionist: return "Illusionist";
            case MultiClass.Inquisitor: return "Inquisitor";
            case MultiClass.Hermit: return "Hermit";
            case MultiClass.Herald: return "Herald";
            case MultiClass.Hunter: return "Hunter";
            case MultiClass.Knight: return "Knight";
            case MultiClass.MagicBard: return "Magic Bard";
            case MultiClass.Mimic: return "Mimic";
            case MultiClass.Necromancer: return "Necromancer";
            case MultiClass.Oracle: return "Oracle";
            case MultiClass.Paladin: return "Paladin";
            case MultiClass.Predator: return "Predator";
            case MultiClass.PriestMaster: return "High Priest";
            case MultiClass.Runecaster: return "Runecaster";
            case MultiClass.Samurai: return "Samurai";
            case MultiClass.ShadowMaster: return "Shadow Master";
            case MultiClass.Shaman: return "Shaman";
            case MultiClass.Sorcerer: return "Sorcerer";
            case MultiClass.SpiritMinstrel: return "Spirit Minstrel";
            case MultiClass.Warlock: return "Warlock";
            case MultiClass.Witch: return "Witch";
            case MultiClass.Zealot: return "Zealot";
        }
        return "ERROR";
    }

    private static MultiClass getWarriorMultiClass(BasicClass basiClass) {
        switch(basiClass) {
            case BasicClass.Priest: return MultiClass.Paladin;
            case BasicClass.Wild: return MultiClass.Barbarian;
            case BasicClass.Mystic: return MultiClass.Gladiator;
            case BasicClass.Magic: return MultiClass.BattleMage;
            case BasicClass.Order: return MultiClass.Knight;
            case BasicClass.Master: return MultiClass.Commander;
            case BasicClass.Bard: return MultiClass.Bladedancer;
            case BasicClass.Dark: return MultiClass.BlackKnight;
        }
        return MultiClass.ERROR;
    }

    private static MultiClass getWildMultiClass(BasicClass basiClass) {
        switch(basiClass) {
            case BasicClass.Bard: return MultiClass.Shaman;
            case BasicClass.Magic: return MultiClass.Druid;
            case BasicClass.War: return MultiClass.Barbarian;
            case BasicClass.Priest: return MultiClass.Hermit;
            case BasicClass.Mystic: return MultiClass.Beastmaster;
            case BasicClass.Master: return MultiClass.Hunter;
            case BasicClass.Order: return MultiClass.ForestProtector;
            case BasicClass.Dark: return MultiClass.Predator;
        }
        return MultiClass.ERROR;
    }

    private static MultiClass getDarkMultiClass(BasicClass basiClass) {
        switch(basiClass) {
            case BasicClass.Mystic: return MultiClass.Necromancer;
            case BasicClass.Magic: return MultiClass.Warlock;
            case BasicClass.Master: return MultiClass.Assasin;
            case BasicClass.Order: return MultiClass.Zealot;
            case BasicClass.Wild: return MultiClass.Predator;
            case BasicClass.War: return MultiClass.BlackKnight;
            case BasicClass.Bard: return MultiClass.Doomsayer;
            case BasicClass.Priest: return MultiClass.ShadowMaster;
        }
        return MultiClass.ERROR;
    }

    private static MultiClass getMysticMultiClass(BasicClass basiClass) {
        switch(basiClass) {
            case BasicClass.Dark: return MultiClass.Necromancer;
            case BasicClass.Magic: return MultiClass.Illusionist;
            case BasicClass.War: return MultiClass.Gladiator;
            case BasicClass.Wild: return MultiClass.Beastmaster;
            case BasicClass.Priest: return MultiClass.Exorcist;
            case BasicClass.Bard: return MultiClass.Witch;
            case BasicClass.Master: return MultiClass.Mimic;
            case BasicClass.Order: return MultiClass.Oracle;
        }
        return MultiClass.ERROR;
    }

    private static MultiClass getPriestMultiClass(BasicClass basiClass) {
        switch(basiClass) {
            case BasicClass.War: return MultiClass.Paladin;
            case BasicClass.Wild: return MultiClass.Hermit;
            case BasicClass.Order: return MultiClass.Inquisitor;
            case BasicClass.Mystic: return MultiClass.Exorcist;
            case BasicClass.Master: return MultiClass.PriestMaster;
            case BasicClass.Magic: return MultiClass.Alchemist;
            case BasicClass.Bard: return MultiClass.SpiritMinstrel;
            case BasicClass.Dark: return MultiClass.ShadowMaster;
        }
        return MultiClass.ERROR;
    }

    private static MultiClass getBardMultiClass(BasicClass basiClass) {
        switch(basiClass) {
            case BasicClass.Wild: return MultiClass.Shaman;
            case BasicClass.Order: return MultiClass.Herald;
            case BasicClass.Magic: return MultiClass.MagicBard;
            case BasicClass.War: return MultiClass.Bladedancer;
            case BasicClass.Mystic: return MultiClass.Witch;
            case BasicClass.Dark: return MultiClass.Doomsayer;
            case BasicClass.Priest: return MultiClass.SpiritMinstrel;
            case BasicClass.Master: return MultiClass.BardMaster;
        }
        return MultiClass.ERROR;
    }

    private static MultiClass getMagicMultiClass(BasicClass basiClass) {
        switch(basiClass) {
            case BasicClass.Wild: return MultiClass.Druid;
            case BasicClass.Mystic: return MultiClass.Illusionist;
            case BasicClass.War: return MultiClass.BattleMage;
            case BasicClass.Dark: return MultiClass.Warlock;
            case BasicClass.Master: return MultiClass.Sorcerer;
            case BasicClass.Bard: return MultiClass.MagicBard;
            case BasicClass.Priest: return MultiClass.Alchemist;
            case BasicClass.Order: return MultiClass.Runecaster;
        }
        return MultiClass.ERROR;
    }

    private static MultiClass getOrderMultiClass(BasicClass basiClass) {
        switch(basiClass) {
            case BasicClass.Master: return MultiClass.Samurai;
            case BasicClass.War: return MultiClass.Knight;
            case BasicClass.Priest: return MultiClass.Inquisitor;
            case BasicClass.Dark: return MultiClass.Zealot;
            case BasicClass.Bard: return MultiClass.Herald;
            case BasicClass.Wild: return MultiClass.ForestProtector;
            case BasicClass.Mystic: return MultiClass.Oracle;
            case BasicClass.Magic: return MultiClass.Runecaster;
        }
        return MultiClass.ERROR;
    }

    private static MultiClass getMasterMultiClass(BasicClass basiClass) {
        switch(basiClass) {
            case BasicClass.Order: return MultiClass.Samurai;
            case BasicClass.Dark: return MultiClass.Assasin;
            case BasicClass.Magic: return MultiClass.Sorcerer;
            case BasicClass.Wild: return MultiClass.Hunter;
            case BasicClass.War: return MultiClass.Commander;
            case BasicClass.Priest: return MultiClass.PriestMaster;
            case BasicClass.Mystic: return MultiClass.Mimic;
            case BasicClass.Bard: return MultiClass.BardMaster;
        }
        return MultiClass.ERROR;
    }
    
}
