using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BasicClass { War, Wild, Magic, Master, Dark, Bard, Mystic, Priest, Order }

public enum MultiClass { 
    ERROR,          // for debugging !!!!!!
   
    Alchemist,              // priest + magic
    Assasin,                // dark + master
    Barbarian,              // wild + war
    BardMaster,             // bard + master
    BattleMage,             // war + magic
    Beastmaster,            // wild + mystic
    BlackKnight,            // dark + war
    Bladedancer,            // war + bard
    Commander,              // war + master
    Doomsayer,              // dark + bard
    Druid,                  // wild + magic
    Exorcist,               // priest + mystic
    ForestProtector,        // wild + order
    Gladiator,              // war + mystic
    Herald,                 // bard + order
    Hermit,                 // wild + priest
    Hunter,                 // wild + master
    Illusionist,            // mystic + magic
    Inquisitor,             // priest + order
    Knight,                 // war + order
    MagicBard,              // magic + bard
    Mimic,                  // mystic + master
    Necromancer,            // dark + mystic
    Oracle,                 // order + mystic
    Paladin,                // priest + war
    Predator,               // dark + wild
    PriestMaster,           // priest + master
    Runecaster,             // order + magic
    Samurai,                 // war + order
    ShadowMaster,           // dark + priest
    Shaman,                 // wild + bard
    Sorcerer,               // magic + master
    SpiritMinstrel,         // priest + bard
    Warlock,                 // magic + dark
    Witch,                   // mystic + bard
    Zealot                  // order + dark
}

public class HeroClass
{
    public string name { get; set; }

    public BasicClass basicClass { get; set; }
    public BasicClass basicClass2 { get; set; }

    public bool isMulticlass { get; set; }

    public MultiClass multiClass { get; set; }

    public HeroClass(BasicClass _basicClass) {
        basicClass = _basicClass;
        name = getClassName(_basicClass);
    }

    public HeroClass(BasicClass _basicClass, BasicClass _basicClass2) {
        basicClass = _basicClass;
        basicClass2 = _basicClass2;
        multiClass = MulticlassCollection.getMultiClass(basicClass, basicClass2);
        name = MulticlassCollection.getMultiClassName(multiClass);
        isMulticlass = true;
    }

    public HeroClass(MultiClass _multiClass) {
        Debug.Log("HeroClass constructor " + _multiClass);
        multiClass = _multiClass;
        List<BasicClass> basicClasses = MulticlassCollection.getBasicClasses(multiClass);
        basicClass = basicClasses[0];
        basicClass2 = basicClasses[1];
        name = MulticlassCollection.getMultiClassName(multiClass);
        isMulticlass = true;
    }

    private string getClassName(BasicClass basicClass) {
        if (!isMulticlass) {
            switch(basicClass) {
                case BasicClass.War: return "Warrior";
                case BasicClass.Wild: return "Wild";
                case BasicClass.Magic: return "Mage";
                case BasicClass.Master: return "Master";
                case BasicClass.Mystic: return "Mystic";
                case BasicClass.Priest: return "Priest";
                case BasicClass.Order: return "Guard";
                case BasicClass.Dark: return "Occultist";
                case BasicClass.Bard: return "Bard";
                default: return "ERROR";
            }
        } else {
            return MulticlassCollection.getMultiClassName(multiClass);
        }
    }
}
