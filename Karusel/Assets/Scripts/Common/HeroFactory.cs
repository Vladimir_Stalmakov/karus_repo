using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HeroFactory
{
    public static BasicHero createHero(BasicClass basicClass) {
        BasicHero hero = createBasicHero(basicClass);
        HeroClass heroClass = new HeroClass(basicClass);
        hero.heroClass = heroClass;
        hero.name = heroClass.name;
        hero.activeSkill = SkillCollection.getSkill(heroClass, SkillPositionType.Active);
        hero.battleSkill = SkillCollection.getSkill(heroClass, SkillPositionType.Battle);
        hero.passiveSkills = PassiveSkillCollection.getSkills(heroClass.multiClass);
        return hero;
    }

    public static BasicHero createMultiClassHero(BasicHero hero1, BasicHero hero2) {
        HeroClass heroClass = new HeroClass(hero1.heroClass.basicClass, hero2.heroClass.basicClass);
        BasicHero hero = createBasicHero(heroClass.multiClass);
        // apply attributes from both heroes to new hero
        foreach(HeroAttribute attr in hero1.attributes) {
            hero.addHeroAttribute(attr);
        }
        foreach(HeroAttribute attr in hero2.attributes) {
            hero.addHeroAttribute(attr);
        }
        //
        addClassAndSkills(hero, heroClass);
        return hero;
    }

    public static BasicHero createMultiClassHero(MultiClass multiClass) {
        HeroClass heroClass = new HeroClass(multiClass);
        BasicHero hero = createBasicHero(multiClass);
        addClassAndSkills(hero, heroClass);
        return hero;
    }

    private static void addClassAndSkills(BasicHero hero, HeroClass heroClass) {
        hero.heroClass = heroClass;
        hero.name = heroClass.name;
        hero.activeSkill = SkillCollection.getSkill(heroClass.multiClass, SkillPositionType.Active);
        hero.battleSkill = SkillCollection.getSkill(heroClass.multiClass, SkillPositionType.Battle);
        hero.passiveSkills = PassiveSkillCollection.getSkills(heroClass.multiClass);
    }

    private static BasicHero createBasicHero(BasicClass basicClass) {
        switch(basicClass) {
        // WARRIROR    
            case BasicClass.War: return // createHero(50,8,0,0,0,0,0,0,HeroDamageType.Physical,20,10,0,0,0);
            createHero(
                50, // health, 
                8, // mana, 
                0, // healthRegen, 
                0, // manaRegen, 
                0, // magicPower, 
                0, // physPower, 
                0, // magicResist, 
                0, // physResist,
                HeroDamageType.Physical, 20, // attackType / attackDamage, 
                10, // shield, 
                0, // armor, 
                0, // evasionChance, 
                0 // critChance
            );
        // WILD
            case BasicClass.Wild: return // createHero(35,9,2,0,0,0,0,0,HeroDamageType.Physical,25,0,0,0,0);
            createHero(
                35, // health, 
                9, // mana, 
                2, // healthRegen, 
                0, // manaRegen, 
                0, // magicPower, 
                0, // physPower, 
                0, // magicResist, 
                0, // physResist,
                HeroDamageType.Physical, 25, // attackType / attackDamage, 
                0, // shield, 
                0, // armor, 
                0, // evasionChance, 
                0 // critChance
            );
        // MAGIC
            case BasicClass.Magic: return // createHero(50,20,0,4,3,0,1,0,HeroDamageType.Magic,20,0,0,0,0);
            createHero(
                25, // health, 
                20, // mana, 
                0, // healthRegen, 
                0, // manaRegen, 
                0, // magicPower, 
                0, // physPower, 
                0, // magicResist, 
                0, // physResist,
                HeroDamageType.Magic, 20, // attackType / attackDamage, 
                0, // shield, 
                0, // armor, 
                0, // evasionChance, 
                0  // critChance
            );
        // MASTER
            case BasicClass.Master: return // createHero(90,10,0,0,0,2,0,0,HeroDamageType.Physical,20,0,0,5,10);
            createHero(
                45, // health, 
                10, // mana, 
                0, // healthRegen, 
                0, // manaRegen, 
                0, // magicPower, 
                0, // physPower, 
                0, // magicResist, 
                0, // physResist,
                HeroDamageType.Physical, 18, // attackType / attackDamage, 
                0, // shield, 
                0, // armor, 
                0, // evasionChance, 
                10  // critChance
            );
        // BARD
            case BasicClass.Bard: return // createHero(80,14,0,0,0,0,0,0,HeroDamageType.Physical,15,0,0,5,0);
            createHero(
                40, // health, 
                14, // mana, 
                0, // healthRegen, 
                0, // manaRegen, 
                0, // magicPower, 
                0, // physPower, 
                0, // magicResist, 
                0, // physResist,
                HeroDamageType.Physical, 15, // attackType / attackDamage, 
                0, // shield, 
                0, // armor, 
                10, // evasionChance, 
                0  // critChance
            );
        // DARK
            case BasicClass.Dark: return // createHero(60,18,0,3,0,0,0,0,HeroDamageType.Magic,12,0,0,0,0);
            createHero(
                30, // health, 
                18, // mana, 
                0, // healthRegen, 
                2, // manaRegen, 
                0, // magicPower, 
                0, // physPower, 
                0, // magicResist, 
                0, // physResist,
                HeroDamageType.Magic, 14, // attackType / attackDamage, 
                0, // shield, 
                0, // armor, 
                0, // evasionChance, 
                0  // critChance
            );
        // PRIEST
            case BasicClass.Priest: return // createHero(80,12,0,1,0,0,0,0,HeroDamageType.Physical,10,0,0,0,0);
            createHero(
                36, // health, 
                12, // mana, 
                0, // healthRegen, 
                1, // manaRegen, 
                0, // magicPower, 
                0, // physPower, 
                0, // magicResist, 
                0, // physResist,
                HeroDamageType.Physical, 12, // attackType / attackDamage, 
                0, // shield, 
                0, // armor, 
                0, // evasionChance, 
                0  // critChance
            );
        // ORDER
            case BasicClass.Order: return //createHero(110,10,0,1,0,0,0,0,HeroDamageType.Physical,6,10,2,0,0);
            createHero(
                32, // health, 
                12, // mana, 
                0, // healthRegen, 
                1, // manaRegen, 
                0, // magicPower, 
                0, // physPower, 
                0, // magicResist, 
                0, // physResist,
                HeroDamageType.Physical, 12, // attackType / attackDamage, 
                20, // shield, 
                2, // armor, 
                0, // evasionChance, 
                0  // critChance
            );
        // MYSTIC
            case BasicClass.Mystic: return // createHero(40,15,0,0,0,0,0,0,HeroDamageType.Magic,7,0,0,0,0);
            createHero(
                20, // health, 
                15, // mana, 
                0, // healthRegen, 
                1, // manaRegen, 
                0, // magicPower, 
                0, // physPower, 
                0, // magicResist, 
                0, // physResist,
                HeroDamageType.Magic, 8, // attackType / attackDamage, 
                0, // shield, 
                0, // armor, 
                0, // evasionChance, 
                0  // critChance
            );
            default: {
                Debug.Log("NO basic hero class found for basicClass: " + basicClass);
                return null;
            }
        }
    }

    private static BasicHero createBasicHero(MultiClass multiClass) {
        switch(multiClass) {
            case MultiClass.Alchemist: return
                createHero(
                    80, // health, 
                    24, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Magic, 12, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Assasin: return
                createHero(
                    60, // health, 
                    10, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 12, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    10, // evasionChance, 
                    10  // critChance
                );
            case MultiClass.Barbarian: return createHero(50,20,0,4,3,0,1,0,HeroDamageType.Magic,30,0,0,0,0);
            // BardMaster NOT IMPLEMENTED
            case MultiClass.BardMaster: return
                createHero(
                    100, // health, 
                    20, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 20, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.BattleMage: return createHero(80,16,0,2,2,2,0,0,HeroDamageType.Mixed,24,0,0,0,0);
            case MultiClass.Beastmaster: return
                createHero(
                    75, // health, 
                    16, // mana, 
                    2, // healthRegen, 
                    0, // manaRegen, 
                    2, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    5, // physResist,
                    HeroDamageType.Physical, 22, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            // BlackKnight NOT IMPLEMENTED
            case MultiClass.BlackKnight: return
                createHero(
                    100, // health, 
                    20, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 20, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Bladedancer: return
                createHero(
                    78, // health, 
                    9, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 26, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    10, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Commander: return
                createHero(
                    90, // health, 
                    12, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    2, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 20, // attackType / attackDamage, 
                    20, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    10  // critChance
                );
            // Doomsayer NOT IMPLEMENTED
            case MultiClass.Doomsayer: return
                createHero(
                    100, // health, 
                    20, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Magic, 20, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Druid: return
                createHero(
                    90, // health, 
                    15, // mana, 
                    0, // healthRegen, 
                    2, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    5, // magicResist, 
                    5, // physResist,
                    HeroDamageType.Physical, 24, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Exorcist: return
                createHero(
                    60, // health, 
                    22, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    2, // physResist,
                    HeroDamageType.Physical, 20, // attackType / attackDamage, 
                    10, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.ForestProtector: return
                createHero(
                    110, // health, 
                    12, // mana, 
                    2, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 12, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );    
            case MultiClass.Gladiator: return createHero(90,10,0,0,0,0,0,0,HeroDamageType.Physical,28,0,2,0,0);
            case MultiClass.Illusionist: return createHero(20,10,0,2,0,0,0,0,HeroDamageType.Magic,10,0,0,0,0);
            case MultiClass.Inquisitor: return
                createHero(
                    76, // health, 
                    18, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    1, // physPower, 
                    10, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 10, // attackType / attackDamage, 
                    10, // shield, 
                    2, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Herald: return
                createHero(
                    75, // health, 
                    12, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 24, // attackType / attackDamage, 
                    24, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Hermit: return
                createHero(
                    70, // health, 
                    14, // mana, 
                    4, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    10, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 27, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Hunter: return
                createHero(
                    46, // health, 
                    10, // mana, 
                    2, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    5, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 20, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    20  // critChance
                );
            case MultiClass.Knight: return
                createHero(
                    80, // health, 
                    10, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    3, // physPower, 
                    2, // magicResist, 
                    5, // physResist,
                    HeroDamageType.Physical, 20, // attackType / attackDamage, 
                    20, // shield, 
                    3, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.MagicBard: return
                createHero(
                    54, // health, 
                    20, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    2, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 16, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    5, // evasionChance, 
                    0  // critChance
                );
            // Mimic NOT IMPLEMENTED
            case MultiClass.Mimic: return
                createHero(
                    100, // health, 
                    20, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Magic, 20, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Necromancer: return // createHero(20,10,0,2,0,0,0,0,HeroDamageType.Magic,10,0,0,0,0);
                createHero(
                    40, // health, 
                    15, // mana, 
                    0, // healthRegen, 
                    3, // manaRegen, 
                    1, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    5, // physResist,
                    HeroDamageType.Magic, 10, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            // Oracle NOT IMPLEMENTED
            case MultiClass.Oracle: return
                createHero(
                    100, // health, 
                    20, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Magic, 20, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Paladin: return createHero(100,15,0,0,0,2,0,5,HeroDamageType.Physical,20,10,0,0,0);
            // Predator NOT IMPLEMENTED
            case MultiClass.Predator: return
                createHero(
                    100, // health, 
                    10, // mana, 
                    2, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 28, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.PriestMaster: return
                createHero(
                    74, // health, 
                    15, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    2, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Physical, 18, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            // Runecaster NOT IMPLEMENTED
            case MultiClass.Runecaster: return
                createHero(
                    100, // health, 
                    20, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Magic, 20, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Samurai: return
                createHero(
                    90, // health, 
                    10, // mana, 
                    0, // healthRegen, 
                    2, // manaRegen, 
                    2, // magicPower, 
                    1, // physPower, 
                    3, // magicResist, 
                    3, // physResist,
                    HeroDamageType.Physical, 20, // attackType / attackDamage, 
                    0, // shield, 
                    2, // armor, 
                    0, // evasionChance, 
                    5  // critChance
                );
            // ShadowMaster NOT IMPLEMENTED
            case MultiClass.ShadowMaster: return
                createHero(
                    100, // health, 
                    20, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Magic, 20, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Shaman: return createHero(80,14,0,2,0,0,0,0,HeroDamageType.Magic,22,0,0,0,5);
            case MultiClass.Sorcerer: return
                createHero(
                    50, // health, 
                    20, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    4, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Magic, 20, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Warlock: return
                createHero(
                    36, // health, 
                    20, // mana, 
                    0, // healthRegen, 
                    2, // manaRegen, 
                    2, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Magic, 18, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            // Witch NOT IMPLEMENTED
            case MultiClass.Witch: return
                createHero(
                    100, // health, 
                    20, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    0, // magicPower, 
                    0, // physPower, 
                    0, // magicResist, 
                    0, // physResist,
                    HeroDamageType.Magic, 20, // attackType / attackDamage, 
                    0, // shield, 
                    0, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            case MultiClass.Zealot: return
                createHero(
                    70, // health, 
                    16, // mana, 
                    0, // healthRegen, 
                    0, // manaRegen, 
                    2, // magicPower, 
                    0, // physPower, 
                    5, // magicResist, 
                    5, // physResist,
                    HeroDamageType.Physical, 16, // attackType / attackDamage, 
                    0, // shield, 
                    4, // armor, 
                    0, // evasionChance, 
                    0  // critChance
                );
            default: {
                //Debug.Log("NO basic hero class found for multiClass: " + multiClass);
                //return null;
                return createHero(100,8,0,0,0,2,0,5,HeroDamageType.Physical,20,10,0,0,0);
            }
        }
    }

    public static BasicHero createHero(int health, int mana, int healthRegen, int manaRegen, int magicPower, int physPower, int magicResist, int physResist,
                                    HeroDamageType attackType, int attackDamage, int shield, int armor, int evasionChance, int critChance
    ) {
        BasicHero hero = new BasicHero();

        hero.health = health;
        hero.mana = mana;
        hero.healthRegen = healthRegen;
        hero.manaRegen = manaRegen;

        hero.magicPower = magicPower;
        hero.physPower = physPower;
        hero.magicResist = magicResist;
        hero.physResist = physResist;

        hero.attackType = attackType;
        hero.damage  = attackDamage;

        hero.shield = shield;
        hero.armor = armor;

        hero.evasionChance = evasionChance;
        hero.critChance = critChance;

        return hero;
    }
}
