using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : BasicHero
{
    public int maxHealth { get; set; }

    public bool isSummon { get; set; }
    
    public List<Buff> buffs { get; set; }
    public List<Status> statuses { get; set; }

    // --------------------------------------------------

    public Hero() : base()
    {
        isSummon = false;
        buffs = new List<Buff>();
        statuses = new List<Status>();
    }

    public static Hero copy(BasicHero basic)
    {
        Hero copy = new Hero();

        copy.heroClass = basic.heroClass;
        copy.name = basic.name;
        copy.health = basic.health;
        copy.mana = basic.mana;
        copy.healthRegen = basic.healthRegen;
        copy.manaRegen = basic.manaRegen;

        copy.magicPower = basic.magicPower;
        copy.physPower = basic.physPower;
        copy.magicResist = basic.magicResist;
        copy.physResist = basic.physResist;
        copy.attackType = basic.attackType;
        copy.damage = basic.damage;
        copy.shield = basic.shield;
        copy.armor = basic.armor;

        copy.evasionChance = basic.evasionChance;
        copy.evasionPower = basic.evasionPower;
        copy.critChance = basic.critChance;
        copy.critPower = basic.critPower;

        copy.activeSkill = basic.activeSkill;
        copy.battleSkill = basic.battleSkill;

        foreach(PassiveSkill passiveSkill in basic.passiveSkills) {
            copy.addPassiveSkill(passiveSkill);
        }

        foreach(HeroAttribute heroAttr in basic.attributes) {
            copy.attributes.Add(heroAttr);
        }

        foreach(Perk perk in basic.perks) {
            copy.perks.Add(perk);
        }

        copy.maxHealth = basic.health;

        return copy;
    }

    public Hero copy()
    {
        Hero copy = new Hero();

        copy.heroClass = heroClass;
        copy.name = name;
        copy.isSummon = isSummon;
        copy.health = health;
        copy.mana = mana;
        copy.healthRegen = healthRegen;
        copy.manaRegen = manaRegen;

        copy.magicPower = magicPower;
        copy.physPower = physPower;
        copy.magicResist = magicResist;
        copy.physResist = physResist;
        copy.attackType = attackType;
        copy.damage = damage;
        copy.shield = shield;
        copy.armor = armor;

        copy.evasionChance = evasionChance;
        copy.evasionPower = evasionPower;
        copy.critChance = critChance;
        copy.critPower = critPower;

        copy.activeSkill = activeSkill; // TODO need copy() here ??
        copy.battleSkill = battleSkill; // TODO need copy() here ??

        foreach(PassiveSkill passiveSkill in passiveSkills) {
            copy.addPassiveSkill(passiveSkill);
        }

        foreach(HeroAttribute heroAttr in attributes) {
            copy.attributes.Add(heroAttr);
        }
        foreach(Perk perk in perks) {
            copy.perks.Add(perk);
        }

        foreach(Buff buff in buffs) {
            copy.addBuff(buff.copy());
        }
        foreach(Status status in statuses) {
            copy.addStatus(status.copy());
        }

        copy.maxHealth = maxHealth;

        return copy;
    }

    // --------------------------------------------------

    public void addBuff(Buff _buff) {
        // TODO: when buff value perk also add value to same buff
        // if same buff refresh duration
        foreach(Buff buff in buffs) {
            if (buff.id == _buff.id) {
                buff.duration = _buff.duration;
                return;
            }
        }
        buffs.Add(_buff);
    }

    public void removeBuff(Buff buff) {
        buffs.Remove(buff);
    }

    public void addStatus(Status status) {
        statuses.Add(status);
    }

    public Status getStatus(StatusType statusType) {
        return statuses.Find(status => status.type == statusType);
    }

    public void removeStatus(StatusType statusType) {
        Status status = getStatus(statusType);
        statuses.Remove(status);
    }

    public int getAttr(Attr attr) {
        switch(attr) {
            case Attr.Damage: return damage;
            case Attr.Shield: return shield;
            case Attr.Armor: return armor;
            case Attr.Health: return health;
            case Attr.HealthRegen: return healthRegen;
            case Attr.Mana: return mana;
            case Attr.ManaRegen: return manaRegen;
            case Attr.MagicPower: return magicPower;
            case Attr.PhysPower: return physPower;
            case Attr.MagicResist: return magicResist;
            case Attr.PhysResist: return physResist;
            case Attr.CritChance: return critChance;
            case Attr.CritPower: return critPower;
            case Attr.EvasionChance: return evasionChance;
            case Attr.EvasionPower: return evasionPower;
        }
        return -1; // ERR
    }

    public void changeAttribute(Attr attr, int value) {
        changeAttribute(attr, value, false);
    }

    public void addAttribute(Attr attr, int value) {
        changeAttribute(attr, value, true);
    }

    public void removeDebuffs() {
        removeBuffs(BuffKind.Debuff);
    }

    public void removeBuffs(BuffType buffType) {
        List<Buff> typeBuffs = new List<Buff>();
        foreach(Buff buff in buffs) {
            if (buff.buffType == buffType) {
                typeBuffs.Add(buff);
            }
        }
        foreach(Buff buff in typeBuffs) {
           buffs.Remove(buff);
        }
    }

    public void removeBuffs() {
        removeBuffs(BuffKind.Buff);
    }

    public void takeDamage(int _damage, Logger l) {
        addAttribute(Attr.Health, -_damage);
        l.log(name + " " + health + " health left");
    }

    public int heal(int healValue) {
        if (health >= maxHealth) {
            healValue = 0;  // dont heal if current hp more than max hp (after swap)
        } else {
            healValue = (health + healValue > maxHealth) ? maxHealth - health : healValue;
        }
        addAttribute(Attr.Health, healValue);
        return healValue;
    }

    private void removeBuffs(BuffKind buffKind) {
        List<Buff> debuffs = new List<Buff>();
        foreach(Buff buff in buffs) {
            if (buff.buffKind == buffKind) {
                debuffs.Add(buff);
            }
        }
        foreach(Buff buff in debuffs) {
           buffs.Remove(buff);
        }
    }

}
