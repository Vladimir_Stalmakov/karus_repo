using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logger
{
    public string text { get; set; }

    public Logger() {
      text = "";  
    }

    public void log(string message) {
        text += " " + message + "."; 
    }
}
