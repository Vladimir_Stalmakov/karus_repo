using System.Collections;
using System.Collections.Generic;

public enum UpgradeType { Multiclass, HeroCount, PerkCount, AddHeroCount, AddPerkCount, HeroSelectCount, PerkSelectCount }



public class SelectUpgrade
{
    public UpgradeType type { get; set; }

    public int id { get; set; }
    public string descr { get; set; }

    public SelectUpgrade(UpgradeType _type, string description, int _id) {
        type = _type;
        descr = description;
        id = _id;
    }

    public SelectUpgrade copy() {
        SelectUpgrade copy = new SelectUpgrade(type, descr, id);
        return copy;
    }

    public void execute(Player player) {
        player.addUpgrade(this.copy());
        switch (type) {
            case UpgradeType.HeroCount: {
                player.heroPlaceCount++;
            }
            break;
            case UpgradeType.PerkCount: {
                player.perkCount++;
            }
            break;
            case UpgradeType.HeroSelectCount: {
                player.selectHeroCount++;
            }
            break;
            case UpgradeType.PerkSelectCount: {
                player.selectPerkCount++;
            }
            break;
            case UpgradeType.Multiclass: {
                player.isMulticlassEnabled = true;
            }
            break;
        }
    }
}

public static class UpgradeCollection {

    private static List<SelectUpgrade> upgrades;
    
    public static void init() {
        upgrades = new List<SelectUpgrade>();
        upgrades.Add(new SelectUpgrade(UpgradeType.HeroCount, "+1 hero", 1));
        upgrades.Add(new SelectUpgrade(UpgradeType.PerkCount, "+1 perk", 2));
        upgrades.Add(new SelectUpgrade(UpgradeType.HeroSelectCount, "+1 hero in select", 3));
        upgrades.Add(new SelectUpgrade(UpgradeType.PerkSelectCount, "+1 perk in select", 4));
        upgrades.Add(new SelectUpgrade(UpgradeType.Multiclass, "enable multiclass", 5));
    }

    public static List<SelectUpgrade> getUpgrades() {
        return upgrades;
    }
}
