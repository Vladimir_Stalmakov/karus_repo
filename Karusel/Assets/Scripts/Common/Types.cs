
public enum Attr { Damage, Health, Mana, HealthRegen, ManaRegen, Shield, Armor, MagicResist, PhysResist, MagicPower, PhysPower, 
                    CritChance, CritPower, EvasionChance, EvasionPower }

public enum HeroDamageType { Physical, Magic, Mixed }

public enum Upgrade { HeroPlaceCount4 , HeroPlaceCount5, Multiclass }

public enum DurationType { Round, Circle, NextDefaultAttack, Always }

public enum ValueType { Absolute, Percent }
