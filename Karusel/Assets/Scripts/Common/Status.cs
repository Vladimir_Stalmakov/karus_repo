using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatusType { Poison, Bleed, MagicShield }

public class Status
{
    public StatusType type { get; set; }
    public int duration { get; set; }
    public DurationType durationType { get; set; }
    public int maxDuration { get; set; }
    public int value { get; set; }
    public BuffKind kind { get; set; }

    public string name { get; set; }

    public Status(StatusSkill skill) {

        type = skill.statusType;
        duration = skill.duration;
        durationType = skill.durationType;
        maxDuration = skill.maxDuration;
        value = skill.absoluteValue;
        if (type == StatusType.MagicShield) {
            kind = BuffKind.Buff;
        } else {
            kind = BuffKind.Debuff;
        }
        name = ""+type;
    }

    public Status() {}

    public Status copy() {
        Status copy = new Status();
        copy.type = type;
        copy.duration = duration;
        copy.durationType = durationType;
        copy.maxDuration = maxDuration;
        copy.value = value;
        copy.kind = kind;
        copy.name = name;
        return copy;
    }

    public void refreshDuration() {
        duration = maxDuration + 1;
    }
}
