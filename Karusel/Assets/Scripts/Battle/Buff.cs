using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuffType { 
    Attribute,                  // increase attribute 
    AddAttrEveryRound,          // increase attribute every round 
    Combined, 
    Other, 
    AttackEveryRound,           // execute deafult attack every round
    HunterMark,                 // debuff increase chance of critical
    HealAfterAttacked,          // buff heals after hero was attacked
    AddNextSkillManaCost,       // debuff increase skill cost of next skill
    IgnoreShieldArmor           // ignore target shield and armor
}
public enum BuffKind { Buff, Debuff }

public class Buff
{
    public string id { get; set; }
    
    public BuffType buffType { get; set; }
    public BuffKind buffKind { get; set; }
    public int duration { get; set; }
    public DurationType durationType { get; set; }
    public int value { get; set; }
    public Attr attribute { get; set; }

    public string name { get; set; }

    public bool isCombined { get; set; }
    public List<Buff> combinedBuffs { get; set; }

    public Buff() {
       combinedBuffs = new List<Buff>();
    }
    
    public Buff(BuffSkill skill) {
        
        combinedBuffs = new List<Buff>();
        //isCombined = skill.isCombined;
        isCombined = skill.type == SkillType.CombinedBuff;
    
        //if (!isCombined) {
        //if (skill.type = SkillType.CombinedBuff)
        buffType = skill.buffType;
        buffKind = skill.buffKind;
        durationType = skill.durationType;
        duration = skill.duration + ((durationType == DurationType.Round) ? 1 : 0);
        attribute = skill.attribute;
        value = skill.absoluteValue;
        name = skill.name;
        id = "buff_"+name;
        //id = string.Join("", name.Split(default(string[]), System.StringSplitOptions.RemoveEmptyEntries));
        //} else {
        if (skill.type == SkillType.CombinedBuff) {
            foreach(BuffSkill cSkill in skill.combinedSkills) {
                addCombinedBuff(new Buff(cSkill));
            }
        }
    }

    public void addCombinedBuff(Buff buff) {
        combinedBuffs.Add(buff);
    }

    public Buff copy() {
        Buff copy = new Buff();
        copy.id = id;
        copy.buffType = buffType;
        copy.buffKind = buffKind;
        copy.duration = duration;
        copy.durationType = durationType;
        copy.attribute = attribute;
        copy.value = value;
        copy.name = name;
        copy.isCombined = isCombined;
        foreach(Buff cBuff in combinedBuffs) {
            copy.addCombinedBuff(cBuff);
        }
        return copy;
    }
}
