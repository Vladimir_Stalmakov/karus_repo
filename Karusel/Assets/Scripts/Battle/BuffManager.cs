using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BuffManager
{
    public static void removeBuffsAfterAttack(Hero hero, DurationType durationType, Logger l) {
        if(durationType == DurationType.NextDefaultAttack) {
            List<Buff> itemstoRemove = new List<Buff>();
            foreach(Buff buff in hero.buffs) {
                if(buff.durationType == DurationType.NextDefaultAttack) {
                    itemstoRemove.Add(buff);
                }
            }
            foreach(Buff buff in itemstoRemove) {
                l.log("remove buff <" + buff.name + "> from " + hero.name);
                hero.buffs.Remove(buff);
            }
        }
    }

    public static void checkBuffsAfterRound(Hero hero, Logger l, RoundResult roundResult) {
        List<Buff> buffsToRemove = new List<Buff>();
        foreach(Buff buff in hero.buffs) {
            l.log("----> BUFF " + buff.name + " on " + hero.name + " D: " + buff.duration);
            //Debug.Log("---------> BUFF " + buff.name + " on " + hero.name + " D: " + buff.duration);
            if (buff.durationType == DurationType.Round) {
                buff.duration--;
                if (buff.duration == 0) {
                    buffsToRemove.Add(buff);
                }
            }
        }

        foreach(Buff buff in buffsToRemove) {
            // RoundAction buffRemoveAction = new RoundAction(RoundActionType.RemoveBuff);
            // buffRemoveAction.actionHeroName = hero.name;
            // buffRemoveAction.buff = buff;
            // roundResult.addBuffRemoveAction(buffRemoveAction);

            hero.removeBuff(buff);
        }
    }

    public static void checkStatusesAfterRound(Hero hero, Logger l, RoundResult roundResult) {
        l.log("\n----> checkStatusesAfterRound " + hero.name);
        List<Status> statusesToRemove = new List<Status>();
        foreach(Status status in hero.statuses) {
            l.log("---------> STATUS " + status.name + " on " + hero.name + " D: " + status.duration);
            if (status.durationType == DurationType.Round) {
                status.duration--;
                if (status.duration == 0) {
                    statusesToRemove.Add(status);
                }
            }
            if (status.durationType == DurationType.Always && status.type == StatusType.Bleed) {
                status.duration--;
            }
        }

        foreach(Status status in statusesToRemove) {
            l.log("REmOVE STATUS " + status.name + " on " + hero.name);
            hero.removeStatus(status.type);
        }
        l.log("AFTEr REMOVE: ");
        foreach(Status status in hero.statuses) {
           l.log("---------> STATUS " + status.name +  " D: " + status.duration);
        }

    }

    // public static void checkBuffAfterRound(Buff buff, Hero hero, Logger l, RoundResult roundResult) {
    //     if (buff.durationType == DurationType.Round) {
    //         buff.duration--;
    //     }
        
    //     if (buff.duration == 0) {
    //         RoundAction buffRemoveAction = new RoundAction(RoundActionType.RemoveBuff);
    //         buffRemoveAction.actionHeroName = hero.name;
    //         buffRemoveAction.buff = buff;
    //         roundResult.addBuffRemoveAction(buffRemoveAction);

    //         hero.removeBuff(buff);
    //     }
    // }

    public static List<Buff> getAttrBuffs(Hero hero, Attr attr) {
        List<Buff> result = new List<Buff>();
        List<Buff> attrBuffs = getBuffs(hero, BuffType.Attribute);
        foreach(Buff buff in attrBuffs) {
            if (buff.attribute == attr) {
                result.Add(buff);
            }
        }
        return result;
    }

    public static List<Buff> getEveryRoundBuffs(Hero hero) {
        List<Buff> buffs = getBuffs(hero, BuffType.AddAttrEveryRound);
        buffs.AddRange(getBuffs(hero, BuffType.AttackEveryRound));
        return buffs;
    }

    public static List<Buff> getBuffs(Hero hero, BuffType buffType) {
       List<Buff> result = new List<Buff>();
       foreach(Buff buff in hero.buffs) {
           result.AddRange(getBuffs(buff, buffType));
       }
       return result;
    }

    public static bool hasBuff(Hero hero, BuffType buffType) {
       return getBuffs(hero, buffType).Count > 0;
    }

    public static List<Buff> getBuffs(Buff buff, BuffType buffType) {
       List<Buff> result = new List<Buff>();
       if (!buff.isCombined) {
           if (buff.buffType == buffType) {
               result.Add(buff);
           }
           return result;
       }
       
       foreach(Buff cBuff in buff.combinedBuffs) {
           result.AddRange(getBuffs(cBuff, buffType));
       }
       return result;
    }

    // ----------------------------------------

    public static void executeBuff(Hero hero, Buff buff, BattleCircle playerCircle, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {
        switch (buff.buffType) {
            case BuffType.AddAttrEveryRound: {
                 l.log(hero.name + " adds " + buff.value + " to" + buff.attribute);
                hero.addAttribute(buff.attribute, buff.value);
            }
            break;
            case BuffType.AttackEveryRound: {
                RoundAction action = new RoundAction(RoundActionType.DefaultAttack);
                FightManager.executeDefaultAttack(hero, playerCircle, enemyCircle, 100, l, roundResult, action);
                action.actionHeroName = hero.name;
                roundResult.addAction(action);
            }
            break;
            case BuffType.HealAfterAttacked: {
                int healValue = hero.heal(buff.value);

                RoundAction healAction = new RoundAction(RoundActionType.Heal);
                healAction.actionHeroName = Constants.HERO_NAME_NONE;
                healAction.value = healValue;
                healAction.targetHeroName = hero.name;
                healAction.targetCircleId = CircleId.Enemy;
                roundResult.addAction(healAction);
            }
            break;
        }
    }

    public static void executeStatus(Hero hero, Status status, Logger l, RoundResult roundResult) {
        if (status.type == StatusType.Poison) {
            FightManager.executeAttack(null, hero, status.value, HeroDamageType.Magic, l, roundResult, null);
            l.log(hero.name + " suffers from poison " + status.value); 
        }
        if (status.type == StatusType.Bleed) {
            if (status.duration == 1) {
                FightManager.executeAttack(null, hero, status.value, HeroDamageType.Magic, l, roundResult, null);
                l.log(hero.name + " suffers from bleed " + status.value);
                status.refreshDuration();
            }
        }
    }
    
}
