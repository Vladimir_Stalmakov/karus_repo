using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CircleId { Ally = 0, Enemy = 1 }

public class BattleCircle {

    public int heroPlaceCount { get; set; }

    public List<Hero> heroes { get; set; }
    public List<HeroPlace> places { get; set; }

    public BattleHeroPlace battlePlace { get; set; }
    public HeroPlace activePlace { get; set; }

    // ------ DEV

    public CircleId id { get; set; }     // player id = 0, enemy id = 1     

    Logger l { get; set; }
    private BattleResult battleResult { get; set; }

    // creates circle and put all heroes on the spots
    public BattleCircle(Player player, Logger logger, BattleResult _battleResult, CircleId _id) {
        id = _id;
        l = logger;
        battleResult = _battleResult;
        l.log("init circle placeCount: " + player.heroPlaceCount + ", heroCount: " + player.heroCount);
        // Debug.Log("-----------> 0 ");
        heroPlaceCount = player.heroPlaceCount;
        heroes = new List<Hero>();
        places = new List<HeroPlace>();

        // Debug.Log("-----------> 1 ");
        for (int i = 0; i < heroPlaceCount; i++) {
            //if (i < player.heroCount) {
                int heroIndex = i == 0 ? 0 : heroPlaceCount - i;
                //if (heroIndex < player.heroCount) {
                if (heroIndex < player.heroes.Count) {
                    // Hero hero = Hero.copy(player.heroes[i]);     // convert BasicHero to Hero here
                    BasicHero heroToCopy = player.heroes[heroIndex];
                    if (heroToCopy != null) {
                        Hero hero = Hero.copy(heroToCopy);        // convert BasicHero to Hero here
                        heroes.Add(hero);
                        places.Add(new HeroPlace(hero, i));
                        l.log("place " + hero.name + " on place " + i);
                    } else {
                        places.Add(new HeroPlace(i));
                    }
                } else {
                    places.Add(new HeroPlace(i));
                }
            //} else {
                //places.Add(new HeroPlace());
            //}
        }
        // Debug.Log("-----------> 2 ");
        for (int i = 0; i < heroPlaceCount; i++) {
            if (i != heroPlaceCount - 1) {
                places[i].nextPlace = places[i+1];
            } else {
                places[i].nextPlace = places[0];
            }
        }
        // Debug.Log("-----------> 3 ");
        activePlace = places[0];
        battlePlace = new BattleHeroPlace();

        places.Add(battlePlace);
    }

    public void rotate(RoundResult roundResult) {
        HeroPlace place0 = places[0].copy();
        for (int i = heroPlaceCount - 1; i > 0; i--) {
            moveToNextPlace(places[i]);
        }
        moveToNextPlace(place0);  
        
        if (battlePlace.isEmpty && activePlace.isEmpty) {
            roundResult.circleRotation.value++; // increase rotation value if more than 1
            rotate(roundResult);
        }
    } 

    public void moveToBattlePlace() {
        moveToPlace(activePlace, battlePlace);

    }

    private void moveToNextPlace(HeroPlace place) {
        moveToPlace(place, place.nextPlace);
    }

    public void moveToPlace(HeroPlace from, HeroPlace to) {
        to.isEmpty = from.isEmpty;
        to.hero = from.hero;
        from.setEmpty();
    }
}
