using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** calculations of damage dealt **/

public static class FightManager
{
    public static void executeDefaultAttack(HeroPlace heroPlace, BattleCircle playerCircle, BattleCircle enemyCircle, int attackPercent, Logger l, RoundResult roundResult, RoundAction action) {
        Hero hero = heroPlace.hero;
        executeDefaultAttack(hero, playerCircle, enemyCircle, attackPercent, l, roundResult, action);
    }

    public static void executeDefaultAttack(Hero hero, BattleCircle playerCircle, BattleCircle enemyCircle, int attackPercent, Logger l, RoundResult roundResult, RoundAction action) {
        Hero targetHero = enemyCircle.battlePlace.isEmpty ? enemyCircle.activePlace.hero : enemyCircle.battlePlace.hero;
        if (hero.hasPassiveSkill(PassiveSkillType.DefaultAttackActive)) {
            targetHero = enemyCircle.activePlace.isEmpty ? enemyCircle.battlePlace.hero : enemyCircle.activePlace.hero;
        }
        l.log(hero.name + " executes default attack (" + attackPercent + "%) on " + targetHero.name);
        executeAttack(hero, targetHero, attackPercent, l, roundResult, action);
    }
    
    /** calculate attack value from attacker parameters **/
    public static void executeAttack(Hero hero, Hero targetHero, int attackPercent, Logger l, RoundResult roundResult, RoundAction action) {

        HeroDamageType damageType = hero.attackType;
        int defaultAttackDamage = hero.damage;
        // multiclass heroes add MP or PP or both to default attack
        if (hero != null && hero.heroClass != null && hero.heroClass.isMulticlass) {
            switch(damageType) {
                case HeroDamageType.Magic: {
                    defaultAttackDamage += hero.magicPower;
                }
                break;
                case HeroDamageType.Physical: {
                    defaultAttackDamage += hero.physPower;
                }
                break;
                 case HeroDamageType.Mixed: {
                    defaultAttackDamage += hero.physPower + hero.magicPower;
                }
                break;
            }
        }

        float damageValue = hero.damage * attackPercent / 100;
        
        // TODO: calculate buffs and debuffs here
        damageValue = calculateBuffs(damageValue, hero, l);

        // check critical
        if (damageType == HeroDamageType.Physical || damageType == HeroDamageType.Mixed) {
            damageValue = calculateCritical(damageValue, hero, targetHero, l, action);
        }

        executeAttack(hero, targetHero, damageValue, damageType, l, roundResult, action);
    }

    // !!! in this variation AttackSkill cannot be critical
    // TODO: add buffs for AttackSkill

    /** calculate attack value afer defender parameters **/
    public static void executeAttack(Hero hero, Hero targetHero, float damageValue, HeroDamageType damageType, Logger l, RoundResult roundResult, RoundAction action) {
        
        if (action != null) {
            action.targetHeroName = targetHero.name;
        }
        
        // TODO: check target hero statuses (like MagicShield)

        damageValue = calculateStatuses(damageValue, targetHero, l);

        if (damageValue > 0) {
            // calculate damage after target hero defense attributes
            if (damageType == HeroDamageType.Physical || damageType == HeroDamageType.Mixed) {
                damageValue = calculateEvasion(damageValue, targetHero, l, action);
                damageValue = calculatePhysResist(damageValue, targetHero, l);
                damageValue = calculateShield(damageValue, hero, targetHero, l, action);
                damageValue = calculateArmor(damageValue, hero, targetHero);
            } 
            
            if (damageType == HeroDamageType.Magic || damageType == HeroDamageType.Mixed) {
                damageValue = calculateMagicResist(damageValue, targetHero, l);
            }

            int damage = (int)damageValue;
            if (action != null) {
                action.damage = damage;
            }

            // execute damage to target hero health
            l.log("deals " + damage + " damage");
            if (damage > 0) {
                //l.log(targetHero.name + " " + (targetHero.health - damage) + " health left");
                //targetHero.addAttribute(Attr.Health, -damage);
                targetHero.takeDamage(damage, l);
            }

            // check after attack buffs, statuses
            List<Buff> afterAttackBuffs = BuffManager.getBuffs(targetHero, BuffType.HealAfterAttacked);
            foreach(Buff buff in afterAttackBuffs) {
                BuffManager.executeBuff(targetHero, buff, null, null, l, roundResult);
            }
            
        }
    }

    public static float calculateCritical(float attackDamage, Hero hero, Hero targetHero, Logger l, RoundAction action) {
        
        int critChance = hero.critChance;

        // Debug.Log("calculateCritical --> " + hero.name + " critChance: " + critChance);

        // check buffs for critChance
        List<Buff> cirtBuffs = BuffManager.getAttrBuffs(hero, Attr.CritChance);
        foreach(Buff buff in cirtBuffs) {
            critChance += buff.value;
        }

        List<Buff> hunterMarks = BuffManager.getBuffs(targetHero, BuffType.HunterMark);
        foreach(Buff debuff in hunterMarks) {
            critChance += debuff.value;
        }
        
        // calculate if crit happened
        float random = Random.Range(0.0f, 1.0f);
        // Debug.Log("calculateCritical random: " + random + ", crit: " + critChance);
        bool isCritical = random * 100 < critChance;

        if (action != null) {
            action.isCritical = isCritical;
        }
        
        if (!isCritical) {
            return attackDamage;
        }
        
        float resultDamage = attackDamage;
        int critPower = hero.critPower;
        
        // check buffs for critPower
        List<Buff> critPowerBuffs = BuffManager.getAttrBuffs(hero, Attr.CritPower);
        foreach(Buff buff in critPowerBuffs) {
            critPower += buff.value;
        }
        
        
        resultDamage += resultDamage * critPower / 100;
        // Debug.Log("---------------> critPower: " + critPower + ", resultDamage: " + resultDamage);
        l.log("critical strike! resultDamage: " + resultDamage);
        return resultDamage;
    }

    public static int calculateBuffs(float attackDamage, Hero hero, Logger l) {
        int resultDamage = (int)attackDamage;
        List<Buff> damageBuffs = BuffManager.getAttrBuffs(hero, Attr.Damage);
        foreach(Buff buff in damageBuffs) {
            resultDamage += buff.value;
        }
        l.log("calculateBuffs initDamage: " + attackDamage + " resultDamage: " + resultDamage);
        return resultDamage;
    }

    public static int calculateStatuses(float attackDamage, Hero targetHero, Logger l) {
        int resultDamage = (int)attackDamage;
        Status magicShieldStatus = targetHero.getStatus(StatusType.MagicShield);
        if (magicShieldStatus != null) {
            l.log(targetHero.name + " magic shield absorbed damage. MagicShield removed");
            resultDamage = 0;
            targetHero.removeStatus(StatusType.MagicShield);
        }
        return resultDamage;
    }

    // calculate damage after physical resist
    public static float calculatePhysResist(float attackDamage, Hero targetHero, Logger l) {
        float resultDamage = calculateResist(attackDamage, targetHero, targetHero.physResist);
        l.log("calculatePhysResist resultDamage: " + resultDamage);
        return resultDamage;
    }

    // calculate damage after magical resist
    public static float calculateMagicResist(float attackDamage, Hero targetHero, Logger l) {
        float resultDamage = calculateResist(attackDamage, targetHero, targetHero.magicResist);
        l.log("calculateMagicResist MR: " + targetHero.magicResist + " resultDamage: " + resultDamage);
        return resultDamage;
    }

    public static float calculateResist(float attackDamage, Hero targetHero, int resist ) {
        float resultDamage = attackDamage;
        resultDamage = resultDamage - (resultDamage * resist / 100);
        return resultDamage;
    }

    // calculate damage after evasion
    public static float calculateEvasion(float attackDamage, Hero targetHero, Logger l, RoundAction action) {

        int evasionChance = targetHero.evasionChance;
        // check buffs for evasionChance
        List<Buff> evasionBuffs = BuffManager.getAttrBuffs(targetHero, Attr.EvasionChance);
        foreach(Buff buff in evasionBuffs) {
            evasionChance += buff.value;
        }

        // calculate if evasion happened
        float random = Random.Range(0.0f, 1.0f);
        bool isEvasion = random <= (float)(evasionChance) / 100;
        //l.log("evasion random: " +random + " chance: " + ((float)(evasionChance) / 100));
        // Debug.Log("calculateEvasion random: " + random + ", evasion: " + (evasionChance / 100));

        if (action != null) {
            action.isEvasion = isEvasion;
        }
        if (!isEvasion) {
            return attackDamage;
        }
        

        float resultDamage = attackDamage;
        int evasionPower = targetHero.evasionPower;
        
        // check buffs for evasionPower
        List<Buff> evasionPowerBuffs = BuffManager.getAttrBuffs(targetHero, Attr.EvasionPower);
        foreach(Buff buff in evasionPowerBuffs) {
            evasionPower += buff.value;
        }

        resultDamage -= resultDamage * evasionPower / 100;
        l.log("evasion! resultDamage: " + resultDamage);
        return resultDamage;
    }

    // calculate damage after shield
    public static float calculateShield(float attackDamage, Hero hero, Hero targetHero, Logger l, RoundAction action) {
        
        float resultDamage = attackDamage;
        int shield = targetHero.shield;
        // calculate shield from buffs and debuffs
        List<Buff> shieldBuffs = BuffManager.getAttrBuffs(targetHero, Attr.Shield);
        foreach(Buff buff in shieldBuffs) {
            shield += buff.value;
        }
        if (hero != null && BuffManager.hasBuff(hero, BuffType.IgnoreShieldArmor)) {
            shield = 0;
        }
        if (shield > 0) {
            if (attackDamage >= shield) {
                l.log("destroys " + shield + " shield");
                resultDamage = attackDamage - shield;
                targetHero.changeAttribute(Attr.Shield, 0);
                if (action != null) {
                    action.shieldDestroyed = shield;
                }
            } else {
                l.log("destroys " + (shield - (int)(attackDamage)) + " shield");
                resultDamage = 0;
                targetHero.addAttribute(Attr.Shield, (int)(-attackDamage));   // change all resultDamage to int ??
                if (action != null) {
                    action.shieldDestroyed = (shield - (int)(attackDamage));
                }
            }
        }
        return resultDamage;
    }

    // calculate damage after shield
    public static float calculateArmor(float attackDamage, Hero hero, Hero targetHero) {
        
        float resultDamage = attackDamage;
        int armor = targetHero.armor;
        // calculate armor from buffs and debuffs
        List<Buff> armorBuffs = BuffManager.getAttrBuffs(targetHero, Attr.Armor);
        foreach(Buff buff in armorBuffs) {
            armor += buff.value;
        }
        if (hero != null && BuffManager.hasBuff(hero, BuffType.IgnoreShieldArmor)) {
            armor = 0;
        }
        if (armor > 0) {
            resultDamage = attackDamage > armor ? attackDamage - armor : 0;
        }
        return resultDamage;
    }

}
