using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BattleManager
{
    public static Player player1 { get; set; }
    public static Player player2 { get; set; }

    public static BattleCircle circle1 { get; set; }
    public static BattleCircle circle2 { get; set; }

    public static void setPlayers(Player _player1, Player _player2) 
    {
        player1 = _player1;
        player2 = _player2;
    }

    public static BattleResult executeBattle() {

        BattleResult battleResult = new BattleResult();
        battleResult.heroPlaceCount1 = player1.heroPlaceCount;
        battleResult.heroPlaceCount2 = player2.heroPlaceCount;
        
        // ---- DEV ------
        Logger logger = new Logger();

        circle1 = new BattleCircle(player1, logger, battleResult, CircleId.Ally);
        circle2 = new BattleCircle(player2, logger, battleResult, CircleId.Enemy);

        // apply perks on heroes

        PerkManager.applyPerks(player1.perks, circle1.heroes);
        PerkManager.applyPerks(player2.perks, circle2.heroes);

        // foreach(Hero hero in circle1.heroes) {
        //     battleResult.heroes1.Add(hero.copy());
        // }
        // foreach(Hero hero in circle2.heroes) {
        //     battleResult.heroes2.Add(hero.copy());
        // }

        foreach(HeroPlace heroPlace in circle1.places) {
            battleResult.heroes1.Add(heroPlace.isEmpty ? null : heroPlace.hero.copy());
        }
        foreach(HeroPlace heroPlace in circle2.places) {
            battleResult.heroes2.Add(heroPlace.isEmpty ? null : heroPlace.hero.copy());
        }
        

        // ----------------------------------------------------
        
        float random = Random.Range(0.0f, 1.0f);
        battleResult.playerStarts = random <= 0.5f ? 0 : 1;

        bool isBattleFinished = false;
        int turn = battleResult.playerStarts;   // 0 - first player, 1 - second player
        int round = 0;
        int winner = 0;
        while(!isBattleFinished && round < 70) {
            // Debug.Log("Round " + round + " started.");
            round++;
            RoundResult roundResult = executeRound(turn, round, logger);
            battleResult.rounds.Add(roundResult);
            int resultAfterRound = checkBattleFinished();
            isBattleFinished = resultAfterRound != 0;

            // Debug.Log("Round " + round + "finished. isBattleFinished: " + isBattleFinished);

            if (isBattleFinished) {
                winner = resultAfterRound;
                logger.log("\n\n>>>>> Battle finsihed. Player " + winner + " wins!");
                // show heroes hp after finishing
                foreach (Hero hero in circle1.heroes) {
                    logger.log(hero.name + ": "+ hero.health);
                }
                foreach (Hero hero in circle2.heroes) {
                    logger.log(hero.name + ": "+ hero.health);
                }
                break;
            }

            // battle continues

            removeDeadHeroes(turn, logger, roundResult);

            // change turn
            if (!isBattleFinished) {
                turn = turn == 1 ? 0 : 1;
            }
        }

        battleResult.playerWins = winner;

        if (winner == 0) {
            battleResult.healthLost = 0;
        } else {
            BattleCircle winnerCircle = winner == 1 ? circle1 : circle2;
            int healthLost = 0;
            foreach (Hero hero in winnerCircle.heroes) {
                if (hero.health > 0) {
                    healthLost += hero.heroClass.isMulticlass ? 5 : 3;
                }
            }
            battleResult.healthLost = healthLost;
        }

        battleResult.descr = logger.text;
        return battleResult;
    }

    /** return 0 if not finished, 1/2 if all heroes on circle 1/2 dead **/ 
    public static int checkBattleFinished() {
        //Debug.Log("check circle 1 heroes");
        bool allDead = true;
        foreach (Hero hero in circle1.heroes) {
            //Debug.Log(hero.name + ": " + hero.health);
            if (hero.health > 0) {
                allDead = false;
                break;
            }
        }

        if (allDead) {
            return 2;
        }

        //Debug.Log("check circle 2 heroes");
        allDead = true;
        foreach (Hero hero in circle2.heroes) {
            //Debug.Log(hero.name + ": " + hero.health);
            if (hero.health > 0) {
                allDead = false;
                break;
            }
        }
        
        return allDead ? 1 : 0;
    }

    public static RoundResult executeRound(int turn, int round, Logger l) {

        RoundResult roundResult = new RoundResult(turn);

        Logger logger = new Logger();
        string log = "\n\n>> Round " + round + " (player "+turn+")";

        BattleCircle playerCircle = turn == 0 ? circle1 : circle2;
        BattleCircle enemyCircle = turn == 0 ? circle2 : circle1;

        // 1. rotate circle -------------------------------
        if (round > 2) {                // dont rotate on first round for each player (rounds number 1 and 2)
            log += " circle rotates.";
            playerCircle.rotate(roundResult);
            roundResult.circleRotation.isRotated = true;
        } else {
            if (playerCircle.heroes.Count == 0 || !playerCircle.activePlace.isEmpty) {
                log += " circle DOES NOT rotate.";
                roundResult.circleRotation.isRotated = false;
            } else {
                log += " circle rotates.";
                playerCircle.rotate(roundResult);
                roundResult.circleRotation.isRotated = true;
            }
        }

        // 2. move to battle pos -------------------------------
        bool isBattleSkill = false; // hero executes battle skill on battle position
        bool isActiveSkill = false; // hero executes active skill on active position
        bool isActiveSkillOnBattleHero = false; // hero executes active skill on battle position

        
        if (!playerCircle.activePlace.isEmpty) {
            if (playerCircle.battlePlace.isEmpty) {
                log += " move hero to battle place.";
                playerCircle.moveToBattlePlace();
                isBattleSkill = true;
                roundResult.circleRotation.isMovedToBattlePlace = true;
            } else {
                log += " dont move hero to battle place. Battleplace hero: " + playerCircle.battlePlace.hero.name;
                isActiveSkill = true;
            }
        } else {
            log += " active place empty.";
            if (!playerCircle.battlePlace.isEmpty) {
                isActiveSkillOnBattleHero = true;
            } else {
                log += " battle place empty.";
            }
        }

        // regen mana before action
        if (isBattleSkill || isActiveSkillOnBattleHero) {
            Hero hero = playerCircle.battlePlace.hero;
            regenerateMana(hero, logger);
        } else if (isActiveSkill) {
            Hero hero = playerCircle.activePlace.hero;
            regenerateMana(hero, logger);
        }   

        // 3. execute active or battle skill -------------------------------  
        if (isBattleSkill) {
            Hero hero = playerCircle.battlePlace.hero;
            log += " " + hero.name + " execute battle skill.";
            SkillManager.executeBattleSkill(playerCircle, enemyCircle, logger, roundResult);
        }

        if (isActiveSkill) {
            Hero hero = playerCircle.activePlace.hero;
            log += " " + hero.name + " execute active skill.";
            SkillManager.executeActiveSkill(hero, HeroPlaceType.Active, playerCircle, enemyCircle, logger, roundResult);
        }

        if (isActiveSkillOnBattleHero) {
            Hero hero = playerCircle.battlePlace.hero;
            log += " " + hero.name + " execute active skill on battle hero.";
            SkillManager.executeActiveSkill(hero, HeroPlaceType.Battle, playerCircle, enemyCircle, logger, roundResult);
        }

        // 4. execute default attack ------------------------------- 
        // TODO: some active skills cann't be followed by default attack
        
        if (isActiveSkill) {
            Hero hero = playerCircle.activePlace.hero;
            log += " "+ hero.name + " default attack.";
            // check and execute summon default attack instead
            if (hero.hasPassiveSkill(PassiveSkillType.SummonDefaultAttack) && 
            (!playerCircle.battlePlace.isEmpty && playerCircle.battlePlace.hero.isSummon)) {
                executeDefaultAttack(playerCircle.battlePlace, playerCircle, enemyCircle, roundResult, logger);
            } else {
                executeDefaultAttack(playerCircle.activePlace, playerCircle, enemyCircle, roundResult, logger);
            }
        } else if (isActiveSkillOnBattleHero || (isBattleSkill && round > 2)) {  // dont execute default attack on the first round of each player
            Hero hero = playerCircle.battlePlace.hero;
            log += " "+ hero.name + " default attack.";
            if (hero.hasPassiveSkill(PassiveSkillType.SummonDefaultAttack) && 
            (!playerCircle.activePlace.isEmpty && playerCircle.activePlace.hero.isSummon)) {
                executeDefaultAttack(playerCircle.activePlace, playerCircle, enemyCircle, roundResult, logger);
            } else {
                executeDefaultAttack(playerCircle.battlePlace, playerCircle, enemyCircle, roundResult, logger);
            }
        }

        // 5. execute totem skill
        BattleHeroPlace battlePlace = playerCircle.battlePlace;
        if (battlePlace.hasTotem) {
            SkillManager.executeTotemSkill(battlePlace, playerCircle, enemyCircle, logger, roundResult);
        }

        // 6. execute eachRound buffs, debuffs, statuses
        // 7. check and remove buffs, debuffs, statuses
        // 8. check and remove totems
        // !!!!! TODO: 8. !!!!

        foreach (HeroPlace heroPlace in playerCircle.places) {
            if (!heroPlace.isEmpty) {
                Hero hero = heroPlace.hero;
                // 6. 
                // execute buffs, debuffs
                foreach(Buff buff in BuffManager.getEveryRoundBuffs(hero)) {
                    BuffManager.executeBuff(hero, buff, playerCircle, enemyCircle, logger, roundResult);
                }
                // execute statuses
                foreach(Status status in hero.statuses) {
                    BuffManager.executeStatus(hero, status, logger, roundResult);
                }
                // 7. 
                // remove buffs, debuffs
                BuffManager.checkBuffsAfterRound(hero, logger, roundResult);
                // statuses
                BuffManager.checkStatusesAfterRound(hero, logger, roundResult);
                
                // 9. execute health regen
                regenerateHealth(hero, logger);
                
            }
        }
        // save heroes
        // player heroes
        List<Hero> copyHeroes = new List<Hero>();
        foreach (HeroPlace heroPlace in playerCircle.places) {
            if (!heroPlace.isEmpty) {
                copyHeroes.Add(heroPlace.hero.copy());
            }
        }
        roundResult.setHeroes(copyHeroes);
        // enemy heroes
        List<Hero> copyEnemyHeroes = new List<Hero>();
        foreach (HeroPlace heroPlace in enemyCircle.places) {
            if (!heroPlace.isEmpty) {
                copyEnemyHeroes.Add(heroPlace.hero.copy());
            }
        }
        roundResult.setEnemyHeroes(copyEnemyHeroes);


        l.log(log + "\n" + logger.text);
        return roundResult;
    }

    private static void removeDeadHeroes(int turn, Logger l, RoundResult roundResult) {

        foreach (HeroPlace place in circle1.places) {
            if (!place.isEmpty && place.hero.health <= 0) {
                // Debug.Log(">>>>>>> " + place.hero.name + " is DEAD. Removed from circle.");
                l.log(place.hero.name + " is dead. Removed from circle.");
                roundResult.addAction(RoundActionType.HeroDeath, place.hero.name, circle1.id);
                place.setEmpty();
            }
        }

        foreach (HeroPlace place in circle2.places) {
            if (!place.isEmpty && place.hero.health <= 0) {
                // Debug.Log(">>>>>>> " + place.hero.name + " is DEAD. Removed from circle.");
                l.log(place.hero.name + " is dead. Removed from circle.");
                roundResult.addAction(RoundActionType.HeroDeath, place.hero.name, circle2.id);
                place.setEmpty();
            }
        }
    }

    private static void executeDefaultAttack(HeroPlace heroPlace, BattleCircle playerCircle, BattleCircle enemyCircle, RoundResult roundResult, Logger logger) {
        
        Hero hero = heroPlace.hero;
        if (hero.hasPassiveSkill(PassiveSkillType.NoDefaultAttack)) {
            logger.log("passive: no default attack");
            return;
        }

        List<RoundAction> actions = new List<RoundAction>();
        if (hero.hasPassiveSkill(PassiveSkillType.DoubleAttack)) {
            //RoundAction action1 = executeDefaultAttack(heroPlace, hero.name, playerCircle, enemyCircle, 40, logger, roundResult);
            executeDefaultAttack(heroPlace, hero.name, playerCircle, enemyCircle, 40, logger, roundResult);
            //action1.actionHeroName = hero.name;
            //RoundAction action2 = executeDefaultAttack(heroPlace, hero.name, playerCircle, enemyCircle, 40, logger, roundResult);
            executeDefaultAttack(heroPlace, hero.name, playerCircle, enemyCircle, 40, logger, roundResult);
            //action2.actionHeroName = hero.name;
            //roundResult.addAction(action1);
            //roundResult.addAction(action2);
        } else {
            //RoundAction action = executeDefaultAttack(heroPlace, hero.name, playerCircle, enemyCircle, 100, logger, roundResult);
            executeDefaultAttack(heroPlace, hero.name, playerCircle, enemyCircle, 100, logger, roundResult);
            //action.actionHeroName = hero.name;
            //roundResult.addAction(action);
        }
        
        BuffManager.removeBuffsAfterAttack(heroPlace.hero, DurationType.NextDefaultAttack, logger);
    }

    private static void executeDefaultAttack(HeroPlace heroPlace, string heroName, BattleCircle playerCircle, BattleCircle enemyCircle, int attackPercent, 
                                                    Logger logger, RoundResult roundResult) {
        RoundAction action = new RoundAction(RoundActionType.DefaultAttack);
        action.actionHeroName = heroName;
        roundResult.addAction(action);
        FightManager.executeDefaultAttack(heroPlace, playerCircle, enemyCircle, attackPercent, logger, roundResult, action);
    }

    private static void regenerateHealth(Hero hero, Logger l) {
        int regenValue = hero.healthRegen;
        List<Buff> buffs = BuffManager.getAttrBuffs(hero, Attr.HealthRegen);
        foreach(Buff buff in buffs) {
            regenValue += buff.value;
        }
        if (regenValue != 0) {
            if (hero.health + regenValue > hero.maxHealth) {
                regenValue = hero.maxHealth - hero.health;
            }
            hero.addAttribute(Attr.Health, regenValue);
            l.log(hero.name + "regens " + regenValue + " HP");
        }
    }

    private static void regenerateMana(Hero hero, Logger l) {
        int regenValue = hero.manaRegen;
        List<Buff> buffs = BuffManager.getAttrBuffs(hero, Attr.ManaRegen);
        foreach(Buff buff in buffs) {
            regenValue += buff.value;
        }
        if (regenValue != 0) {
            hero.addAttribute(Attr.Mana, regenValue);
            l.log(hero.name + "regens " + regenValue + " Mana");
        }
    }
}

