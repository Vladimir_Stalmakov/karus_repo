using System.Collections;
using System.Collections.Generic;

public class BattleResult
{
    public int playerWins { get; set; }             // 1 or 2 or 0(draw)
    public int healthLost { get; set; }

    public int playerStarts { get; set; }           // 1 or 2 or

    public List<RoundResult> rounds { get; set; }


    //public Player player1 { get; set; }
    public int heroPlaceCount1 { get; set; }  
    public List<Hero> heroes1 { get; set; }

    //public Player player2 { get; set; }
    public int heroPlaceCount2 { get; set; }  
    public List<Hero> heroes2 { get; set; }
    

    public int heroCount { get; set; }
        
    

    // ----- DEV
    public string descr { get; set; }

    public BattleResult() {
        heroes1 = new List<Hero>();
        heroes2 = new List<Hero>();
        rounds = new List<RoundResult>();
        descr = "";
    }
}

public class RoundResult
{
    // TODO: store all data about actions in round
    public int turn { get; set; } // 0 = player, 1 = enemy
    public CircleRotation circleRotation { get; set; }
    public List<RoundAction> actions { get; set; }

    public List<RoundAction> buffRemoveActions { get; set; }

    public List<Hero> heroes { get; set; }
    public List<Hero> enemyHeroes { get; set; }


    public string descr { get; set; }

    // -- temp values ----------

    public CircleId tempCircleId { get; set; }

    public RoundResult(int _turn) {
        turn = _turn;
        descr = "";
        actions = new List<RoundAction>();
        circleRotation = new CircleRotation();

        buffRemoveActions = new List<RoundAction>();
    }

    public void addAction(RoundActionType actionType, string heroName, string skillName) {
        RoundAction action = new RoundAction(actionType, heroName);
        action.skillName = skillName;
        actions.Add(action);
    }

    public void addAction(RoundActionType actionType, string heroName) {
        RoundAction action = new RoundAction(actionType, heroName);
        actions.Add(action);
    }

    public void addAction(RoundAction action) {
        actions.Add(action);
    }

    public void addAction(RoundActionType actionType, string heroName, CircleId circleId) {
        RoundAction action = new RoundAction(actionType, heroName);
        action.circleId = circleId;
        actions.Add(action);
    }

    public void addAction(RoundActionType actionType, string heroName, int fromPlaceId, int targetPlaceId, CircleId circleId) {
        RoundAction action = new RoundAction(actionType, heroName);
        action.fromPlaceId = fromPlaceId;
        action.targetPlaceId = targetPlaceId;
        action.circleId = circleId;
        actions.Add(action);
    }

    public void addSummonAction(CircleId circleId, int targetPlaceId, Hero summon) {
        RoundAction summonAction = new RoundAction(RoundActionType.Summon);
        summonAction.circleId = circleId;
        summonAction.targetPlaceId = targetPlaceId;
        summonAction.summonHero = summon;
        actions.Add(summonAction);
    }

    public void addSummonTotemAction(CircleId circleId, int targetPlaceId, Totem totem) {
        RoundAction totemAction = new RoundAction(RoundActionType.SummonTotem);
        totemAction.circleId = circleId;
        totemAction.targetPlaceId = targetPlaceId;
        totemAction.totem = totem;
        actions.Add(totemAction);
    }

    public void addRemoveTotemAction(CircleId circleId, int targetPlaceId) {
        RoundAction totemAction = new RoundAction(RoundActionType.RemoveTotem);
        totemAction.circleId = circleId;
        totemAction.targetPlaceId = targetPlaceId;
        actions.Add(totemAction);
    }

    public void addBuffRemoveAction(RoundAction action) {
        buffRemoveActions.Add(action);
    }

    public void setHeroes(List<Hero> _heroes) {
        heroes = _heroes;
    }

    public void setEnemyHeroes(List<Hero> _heroes) {
        enemyHeroes = _heroes;
    }
}

public class CircleRotation {
    public bool isRotated { get; set; }
    public int value { get; set; } // how many places circke rotated (default to 1)
    public bool isMovedToBattlePlace { get; set; }

    public CircleRotation() {
        isRotated = false;
        isMovedToBattlePlace = false;
        value = 1;
    }
}

public enum RoundActionType { Skill, AttackSkill, DefaultAttack, Summon, HeroDeath, HeroMove, SummonTotem, RemoveTotem, Heal, Buff, RemoveBuff, Status, SwapHealth }

public class RoundAction {
    public RoundActionType type { get; set; }
    public Skill skill { get; set; }

    public Hero actionHero { get; set; } 
    public string actionHeroName { get; set; }
    public string skillName { get; set; } 

    public CircleId circleId { get; set; } = 0;             // circle action is performed from
    public CircleId targetCircleId { get; set; } = 0;       // circle action is targeted to

    // ------ for AttackSkill and DefaultAttack types

    public Hero targetHero { get; set; }
    public string targetHeroName { get; set; }

    public bool isCritical { get; set; }
    public bool isEvasion { get; set; }
    public int shieldDestroyed { get; set; }

    public int damage { get; set; }
    // public int healthLost { get; set; }
    // TODO: magic shields and so on

    // ------ for MoveHero type

    public int fromPlaceId { get; set; }
    public int targetPlaceId { get; set; }

    // ------ for Summon type

    public Hero summonHero { get; set; }

    // ------ for SummonTotem type

    public Totem totem { get; set; }

    // ------ for Heal type

    public int value { get; set; }

    // ------ for Buff type

    public Buff buff { get; set; }

    // ------ for Status type

    public Status status { get; set; }

    // ------ for SwapHealth type

    public int fromHealth { get; set; }     // result skill performing hero health
    public int targetHealth { get; set; }   // result target hero health

    // ----------------------------------------

    public RoundAction(RoundActionType actionType) {
        type = actionType;
    }

    public RoundAction(RoundActionType actionType, string heroName) {
        type = actionType;
        actionHeroName = heroName;
    }
}


