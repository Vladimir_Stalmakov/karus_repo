using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HeroPlaceType { Battle, Active }

public class HeroPlace {

    public bool isEmpty { get; set; }
    public Hero hero { get; set; }
    public HeroPlace nextPlace { get; set; }

    public int id { get; set; }

    public HeroPlace(Hero _hero, int _id) {
        hero = _hero;
        id = _id;
        isEmpty = false;
    }

    public HeroPlace(int _id) {
        id = _id;
        isEmpty = true;
    }

    public virtual HeroPlace copy() {
        HeroPlace copy = new HeroPlace(id);
        copy.isEmpty = isEmpty;
        copy.hero = hero;
        copy.nextPlace = nextPlace;
        return copy;
    }

    public void setEmpty() {
        isEmpty = true;
        hero = null;
    }

    public void setHero(Hero _hero) {
        if (_hero == null) {
            setEmpty();
            return;
        }
        isEmpty = false;
        hero = _hero;
    }
}

public class BattleHeroPlace : HeroPlace {

    public Totem totem { get; set; }
    public bool hasTotem { get; set; }

    public BattleHeroPlace() : base(8) {    // battlePlace id = 8
        hasTotem = false;
    }

    public void setTotem(Totem _totem) {
        // Debug.Log(">>>>>>>>> BattleHeroPlace setTotem");
        if (_totem == null) {
           Debug.Log(">>>>>>>>> setTotem NULL"); 
        }
        totem = _totem;
        hasTotem = true;
    }

    public void removeTotem() {
        totem = null;
        hasTotem = false;
    }

    public override HeroPlace copy() {
        // Debug.Log(">>>>>>>>> BattleHeroPlace COPY()");
        BattleHeroPlace copy = new BattleHeroPlace();
        copy.isEmpty = isEmpty;
        copy.hero = hero;
        copy.nextPlace = nextPlace;
        copy.totem = totem;
        copy.hasTotem = hasTotem;
        return copy;
    }
}
