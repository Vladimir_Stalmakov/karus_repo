
public enum SummonType { Preset, SelfCopy }

public class SummonSkill : Skill
{
    public SummonType summonType { get; set; }
    public Hero summon { get; set; }
    public PlaceType place { get; set; }

    public SummonSkill() : base()
    {
        summonType = SummonType.Preset;
        type = SkillType.Summon;
    }

    public override Skill copy() {
        SummonSkill copy = new SummonSkill();
        Skill.copyAll(this, copy);
        copy.summonType = summonType;
        copy.summon = summon; // need summon.copy() here ????
        copy.place = place;
        return copy;
    }
}
