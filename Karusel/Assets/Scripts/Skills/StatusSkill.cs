using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusSkill : Skill
{
    public StatusType statusType { get; set; }

    public int duration { get; set; }
    public DurationType durationType { get; set; }
    public int maxDuration { get; set; }            // set duration to maxDuration after Bleed executes
    
    public StatusSkill() : base()
    {
        type = SkillType.Status;
    }

    public override Skill copy() {
        StatusSkill copy = new StatusSkill();
        Skill.copyAll(this, copy);
        copy.statusType = statusType;
        copy.duration = duration;
        copy.maxDuration = maxDuration;
        copy.durationType = durationType;
        return copy;
    }
}
