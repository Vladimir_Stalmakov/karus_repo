using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SkillManager {


    public static void executeBattleSkill(BattleCircle playerCirlce, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {
        Hero hero = playerCirlce.battlePlace.hero;
        Skill skill = hero.battleSkill;
        if (skill == null) {
            l.log("no battle skill");
            return;
        }
        executeSkill(skill, hero, HeroPlaceType.Battle, playerCirlce, enemyCircle, l, roundResult);
    }

    public static void executeActiveSkill(Hero hero, HeroPlaceType placeType, BattleCircle playerCirlce, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {
        // Hero hero = playerCirlce.activePlace.hero;
        Skill skill = hero.activeSkill;
        if (skill == null) {
            l.log("no active skill");
            return;
        }
        executeSkill(skill, hero, placeType, playerCirlce, enemyCircle, l, roundResult);
    }

    public static void executeTotemSkill(BattleHeroPlace place, BattleCircle playerCirlce, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {
        if (!place.hasTotem) {
            Debug.Log("executeTotemSkill !hasTotem");
        } else if (place.totem == null) {
            Debug.Log("executeTotemSkill totem NULL");
        }
        Totem totem = place.totem;
        Skill skill = totem.skill;
        executeSkill(skill, null, HeroPlaceType.Battle, playerCirlce, enemyCircle, l, roundResult);
    }

    private static void executeSkill(Skill skill, Hero hero, HeroPlaceType placeType, BattleCircle playerCirlce, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {

        l.log("executeSkill <" + skill.name + ">");

        // check palce skill conditions
        foreach(SkillCondition condition in skill.conditions) {
            if (!isPlaceConditionMet(condition, placeType)) {
                l.log("place condition not met: " + condition.type);
                return;
            }
        }
        
        // check if enough mana for skill and pay mana cost
        if (!checkAndPayManaCost(skill, hero, l)) {
            return;
        }

        // check if skill includes several skills
        if (skill.type == SkillType.Combined) {
            l.log("Combined skill");
            // Debug.Log("Combined skill");
            foreach(Skill cSkill in skill.combinedSkills) {
                executeSkill(cSkill, hero, placeType, playerCirlce, enemyCircle, l, roundResult);
            }
            return;
        }

        // if multitarget devide skill into onetarget skills
        if (skill.target == SkillTargetType.AllAllies || skill.target == SkillTargetType.AllEnemies) {
            BattleCircle circle = skill.target == SkillTargetType.AllAllies ? playerCirlce : enemyCircle;
            roundResult.tempCircleId = skill.target == SkillTargetType.AllAllies ? CircleId.Ally : CircleId.Enemy;
            foreach (HeroPlace place in circle.places) {
                if (!place.isEmpty) {
                    Hero newTargetHero = place.hero;
                    executeSkill(skill, hero, newTargetHero, playerCirlce, enemyCircle, l, roundResult); // TODO": why do I need skill.copy() here ???
                }
            }
            return;
        }

        // if skill require no target
        if (skill.type == SkillType.Totem || skill.type == SkillType.Summon) {
            executeSkill(skill, hero, null, playerCirlce, enemyCircle, l, roundResult);
            return;
        }

        executeSkillWithTarget(skill, hero, playerCirlce, enemyCircle, l, roundResult);
    }

    private static void  executeSkillWithTarget(Skill skill, Hero hero, BattleCircle playerCirlce, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {
        // find skill target
        Hero targetHero = findTarget(skill, hero, playerCirlce, enemyCircle, roundResult);
        if (targetHero == null) {
            Debug.Log("No target found for skill. Skill skipped.");
            l.log("No target found for skill. Skill skipped.");
            return;
        }

        // check target skill conditions
        foreach(SkillCondition condition in skill.conditions) {
            if (!isTargetConditionMet(condition, hero, targetHero)) {
                l.log("target condition not met: " + condition.type);
                return;
            }
        }

        executeSkill(skill, hero, targetHero, playerCirlce, enemyCircle, l, roundResult);
    }

    private static void executeSkill(Skill skill, Hero hero, Hero targetHero, BattleCircle playerCirlce, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {
        // calculate skill value
        if (skill.type != SkillType.RemoveDebuffs) {
            skill.absoluteValue = getAbsoluteValue(skill, hero, targetHero);
        }

        if (roundResult != null) {
            if (hero == null) { // totem skill
                return;
            }

            if (skill.type == SkillType.Move || skill.type == SkillType.Summon || skill.type == SkillType.Totem || skill.type == SkillType.Buff 
                || skill.type == SkillType.Status) {
                
            } else {
                roundResult.addAction(RoundActionType.Skill, hero.name, skill.name);
                if (skill.type == SkillType.Summon) {
                    roundResult.addAction(RoundActionType.Summon, hero.name);
                }
            }
        }
        
        // execute
        switch(skill.type) {
            case SkillType.Buff: executeBuffSkill((BuffSkill)skill, hero, targetHero, playerCirlce, enemyCircle, l, roundResult);
            break;
            case SkillType.CombinedBuff: executeBuffSkill((BuffSkill)skill, hero, targetHero, playerCirlce, enemyCircle, l, roundResult);
            break;
            case SkillType.AddAttribute: executeAttrSkill((AddAttrSkill)skill, hero, targetHero, l);
            break;
            case SkillType.Status: executeStatusSkill((StatusSkill)skill, hero, targetHero, l, roundResult);
            break;
            case SkillType.Totem: executeSummonTotemSkill((TotemSkill)skill, hero, playerCirlce, enemyCircle, l, roundResult);
            break;
            case SkillType.RemoveTotem: executeRemoveTotemSkill((TotemSkill)skill, hero, playerCirlce, enemyCircle, l, roundResult);
            break;
            case SkillType.Attack: executeAttackSkill((AttackSkill)skill, hero, targetHero, l, roundResult);
            break;
            case SkillType.Heal: executeHealSkill((HealSkill)skill, hero, targetHero, l, roundResult);
            break;
            case SkillType.Move: executeMoveSkill((MoveSkill)skill, hero, targetHero, playerCirlce, enemyCircle, l, roundResult);
            break;
            case SkillType.Swap: executeMoveSkill((MoveSkill)skill, hero, targetHero, playerCirlce, enemyCircle, l, roundResult);
            break;
            case SkillType.Summon: executeSummmonSkill((SummonSkill)skill, hero, playerCirlce, enemyCircle, l, roundResult);
            break;
            case SkillType.RemoveDebuffs: executeRemoveDebuffSkill(skill, hero, targetHero, l, roundResult);
            break;
            case SkillType.RemoveBuffs: executeRemoveBuffSkill(skill, hero, targetHero, l, roundResult);
            break;
            case SkillType.SwapHealth: executeSwapHealthSkill(skill, hero, targetHero, l, roundResult);
            break;
        }
    }

    private static Hero findTarget(Skill skill, Hero hero, BattleCircle playerCirlce, BattleCircle enemyCircle, RoundResult roundResult) {

        switch(skill.target) {
            case SkillTargetType.Self: {
                roundResult.tempCircleId = CircleId.Ally;
                return hero;
            }
            case SkillTargetType.BattleEnemy: {
                roundResult.tempCircleId = CircleId.Enemy;
                if (!enemyCircle.battlePlace.isEmpty) {
                    return enemyCircle.battlePlace.hero;
                } else if (!enemyCircle.activePlace.isEmpty) {
                     return enemyCircle.activePlace.hero;
                } else {
                    Debug.Log("ERROR! findTarget not found with target type: " + skill.target);
                    return null; // ERR
                }
            }
            case SkillTargetType.ActiveEnemy: {
                roundResult.tempCircleId = CircleId.Enemy;
                if (enemyCircle.activePlace.isEmpty) {
                    Debug.Log("ERROR! findTarget not found with target type: " + skill.target);
                    return null;
                } 
                return enemyCircle.activePlace.hero;
            }
            case SkillTargetType.BattleAlly: {
                roundResult.tempCircleId = CircleId.Ally;
                return playerCirlce.battlePlace.hero;
            }
        }
        Debug.Log("ERROR! findTarget not found with target type: " + skill.target);
        return null; // ERR
    }

    // ---------------------------- BUFF SKILL ------------------------------------------------------

    private static void executeBuffSkill(BuffSkill skill, Hero hero, Hero targetHero, BattleCircle playerCirlce, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {
        l.log("execute Buff Skill " + skill.name + " on " + targetHero.name );
        
        if (skill.type == SkillType.CombinedBuff) {
            foreach(BuffSkill cSkill in skill.combinedSkills) {
                cSkill.absoluteValue = getAbsoluteValue(cSkill, hero, targetHero);
            }  
        }
        
        Buff buff = new Buff(skill);
        targetHero.addBuff(buff);

        RoundAction buffAction = new RoundAction(RoundActionType.Buff);
        buffAction.actionHeroName = hero == null ? Constants.HERO_NAME_TOTEM : hero.name;
        buffAction.buff = buff.copy();
        buffAction.targetHeroName = targetHero.name;
        buffAction.targetCircleId = roundResult.tempCircleId;
        roundResult.addAction(buffAction);
    }

    // ---------------------------- ATTRIBUTE SKILL ------------------------------------------------------

    private static void executeAttrSkill(AddAttrSkill skill, Hero hero, Hero targetHero, Logger l) {
        l.log("execute Attr Skill " + skill.attribute + " value: " + skill.absoluteValue);
        targetHero.addAttribute(skill.attribute, skill.absoluteValue);
    }

    // ---------------------------- STATUS SKILL ------------------------------------------------------

    private static void executeStatusSkill(StatusSkill skill, Hero hero, Hero targetHero, Logger l, RoundResult roundResult) {
        l.log("execute Status Skill " + skill.statusType + " value: " + skill.absoluteValue);
        Status status = new Status(skill);
        targetHero.addStatus(status);

        RoundAction statusAction = new RoundAction(RoundActionType.Status);
        statusAction.actionHeroName = hero == null ? Constants.HERO_NAME_TOTEM : hero.name;
        statusAction.status = status.copy();
        statusAction.targetHeroName = targetHero.name;
        statusAction.targetCircleId = roundResult.tempCircleId;
        roundResult.addAction(statusAction);
    }

    // ---------------------------- SUMMON TOTEM SKILL ------------------------------------------------------

    private static void executeSummonTotemSkill(TotemSkill skill, Hero hero, BattleCircle playerCirlce, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {
        l.log("execute Totem Skill " + skill.name + ". Totem placed");
        BattleHeroPlace heroPlace = skill.side == TotemSide.Ally ? playerCirlce.battlePlace : enemyCircle.battlePlace;
        
        // set absolute value for totem skills if scale from summoner stats
        Skill totemSkill = skill.totem.skill;
        if (totemSkill.type == SkillType.Combined) {
            foreach(Skill cSkill in totemSkill.combinedSkills) {
                if ((cSkill.valueType == SkillValueType.Percent || cSkill.valueType == SkillValueType.Combined) && cSkill.valueFrom.target == TargetType.Summoner) {
                    cSkill.absoluteValue = getAbsoluteValue(cSkill, hero, null);
                }
            }
        } else {
            if ((totemSkill.valueType == SkillValueType.Percent || totemSkill.valueType == SkillValueType.Combined) && totemSkill.valueFrom.target == TargetType.Summoner) {
                totemSkill.absoluteValue = getAbsoluteValue(totemSkill, hero, null);
            }
        }

        heroPlace.setTotem(skill.totem);
        roundResult.addSummonTotemAction(skill.side == TotemSide.Ally ? CircleId.Ally : CircleId.Enemy, heroPlace.id, skill.totem.copy());
    }

    // ---------------------------- REMOVE TOTEM SKILL ------------------------------------------------------

    private static void executeRemoveTotemSkill(TotemSkill skill, Hero hero, BattleCircle playerCirlce, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {
        l.log("execute Remove Totem Skill ");
        BattleHeroPlace heroPlace = skill.side == TotemSide.Ally ? playerCirlce.battlePlace : enemyCircle.battlePlace;
        heroPlace.removeTotem();
        roundResult.addRemoveTotemAction(skill.side == TotemSide.Ally ? CircleId.Ally : CircleId.Enemy, heroPlace.id);
    }

    // ---------------------------- ATTACK SKILL ------------------------------------------------------

    private static void executeAttackSkill(AttackSkill skill, Hero hero, Hero targetHero, Logger l, RoundResult roundResult) {
        l.log("execute Attack Skill " + skill.name + " on " + targetHero.name);
        RoundAction action = new RoundAction(RoundActionType.AttackSkill);
        action.actionHeroName = hero == null ? Constants.HERO_NAME_TOTEM : hero.name;
        FightManager.executeAttack(hero, targetHero, skill.absoluteValue, skill.attackType, l, roundResult, action);  /// TODO: move action from up
        roundResult.addAction(action);
    }

    // ---------------------------- HEAL SKILL ------------------------------------------------------

    private static void executeHealSkill(HealSkill skill, Hero hero, Hero targetHero, Logger l, RoundResult roundResult) {
        
        // int healValue = 0;
        // if (targetHero.health >= targetHero.maxHealth) {
        //     healValue = 0;  // dont heal if current hp more than max hp (after swap)
        // } else {
        //     healValue = (targetHero.health + skill.absoluteValue > targetHero.maxHealth) ? targetHero.maxHealth - targetHero.health : skill.absoluteValue;
        // }
        // l.log("execute Heal Skill " + skill.name + " on " + targetHero.name + " value: " + healValue);
        // targetHero.addAttribute(Attr.Health, healValue);
        int healValue = targetHero.heal(skill.absoluteValue);

        RoundAction healAction = new RoundAction(RoundActionType.Heal);
        healAction.actionHeroName = hero == null ? Constants.HERO_NAME_TOTEM : hero.name;
        healAction.value = healValue;
        healAction.targetHeroName = targetHero.name;
        roundResult.addAction(healAction);
    }

    // ---------------------------- MOVE SKILL ------------------------------------------------------

    private static void executeMoveSkill(MoveSkill skill, Hero hero, Hero targetHero, BattleCircle playerCirlce, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {
        Debug.Log("---------> executeMoveSkill");
        l.log("execute Move Skill from " + skill.fromPlace + " to " + skill.targetPlace + "\n");

        BattleCircle circle;

        circle = playerCirlce;
        // switch (skill.target) {
        //     case SkillTargetType.Self: circle = playerCirlce;
        //     break;
        //     case SkillTargetType.BattleAlly: circle = playerCirlce;
        //     break;
        //     case SkillTargetType.BattleEnemy: circle = enemyCircle;
        //     break;
        //     case SkillTargetType.ActiveEnemy: circle = enemyCircle;
        //     break;
        //     default: circle = playerCirlce;
        //     break;
        //     // TODO all cases
        // }

        // if (circle.battlePlace.hero == null) {
        //    l.log("circle.battle NULL"); 
        // } else {
        //    l.log("circle.battle " + circle.battlePlace.hero.name);  
        // }
        // if (circle.activePlace.hero == null) {
        //    l.log("circle.active NULL"); 
        // } else {
        //    l.log("circle.active " + circle.activePlace.hero.name);  
        // }

        // if (enemyCircle.battlePlace.hero == null) {
        //    l.log("enemyCircle.battle NULL"); 
        // } else {
        //    l.log("enemyCircle.battle " + enemyCircle.battlePlace.hero.name);  
        // }
        // if (enemyCircle.activePlace.hero == null) {
        //    l.log("enemyCircle.active NULL"); 
        // } else {
        //    l.log("enemyCircle.active " + enemyCircle.activePlace.hero.name);  
        // }

        HeroPlace fromPlace = getPlace(circle, enemyCircle, skill.fromPlace);
        HeroPlace targetPlace = getPlace(circle, enemyCircle, skill.targetPlace);

        Hero targetPlaceHero = null;
        if (skill.type == SkillType.Swap) {
            l.log("swap Skill!");
            targetPlaceHero = targetPlace.hero;
            if (targetPlaceHero == null) {
                l.log("targetPlaceHero NULL");
            } else {
                l.log("targetPlaceHero " + targetPlaceHero.name);
            }
        }
        
        circle.moveToPlace(fromPlace, targetPlace);

        if (skill.type == SkillType.Swap) {
            if (targetPlaceHero == null) {
                Debug.Log("Swap action target place hero is null");
                l.log("target place hero is null ");
            }
            l.log("set fromPlace");
            if (targetPlaceHero == null) {
                l.log("targetPlaceHero null");
            } else {
                l.log("targetPlaceHero hero: " + targetPlaceHero.name);
            }
            fromPlace.setHero(targetPlaceHero); 
        }
        CircleId circleId = getCircleId(skill.fromPlace);
        l.log("circleId: " + circleId);
        Debug.Log("circleId: " + circleId);
        roundResult.addAction(RoundActionType.HeroMove, hero.name, fromPlace.id, targetPlace.id, circleId);

        if (fromPlace.isEmpty) {
            l.log("fromPlace is empty");
        } else {
            l.log("fromPlace hero " + fromPlace.hero.name);
        }

        if (targetPlace.isEmpty) {
            l.log("targetPlace is empty");
        } else {
            l.log("targetPlace hero " + targetPlace.hero.name);
        }
    }

    // ---------------------------- SUMMON SKILL ------------------------------------------------------

    private static void executeSummmonSkill(SummonSkill skill, Hero hero, BattleCircle playerCirlce, BattleCircle enemyCircle, Logger l, RoundResult roundResult) {
        Debug.Log("executeSummmonSkill > p.id: " + playerCirlce.id + " en.id: " + enemyCircle.id);
        Hero summonHero;
        if (skill.summonType == SummonType.Preset) {
            summonHero = skill.summon;
        } else { //if (skill.summonType == SummonType.SelfCopy) {
            summonHero = hero.copy();
            summonHero.name = skill.summon.name;
        }
        l.log("execute Summon Skill " + summonHero.name);
        HeroPlace summonPlace;
        switch (skill.place) {
            case PlaceType.AllyBattle: summonPlace = playerCirlce.battlePlace;
            break;
            case PlaceType.AllyActive: summonPlace = playerCirlce.activePlace;
            break;
            default: summonPlace = playerCirlce.battlePlace;
            break;
            //TODO: all cases
        }
        summonPlace.setHero(summonHero.copy());
        roundResult.addSummonAction(CircleId.Ally, summonPlace.id, summonHero.copy());
    }

    // ---------------------------- REMOVE DEBUFFS SKILL ------------------------------------------------------

    private static void executeRemoveDebuffSkill(Skill skill, Hero hero, Hero targetHero, Logger l, RoundResult roundResult) {
        Debug.Log("execute RemoveDebuffSkill");
        l.log("execute Remove Debuff Skill on " + targetHero.name);      
        targetHero.removeDebuffs();
    }

    // ---------------------------- REMOVE BUFFS SKILL ------------------------------------------------------

    private static void executeRemoveBuffSkill(Skill skill, Hero hero, Hero targetHero, Logger l, RoundResult roundResult) {
        Debug.Log("execute RemoveBuffSkill");
        l.log("execute Remove Buff Skill on " + targetHero.name);      
        targetHero.removeBuffs();
    }

    // ---------------------------- SWAP HEALTH SKILL ------------------------------------------------------

    private static void executeSwapHealthSkill(Skill skill, Hero hero, Hero targetHero, Logger l, RoundResult roundResult) {
        Debug.Log("execute SwapHealth Skill");
        l.log("execute Swap Health Skill on " + targetHero.name);      
        int targetHeroHealth = targetHero.health;
        targetHero.changeAttribute(Attr.Health, hero.health);
        hero.changeAttribute(Attr.Health, targetHeroHealth);
        RoundAction action = new RoundAction(RoundActionType.SwapHealth);
        action.actionHeroName = hero.name;
        action.targetHeroName = targetHero.name;
        action.circleId = CircleId.Ally;
        action.targetCircleId = CircleId.Ally;
        action.fromHealth = hero.health;
        action.targetHealth = targetHero.health;
        roundResult.addAction(action);
    }

    // ------------------------------------------------------------------------------------------------

    private static HeroPlace getPlace(BattleCircle circle, BattleCircle enemyCircle, PlaceType placeType) {
        switch (placeType) {
            case PlaceType.AllyBattle: return circle.battlePlace;
            case PlaceType.AllyActive: return circle.activePlace;
            case PlaceType.EnemyBattle: return enemyCircle.battlePlace;
            case PlaceType.EnemyActive: return enemyCircle.activePlace;
            default: {
                Debug.Log("ERROR: Cannot find place for placeType" + placeType);
                return null;
            }
            // TODO all cases
        }
    }

    private static CircleId getCircleId(PlaceType placeType) {
        switch (placeType) {
            case PlaceType.AllyBattle: return CircleId.Ally;
            case PlaceType.AllyActive: return CircleId.Ally;
            case PlaceType.EnemyBattle: return CircleId.Enemy;
            case PlaceType.EnemyActive: return CircleId.Enemy;
            default: {
                Debug.Log("ERROR: Cannot find place for placeType" + placeType);
                return CircleId.Ally;
            }
            // TODO all cases
        }
    }

    // ------------------------------------------------------------------------------------------------

    /** convert percent of some hero attribute into absolute value **/
    private static int getAbsoluteValue(Skill skill, Hero hero, Hero targetHero) {
        // if absolute value already set return it
        if (skill.absoluteValue != -1) {
            return skill.absoluteValue;
        }
        //Debug.Log(">>> SKILL getAbsoluteValue " + skill.name);
        if (skill.valueType == SkillValueType.Absolute) {
            //Debug.Log(">>> return absolute " + skill.value);
            return skill.value;
        }

        if (skill.valueType == SkillValueType.Percent || skill.valueType == SkillValueType.Combined) {
            Hero target = null;
            CalculateFrom valueFrom = skill.valueFrom;
            switch(valueFrom.target) {
                case TargetType.Self: {
                    target = hero;
                }
                break;
                case TargetType.SkillTarget: {
                    target = targetHero;
                }
                break;
                case TargetType.Summoner: {
                    target = hero;
                }
                break;
                // TODO: all cases
            }

            if (target == null) {
                Debug.Log(">>> target == null RETURN -1 ");
                return -1; // ERR
            }

            int attrValue = target.getAttr(valueFrom.attr);
            // Debug.Log(">>> Attr: "+ valueFrom.attr + " : " + attrValue);

            int result = (int)(skill.value * attrValue / 100);

            if (skill.valueType == SkillValueType.Combined) {
                result += skill.value2;
            }
            //Debug.Log(">>> SUCCESS! result: "+result);
            return result;
        }
        //Debug.Log(">>> valueType "+skill.valueType+" not found. RETURN -1 ");
        return -1; // ERR
    }

    private static bool isPlaceConditionMet(SkillCondition condition, HeroPlaceType placeType) {
        switch(condition.type) {
            case SkillConditionType.OnBattlePlace: return placeType == HeroPlaceType.Battle;
            case SkillConditionType.OnActivePlace: return placeType == HeroPlaceType.Active;
        }
        return true;
    }

    private static bool isTargetConditionMet(SkillCondition condition, Hero hero, Hero targetHero) {
        switch(condition.type) {
            case SkillConditionType.TargetMagicDamageType: return targetHero.attackType == HeroDamageType.Magic || targetHero.attackType == HeroDamageType.Mixed;
            case SkillConditionType.TargetHealthLower: return targetHero.health < hero.health;
        }
        return true;
    }

    // check if enough mana for skill and pay mana cost
    private static bool checkAndPayManaCost(Skill skill, Hero hero, Logger l) {
        if (skill.manaCost > 0) {    
            int manaCost = skill.manaCost;
            // check mana cost debuffs
            List<Buff> costDebuffs = BuffManager.getBuffs(hero, BuffType.AddNextSkillManaCost);
            foreach(Buff debuff in costDebuffs) {
                manaCost += debuff.value;
            }
            // check if enough mana for skill
            bool enoughMana = hero.mana >= manaCost;
            if (!enoughMana) {
                l.log("not enough mana");
                return false;
            }
            // pay mana cost
            hero.addAttribute(Attr.Mana, -manaCost);
            l.log(hero.mana + " mana left");
            // remove mana cost debuffs
            hero.removeBuffs(BuffType.AddNextSkillManaCost);
            return true;
        }
        return true;
    }
}
