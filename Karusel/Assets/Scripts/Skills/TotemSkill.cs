using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TotemSide { Ally, Enemy }

public class TotemSkill : Skill
{
    public Totem totem { get; set; }
    public TotemSide side { get; set; }

    public TotemSkill() : base()
    {
        type = SkillType.Totem;
    }

    public override Skill copy() {
        // Debug.Log("------> TotemSkill copy");
        TotemSkill copy = new TotemSkill();
        Skill.copyAll(this, copy);
        copy.totem = totem.copy();
        copy.side = side;
        return copy;
    }
}
