using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackSkill : Skill
{
    public HeroDamageType attackType { get; set; }
    
    public AttackSkill() : base()
    {
        type = SkillType.Attack;
    }

    public override Skill copy() {
        AttackSkill copy = new AttackSkill();
        Skill.copyAll(this, copy);
        copy.attackType = attackType;
        return copy;
    }
}
