using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SkillCollection
{
    // private static Dictionary<string, Skill> skills;

    // private static List<string> skillIdsLevel1;
    // private static List<string> skillIdsLevel2;
    // private static List<string> skillIdsLevel3;

    public static Skill getSkill(HeroClass heroClass, SkillPositionType skillPosType) {
        //if (!heroClass.isMulticlass) {
        switch(heroClass.basicClass) {
            case BasicClass.War: return getWarriorSkill(skillPosType);
            case BasicClass.Wild: return getWildSkill(skillPosType);
            case BasicClass.Magic: return getMageSkill(skillPosType);
            case BasicClass.Master: return getMasterSkill(skillPosType);
            case BasicClass.Bard: return getBardSkill(skillPosType);
            case BasicClass.Dark: return getDarkSkill(skillPosType);
            case BasicClass.Priest: return getPriestSkill(skillPosType);
            case BasicClass.Order: return getOrderSkill(skillPosType);
            case BasicClass.Mystic: return getMysticSkill(skillPosType);
            default: {
                Debug.Log("ERROR! No skill found for class " + heroClass.basicClass);
                return null;
            }
        }
        //}
        //return new Skill();
    }

    public static Skill getSkill(MultiClass heroMultiClass, SkillPositionType skillPosType) {
        return MCSkillCollection.getSkill(heroMultiClass, skillPosType);
    }

    private static Skill getWarriorSkill(SkillPositionType skillPosType) {

        // +10 dmg to next def attack
        if (skillPosType == SkillPositionType.Active) {

            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "+10dmg def attack";
            skill.manaCost = 3;

            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 10;    

            skill.buffType = BuffType.Attribute;
            skill.attribute = Attr.Damage;
            skill.duration = 1;
            skill.durationType = DurationType.NextDefaultAttack;
            
            return skill;
        }

        // +50% shield
        if (skillPosType == SkillPositionType.Battle) {
            
            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "+50% shield";
            skill.attribute = Attr.Shield;
            skill.manaCost = 5;
            
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Percent;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.Shield);
            skill.value = 50;
            
            return skill;
        }

        return null;
    }

    private static Skill getWildSkill(SkillPositionType skillPosType) {

        // +2 dmg def attack
        if (skillPosType == SkillPositionType.Active) {
            
            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "+2 damage";
            skill.attribute = Attr.Damage;
            skill.manaCost = 2;
            
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 2;
            
            return skill;
        }

        // +10% dmg def attack, +2 hp regen
        if (skillPosType == SkillPositionType.Battle) {
            
            Skill skill = new Skill();
            skill.name = "+10% dmg, +2 hp regen";
            skill.manaCost = 5;

            //skill.isCombined = true;
            skill.type = SkillType.Combined;

            // 1. +10% dmg def attack
            AddAttrSkill skill1 = new AddAttrSkill();
            skill1.name = "+10% damage";
            skill1.attribute = Attr.Damage;
            
            skill1.target = SkillTargetType.Self;
            skill1.valueType = SkillValueType.Percent;
            skill1.valueFrom = new CalculateFrom(TargetType.Self, Attr.Damage);
            skill1.value = 10;
            
            skill.addCombinedSkill(skill1);

            // 2. +2 hp regen
            AddAttrSkill skill2 = new AddAttrSkill();
            skill2.name = "+2 hp regen";
            skill2.attribute = Attr.HealthRegen;
            
            skill2.target = SkillTargetType.Self;
            skill2.valueType = SkillValueType.Absolute;
            skill2.value = 2;

            skill.addCombinedSkill(skill2);
            
            // ------------------------------------------
            
            return skill;
        }

        return null;
    }

    private static Skill getMageSkill(SkillPositionType skillPosType) {
        // -10 magic resist debuff on enemy
        if (skillPosType == SkillPositionType.Active) {

            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "-10 magic resist";
            skill.attribute = Attr.MagicResist;
            skill.manaCost = 5;

            skill.target = SkillTargetType.BattleEnemy;
            skill.valueType = SkillValueType.Absolute;
            skill.value = -10;
            
            return skill;
        }
        // magic shield self
        if (skillPosType == SkillPositionType.Battle) {
            
            StatusSkill skill = new StatusSkill();
            skill.name = "magic shield";
            skill.manaCost = 8;
            
            skill.target = SkillTargetType.Self;
            skill.statusType = StatusType.MagicShield;
            skill.durationType = DurationType.Always;
            
            return skill;
        }
        return null;
    }

    private static Skill getMasterSkill(SkillPositionType skillPosType) {

        // +50 crit chance for next attack
        if (skillPosType == SkillPositionType.Active) {

            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "+50 crit next attack";
            skill.manaCost = 3;

            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 50;    

            skill.buffType = BuffType.Attribute;
            skill.attribute = Attr.CritChance;
            skill.duration = 1;
            skill.durationType = DurationType.NextDefaultAttack;
            
            return skill;
        }

        // +20 critPower 
        if (skillPosType == SkillPositionType.Battle) {
            
            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "+20 critPower";
            skill.attribute = Attr.CritPower;
            skill.manaCost = 5;
            
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 20;
            
            return skill;
        }

        return null;
    }

    private static Skill getDarkSkill(SkillPositionType skillPosType) {

        // -10% damage battle hero
        if (skillPosType == SkillPositionType.Active) {

            BuffSkill skill = new BuffSkill(BuffKind.Debuff);
            skill.name = "-10% damage";
            skill.manaCost = 4;

            skill.target = SkillTargetType.BattleEnemy;
            skill.valueType = SkillValueType.Percent;
            skill.valueFrom = new CalculateFrom(TargetType.SkillTarget, Attr.Damage);
            skill.value = 10;    

            skill.buffType = BuffType.Attribute;
            skill.attribute = Attr.Damage;
            skill.duration = 5;
            skill.durationType = DurationType.Round;
            
            return skill;
        }

        // summon orb (totem) to deal 3dmg every round
        if (skillPosType == SkillPositionType.Battle) {

            TotemSkill skill = new TotemSkill();
            skill.name = "totem, attacks 3dmg";
            skill.manaCost = 10;

            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 0;
            // totem
            Totem totem = new Totem();
            totem.name = "Dark Orb";
            totem.durationType = DurationType.Always;
            // skill for totem 
            AttackSkill totemSkill = new AttackSkill();
            totemSkill.name = "attack 3 dmg(m)";
            totemSkill.manaCost = 0;                    // !importatnt to set manaCost 0 
            totemSkill.attackType = HeroDamageType.Magic;
            totemSkill.target = SkillTargetType.BattleEnemy;                 
            totemSkill.valueType = SkillValueType.Absolute;
            totemSkill.value = 3;
            totem.skill = totemSkill;
            // ----
            skill.totem = totem;
             // ----
            return skill;
        }

        return null;
    }

    private static Skill getBardSkill(SkillPositionType skillPosType) {

        // +10% damage all allies
        if (skillPosType == SkillPositionType.Active) {

            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "+10% damage all";
            skill.manaCost = 6;

            skill.target = SkillTargetType.AllAllies;
            skill.valueType = SkillValueType.Percent;
            skill.valueFrom = new CalculateFrom(TargetType.SkillTarget, Attr.Damage);
            skill.value = 10;    

            skill.buffType = BuffType.Attribute;
            skill.attribute = Attr.Damage;
            skill.duration = 4;
            skill.durationType = DurationType.Round;
            
            return skill;
        }

        // +10 evasionChance all allies
        if (skillPosType == SkillPositionType.Battle) {

            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "+10 evasion all";
            skill.manaCost = 8;

            skill.target = SkillTargetType.AllAllies;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 10;    

            skill.buffType = BuffType.Attribute;
            skill.attribute = Attr.EvasionChance;
            skill.duration = 6;
            skill.durationType = DurationType.Round;
            
            return skill;
        }

        return null;
    }

    private static Skill getPriestSkill(SkillPositionType skillPosType) {
        
        // heal battle hero for x
        if (skillPosType == SkillPositionType.Active) {

            HealSkill skill = new HealSkill();
            skill.name = "heal 10";
            skill.manaCost = 4;

            skill.target = SkillTargetType.BattleAlly;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 10;
            
            return skill;
        }

        // buff self to increase dmg every round
        if (skillPosType == SkillPositionType.Battle) {

            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "+2 dmg every round";
            skill.manaCost = 8;

            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 2;    

            skill.buffType = BuffType.AddAttrEveryRound;
            skill.attribute = Attr.Damage;
            skill.duration = 8;
            skill.durationType = DurationType.Round;
            
            return skill;
        }

        return null;
    }

    private static Skill getOrderSkill(SkillPositionType skillPosType) {
        
        // buff self phys dmg reduction for 3 rounds
        if (skillPosType == SkillPositionType.Active) {

            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "+30 phys resist";
            skill.manaCost = 5;

            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 30;    

            skill.buffType = BuffType.Attribute;
            skill.attribute = Attr.PhysResist;
            skill.duration = 3;
            skill.durationType = DurationType.Round;
            
            return skill;
        }

        // add armor to attack
        if (skillPosType == SkillPositionType.Battle) {
            
            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "+add armor to attack";
            skill.attribute = Attr.Damage;
            skill.manaCost = 5;
            
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Percent;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.Armor);
            skill.value = 100;
            
            return skill;
        }

        return null;
    }

    private static Skill getMysticSkill(SkillPositionType skillPosType) {
        
        // buff +5 damage. TODO: if battle hero is summon, he executes default attack instead (TODO: if summon increase his dmg)
        if (skillPosType == SkillPositionType.Active) {

            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "buff +5 damage";
            skill.manaCost = 5;

            skill.target = SkillTargetType.BattleAlly;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 5;    

            skill.buffType = BuffType.Attribute;
            skill.attribute = Attr.Damage;
            skill.duration = 3;
            skill.durationType = DurationType.Round;
            
            return skill;
        }

        // summon creep and move self back to active
        if (skillPosType == SkillPositionType.Battle) {

            Skill skill = new Skill();
            skill.name = "summon ghost and move back";
            skill.manaCost = 10;

            //skill.isCombined = true;
            skill.type = SkillType.Combined;

            // 1. move self back to active
            MoveSkill skill1 = new MoveSkill();
            skill1.name = "move back";
            skill1.manaCost = 0;
            
            skill1.target = SkillTargetType.Self;
            skill1.fromPlace = PlaceType.AllyBattle;
            skill1.targetPlace = PlaceType.AllyActive;

            skill.addCombinedSkill(skill1);
            
            // 2. summon
            SummonSkill skill2 = new SummonSkill();
            skill2.name = "summon ghost";
            skill2.manaCost = 0;
            skill2.place = PlaceType.AllyBattle;
            // ------------- summon ---------------------
            Hero summonHero = Hero.copy(HeroFactory.createHero(30, 0, 0, 0, 0, 0, 0, 0, HeroDamageType.Physical, 10, 0, 0, 0, 0));
            summonHero.isSummon = true;
            summonHero.name = "ghost";
            summonHero.activeSkill = null;
            summonHero.battleSkill = null;
            skill2.summon = summonHero;
            // ------------- summon ---------------------
            skill.addCombinedSkill(skill2);

            // ------------------------------------------
            
            return skill;
        }

        return null;
    }
    
}
