using System.Collections;
using System.Collections.Generic;

public static class PassiveSkillCollection
{
    public static List<PassiveSkill> getSkills(BasicClass heroBasicClass) {
        switch(heroBasicClass) {
            case BasicClass.Mystic: return getMysticSkills();
            default: {
                return getNoSkills();
            }
        }
    }
    
    
    public static List<PassiveSkill> getSkills(MultiClass heroMultiClass) {
        switch(heroMultiClass) {
            case MultiClass.Assasin: return getAssasinSkills();
            case MultiClass.Barbarian: return getBarbarianSkills();
            case MultiClass.BattleMage: return getBattleMageSkills();
            case MultiClass.Bladedancer: return getBladedancerSkills();
            case MultiClass.ForestProtector: return getForestProtectorSkills();
            case MultiClass.Gladiator: return getGladiatorSkills();
            case MultiClass.Hunter: return getHunterSkills();
            case MultiClass.Illusionist: return getIllusionistSkills();
            case MultiClass.Paladin: return getPaladinSkills();
            case MultiClass.Samurai: return getSamuraiSkills();
            case MultiClass.Shaman: return getShamanSkills();
            default: {
                return getNoSkills();
                //Debug.Log("ERROR! No skill found for class " + heroMultiClass);
                //return null;
            }
        }
    }

    // BASIC CLASS

    private static List<PassiveSkill> getMysticSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        skills.Add(new PassiveSkill(PassiveSkillType.SummonDefaultAttack));
        return skills;
    }

    // MULTI CLASS

    private static List<PassiveSkill> getNoSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        return skills;
    }

    private static List<PassiveSkill> getPaladinSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        return skills;
    }

    private static List<PassiveSkill> getBarbarianSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        return skills;
    }

    private static List<PassiveSkill> getBattleMageSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        skills.Add(new PassiveSkill(PassiveSkillType.MixedAttackType));
        return skills;
    }

    private static List<PassiveSkill> getShamanSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        return skills;
    }

    private static List<PassiveSkill> getSamuraiSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        skills.Add(new PassiveSkill(PassiveSkillType.DoubleAttack));
        return skills;
    }
    

    private static List<PassiveSkill> getIllusionistSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        return skills;
    }

    private static List<PassiveSkill> getGladiatorSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        skills.Add(new PassiveSkill(PassiveSkillType.DoubleAttack));
        return skills;
    }

    private static List<PassiveSkill> getAssasinSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        skills.Add(new PassiveSkill(PassiveSkillType.NoDefaultAttack));
        return skills;
    }

    private static List<PassiveSkill> getHunterSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        skills.Add(new PassiveSkill(PassiveSkillType.DefaultAttackActive));
        return skills;
    }

    private static List<PassiveSkill> getForestProtectorSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        skills.Add(new PassiveSkill(PassiveSkillType.SummonDefaultAttack));
        return skills;
    }

    private static List<PassiveSkill> getBladedancerSkills() {
        List<PassiveSkill> skills = new List<PassiveSkill>();
        skills.Add(new PassiveSkill(PassiveSkillType.DoubleAttack));
        return skills;
    }

    
       
}
