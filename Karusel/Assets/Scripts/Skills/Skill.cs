using System.Collections;
using System.Collections.Generic;

public enum SkillPositionType { Active, Battle }

public enum SkillValueType { Absolute, Percent, Combined }

public enum SkillType { Buff, Attack, Heal, AddAttribute, Summon, Move, Swap, Totem, RemoveTotem, Combined, CombinedBuff, Status, RemoveDebuffs, RemoveBuffs, SwapHealth }

public enum SkillTargetType { Self, BattleEnemy, BattleAlly, ActiveEnemy, ActiveAlly, RandomAlly, RandomEnemy, AllAllies, AllEnemies, AllAlliesExceptBattle, AllEnemiesExceptBattle }

public enum percentValueFrom { Self, BattleEnemy, BattleAlly, ActiveEnemy, ActiveAlly, RandomAlly, RandomEnemy, AllAllies, AllEnemies, AllAlliesExceptBattle, AllEnemiesExceptBattle }


public class Skill
{
    public int manaCost { get; set; }

    public int value { get; set; }
    public int value2 { get; set; }                     // additional value for Combined SkillValueType
    public int absoluteValue { get; set; }
    public SkillValueType valueType { get; set; }
    public CalculateFrom valueFrom { get; set; }        // target+attr to calculate percent from 
    
    public List<Skill> combinedSkills { get; set; }

    public SkillPositionType positionType { get; set; }
    public SkillType type { get; set; }

    public SkillTargetType target { get; set; }

    public bool hasConditions { get; set; }
    public List<SkillCondition> conditions { get; set; }

    public string name { get; set; }
    public string descr { get; set; }

    public Skill() {
        valueType = SkillValueType.Absolute;
        value = 0;
        absoluteValue = -1; // -1 = value is not set yet
        combinedSkills = new List<Skill>();
        conditions = new List<SkillCondition>();
    }

    public void addCombinedSkill(Skill skill) {
        combinedSkills.Add(skill);
    }

    // public void addCondition(SkillCondition condition) {
    //     conditions.Add(condition);
    // }

    public void addCondition(SkillConditionType conditionType) {
        conditions.Add(new SkillCondition(conditionType));
    }

    public virtual Skill copy() {
        Skill copy = new Skill();
        copyAll(this, copy);
        return copy;
    }

    public static void copyAll(Skill from, Skill to) {
        to.value = from.value;
        to.value2 = from.value2;
        to.absoluteValue = from.absoluteValue;

        to.valueType = from.valueType;
        to.valueFrom = from.valueFrom;
        to.positionType = from.positionType;
        to.type = from.type;
        to.target = from.target;
        to.hasConditions = from.hasConditions;
        to.conditions = from.conditions;           //TODO: copy conditions ?
        to.descr = from.descr;

        List<Skill> combSkills = new List<Skill>();
        foreach(Skill skill in from.combinedSkills) {
            combSkills.Add(skill.copy());
        }
        to.combinedSkills = combSkills;
    }
}
