using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkillConditionType { 
    OnBattlePlace,                  // hero is on battle position    
    OnActivePlace,                  // hero is on active position 
    TargetMagicDamageType,          // target hero has HeroDamageType.Magic
    TargetHealthLower               // target hero has less health then skill performing hero
}

public class SkillCondition
{
    public SkillConditionType type { get; set; }

    public SkillCondition(SkillConditionType _type) {
        type = _type;
    }
}
