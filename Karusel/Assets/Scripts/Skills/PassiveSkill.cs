
public enum PassiveSkillType { 
    DoubleAttack,               // on default attack execute attack twice with 40% damage each
    MixedAttackType,            // default attack type is HeroDamageType.Mixed
    NoDefaultAttack,            // skip default attack
    DefaultAttackActive,        // execute default attack on active enemy instead battle enemy
    SummonDefaultAttack         // summon executes default attack instead of hero
}

public class PassiveSkill
{
    public PassiveSkillType type { get; set; }

    public PassiveSkill(PassiveSkillType _type) {
        type = _type;
    }
}
