using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlaceType { AllyBattle, AllyActive, AllyRandom, EnemyBattle, EnemyActive }

public class MoveSkill : Skill
{
    public PlaceType fromPlace { get; set; }
    public PlaceType targetPlace { get; set; }

    public MoveSkill() : base()
    {
        type = SkillType.Move;
    }

    public override Skill copy() {
        MoveSkill copy = new MoveSkill();
        Skill.copyAll(this, copy);
        copy.fromPlace = fromPlace;
        copy.targetPlace = targetPlace;
        return copy;
    }
}
