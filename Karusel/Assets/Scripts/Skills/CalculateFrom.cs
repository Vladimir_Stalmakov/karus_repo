using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TargetType { Self, SkillTarget, BattleEnemy, BattleAlly, DamageDone, HealthHealed, Summoner }

/** store data how to calculate relative skill value **/

public class CalculateFrom
{
    public TargetType target { get; set; }
    
    public Attr attr { get; set; }

    public CalculateFrom(TargetType _target) {
        target = _target;
    }

    public CalculateFrom(TargetType _target, Attr _attr) {
        target = _target;
        attr = _attr;
    }
}
