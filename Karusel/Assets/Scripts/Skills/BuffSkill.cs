
public class BuffSkill : Skill
{
    public BuffType buffType { get; set; }
    public BuffKind buffKind { get; set; }

    public int duration { get; set; }
    public DurationType durationType { get; set; }

    public Attr attribute { get; set; }
    
    public BuffSkill(BuffKind _buffKind) : base()
    {
        type = SkillType.Buff;
        buffKind = _buffKind;
    }

    public override Skill copy() {
        BuffSkill copy = new BuffSkill(buffKind);
        Skill.copyAll(this, copy);
        copy.buffType = buffType;
        copy.duration = duration;
        copy.durationType = durationType;
        copy.attribute = attribute;
        return copy;
    }
}
