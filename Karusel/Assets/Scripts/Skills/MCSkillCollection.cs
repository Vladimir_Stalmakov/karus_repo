using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* skill collection for multiclass heroes */

public static class MCSkillCollection
{
   public static Skill getSkill(MultiClass heroMultiClass, SkillPositionType skillPosType) {
        switch(heroMultiClass) {
            case MultiClass.Alchemist: return getAlchemistSkill(skillPosType);
            case MultiClass.Assasin: return getAssasinSkill(skillPosType);
            case MultiClass.Barbarian: return getBarbarianSkill(skillPosType);
            case MultiClass.BardMaster: return getBardMasterSkill(skillPosType);
            case MultiClass.BattleMage: return getBattleMageSkill(skillPosType);
            case MultiClass.Beastmaster: return getBeastmasterSkill(skillPosType);
            case MultiClass.BlackKnight: return getBlackKnightSkill(skillPosType);
            case MultiClass.Bladedancer: return getBladedancerSkill(skillPosType);
            case MultiClass.Commander: return getCommanderSkill(skillPosType);
             case MultiClass.Doomsayer: return getDoomsayerSkill(skillPosType);
            case MultiClass.Druid: return getDruidSkill(skillPosType);
            case MultiClass.Exorcist: return getExorcistSkill(skillPosType);
            case MultiClass.ForestProtector: return getForestProtectorSkill(skillPosType);
            case MultiClass.Gladiator: return getGladiatorSkill(skillPosType);
            case MultiClass.Illusionist: return getIllusionistSkill(skillPosType);
            case MultiClass.Inquisitor: return getInquisitorSkill(skillPosType);
            case MultiClass.Herald: return getHeraldSkill(skillPosType);
            case MultiClass.Hermit: return getHermitSkill(skillPosType);
            case MultiClass.Hunter: return getHunterSkill(skillPosType);
            case MultiClass.Knight: return getKnightSkill(skillPosType);
            case MultiClass.MagicBard: return getMagicBardSkill(skillPosType);
            case MultiClass.Mimic: return getMagicMimicSkill(skillPosType);
            case MultiClass.Necromancer: return getNecromancerSkill(skillPosType);
            case MultiClass.Oracle: return getOracleSkill(skillPosType);
            case MultiClass.Paladin: return getPaladinSkill(skillPosType);
            case MultiClass.Predator: return getPredatorSkill(skillPosType);
            case MultiClass.Runecaster: return getPredatorSkill(skillPosType);
            case MultiClass.PriestMaster: return getPriestMasterSkill(skillPosType);
            case MultiClass.Samurai: return getSamuraiSkill(skillPosType);
            case MultiClass.ShadowMaster: return getShadowMasterSkill(skillPosType);
            case MultiClass.Shaman: return getShamanSkill(skillPosType);
            case MultiClass.Sorcerer: return getSorcererSkill(skillPosType);
            case MultiClass.Warlock: return getWarlockSkill(skillPosType);
            case MultiClass.Witch: return getWitchSkill(skillPosType);
            case MultiClass.Zealot: return getZealotSkill(skillPosType);
            default: {
                return getPaladinSkill(skillPosType);
                //Debug.Log("ERROR! No skill found for class " + heroMultiClass);
                //return null;
            }
        }
    }

    private static Skill getPaladinSkill(SkillPositionType skillPosType) {
        // heal battle hero for x
        if (skillPosType == SkillPositionType.Active) {

            HealSkill skill = new HealSkill();
            skill.name = "heal 15+MP";
            skill.manaCost = 4;

            skill.target = SkillTargetType.BattleAlly;
            skill.valueType = SkillValueType.Combined;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill.value = 100;
            skill.value2 = 15;
            
            return skill;
        }
        // magic shield self
        if (skillPosType == SkillPositionType.Battle) {
            
            StatusSkill skill = new StatusSkill();
            skill.name = "holy shield";
            skill.manaCost = 8;
            
            skill.target = SkillTargetType.Self;
            skill.statusType = StatusType.MagicShield;
            skill.durationType = DurationType.Always;
            
            return skill;
        }

        return null;
    }

    private static Skill getBarbarianSkill(SkillPositionType skillPosType) {
        // +PP dmg 
        if (skillPosType == SkillPositionType.Active) {
            
            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "+PP damage";
            skill.attribute = Attr.Damage;
            skill.manaCost = 5;
            
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Percent;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.PhysPower);
            skill.value = 100;
            
            return skill;
        }
        // +20% dmg def attack, +4 hp regen
        if (skillPosType == SkillPositionType.Battle) {
            
            Skill skill = new Skill();
            skill.name = "+20% dmg, +4 hp regen";
            skill.manaCost = 5;
            skill.type = SkillType.Combined;

            // 1. +10% dmg def attack
            AddAttrSkill skill1 = new AddAttrSkill();
            skill1.name = "+20% damage";
            skill1.attribute = Attr.Damage;
            
            skill1.target = SkillTargetType.Self;
            skill1.valueType = SkillValueType.Percent;
            skill1.valueFrom = new CalculateFrom(TargetType.Self, Attr.Damage);
            skill1.value = 20;
            
            skill.addCombinedSkill(skill1);

            // 2. +4 hp regen
            AddAttrSkill skill2 = new AddAttrSkill();
            skill2.name = "+4 hp regen";
            skill2.attribute = Attr.HealthRegen;
            
            skill2.target = SkillTargetType.Self;
            skill2.valueType = SkillValueType.Absolute;
            skill2.value = 4;

            skill.addCombinedSkill(skill2);
            // ------------------------------------------
            return skill;
        }
        return null;
    }

    private static Skill getBattleMageSkill(SkillPositionType skillPosType) {
        // attack magic MP and physic PP
        if (skillPosType == SkillPositionType.Active) {
            Skill skill = new Skill();
            skill.name = "attack MP and PP";
            skill.manaCost = 6;

            //skill.isCombined = true;
            skill.type = SkillType.Combined;

            // 1. attack PP
            AttackSkill skill1 = new AttackSkill();
            skill1.name = "attack PP";
            skill1.manaCost = 0;
            skill1.attackType = HeroDamageType.Physical;
            skill1.target = SkillTargetType.BattleEnemy;                 
            skill1.valueType = SkillValueType.Percent;
            skill1.valueFrom = new CalculateFrom(TargetType.Self, Attr.PhysPower);
            skill1.value = 100;
            skill.addCombinedSkill(skill1);

            // 2. attack MP
            AttackSkill skill2 = new AttackSkill();
            skill2.name = "attack MP";
            skill2.manaCost = 0;
            skill2.attackType = HeroDamageType.Magic;
            skill2.target = SkillTargetType.BattleEnemy;                 
            skill2.valueType = SkillValueType.Percent;
            skill2.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill2.value = 100;
            skill.addCombinedSkill(skill2);
            // ------------------------------------------
            return skill;
        }
        // +5 MR and +5 PR
        if (skillPosType == SkillPositionType.Battle) {
            Skill skill = new Skill();
            skill.name = "+5 MR, +5 PR";
            skill.manaCost = 7;

            //skill.isCombined = true;
            skill.type = SkillType.Combined;

            // 1. +5 MR
            AddAttrSkill skill1 = new AddAttrSkill();
            skill1.name = "+5 MR";
            skill1.attribute = Attr.MagicResist;
            skill1.target = SkillTargetType.Self;
            skill1.valueType = SkillValueType.Absolute;
            skill1.value = 5;
            skill.addCombinedSkill(skill1);

            // 2. +5 PR
            AddAttrSkill skill2 = new AddAttrSkill();
            skill2.name = "+5 PR";
            skill2.attribute = Attr.PhysResist;
            skill2.target = SkillTargetType.Self;
            skill2.valueType = SkillValueType.Absolute;
            skill2.value = 5;
            skill.addCombinedSkill(skill2);
            // ------------------------------------------
            return skill;
        }
        return null;
    }
    

    private static Skill getShamanSkill(SkillPositionType skillPosType) {
        // buff MR, hpReg all allies 
        if (skillPosType == SkillPositionType.Active) {
            
            Skill skill = new Skill();
            skill.name = "buff MR, hpPreg all";
            skill.manaCost = 6;

            //skill.isCombined = true;
            skill.type = SkillType.Combined;

            // 1. buff +1 MR to all
            BuffSkill skill1 = new BuffSkill(BuffKind.Buff);
            skill1.name = "buff +1 MR";
            skill1.manaCost = 0;
            skill1.buffType = BuffType.Attribute;
            skill1.attribute = Attr.MagicResist;
            skill1.target = SkillTargetType.AllAllies;
            skill1.valueType = SkillValueType.Absolute;
            skill1.value = 1;
            skill1.duration = 2;
            skill1.durationType = DurationType.Round;
            
            skill.addCombinedSkill(skill1);

            // 2. +4 hp regen
            BuffSkill skill2 = new BuffSkill(BuffKind.Buff);
            skill2.name = "buff +4 hpRegen";
            skill2.manaCost = 0;
            skill2.buffType = BuffType.Attribute;
            skill2.attribute = Attr.HealthRegen;
            skill2.target = SkillTargetType.AllAllies;
            skill2.valueType = SkillValueType.Absolute;
            skill2.value = 4;
            skill2.duration = 2;
            skill2.durationType = DurationType.Round;

            skill.addCombinedSkill(skill2);
            
            // ------------------------------------------
            
            return skill;
        }
        // summon wooden totem (totem) to add shield every round
        if (skillPosType == SkillPositionType.Battle) {

            TotemSkill skill = new TotemSkill();
            skill.name = "totem, shield 4";
            skill.manaCost = 10;

            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 0;
            // totem
            Totem totem = new Totem();
            totem.name = "Wooden totem";
            totem.durationType = DurationType.Always;
            // skill for totem 
            AddAttrSkill totemSkill = new AddAttrSkill();
            totemSkill.name = "+4 shield";
            totemSkill.attribute = Attr.Shield;
            totemSkill.manaCost = 0;
            totemSkill.target = SkillTargetType.BattleAlly;
            totemSkill.valueType = SkillValueType.Absolute;
            totemSkill.value = 4;
            totem.skill = totemSkill;
            // ----
            skill.totem = totem;
             // ----
            return skill;
        }

        return null;
    }

    private static Skill getSamuraiSkill(SkillPositionType skillPosType) {
        // Apply Bleed PPx2
        if (skillPosType == SkillPositionType.Active) {
            
            StatusSkill skill = new StatusSkill();
            skill.name = "Apply Bleed PPx2";
            skill.manaCost = 4;
            skill.target = SkillTargetType.BattleEnemy;
            skill.statusType = StatusType.Bleed;
            skill.durationType = DurationType.Always;
            skill.duration = 2;
            skill.maxDuration = 2;
            skill.valueType = SkillValueType.Percent;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.PhysPower);
            skill.value = 200;
            
            return skill;
        }
        // buff shield, buff armor
        if (skillPosType == SkillPositionType.Battle) {

            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "10+PP SH, 2AR";
            skill.manaCost = 6;
            skill.type = SkillType.CombinedBuff;
            skill.buffType = BuffType.Combined;
            skill.duration = 4;
            skill.durationType = DurationType.Round;
            skill.target = SkillTargetType.Self;

            // 1. buff 10+PP shield
            BuffSkill skill1 = new BuffSkill(BuffKind.Buff);
            skill1.name = "buff 10+PP Shield";
            skill1.manaCost = 0;
            skill1.buffType = BuffType.Attribute;
            skill1.attribute = Attr.Shield;
            //skill1.target = SkillTargetType.Self;
            skill1.valueType = SkillValueType.Combined;
            skill1.valueFrom = new CalculateFrom(TargetType.Self, Attr.PhysPower);
            skill1.value = 100;
            skill1.value2 = 10;
            skill1.duration = 4;
            skill.addCombinedSkill(skill1);

            // 2. buff 2 armor
            BuffSkill skill2 = new BuffSkill(BuffKind.Buff);
            skill2.name = "buff +2 Armor";
            skill2.manaCost = 0;
            skill2.buffType = BuffType.Attribute;
            skill2.attribute = Attr.Armor;
            //skill2.target = SkillTargetType.Self;
            skill2.valueType = SkillValueType.Absolute;
            skill2.value = 2;
            skill.addCombinedSkill(skill2);
            //
            return skill;
        }

        return null;
    }
    
    private static Skill getIllusionistSkill(SkillPositionType skillPosType) {
        // buff MR, hpReg all allies 
        // +20% dmg def attack, +4 hp regen
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // summon self copy and move back on active
        if (skillPosType == SkillPositionType.Battle) {

            Skill skill = new Skill();
            skill.name = "summon ghost and move back";
            skill.manaCost = 10;

            //skill.isCombined = true;
            skill.type = SkillType.Combined;

            // 1. move self back to active
            MoveSkill skill1 = new MoveSkill();
            skill1.name = "move back";
            skill1.manaCost = 0;
            
            skill1.target = SkillTargetType.Self;
            skill1.fromPlace = PlaceType.AllyBattle;
            skill1.targetPlace = PlaceType.AllyActive;

            skill.addCombinedSkill(skill1);
            
            // 2. summon
            SummonSkill skill2 = new SummonSkill();
            skill2.name = "summon illusion";
            skill2.summonType = SummonType.SelfCopy;
            skill2.place = PlaceType.AllyBattle;
            skill2.manaCost = 0;

            // ------------- summon ---------------------
            Hero summonHero = new Hero();
            // summonHero.isSummon = true;
            summonHero.name = "Illusion";
            // summonHero.activeSkill = null;
            // summonHero.battleSkill = null;
            skill2.summon = summonHero;
            // ------------- summon ---------------------
            skill.addCombinedSkill(skill2);

            // ------------------------------------------
            
            return skill;
        }

        return null;
    }

    private static Skill getGladiatorSkill(SkillPositionType skillPosType) {
        // TODO: get battle place bonuses if on batlle place
        if (skillPosType == SkillPositionType.Active) {
            // temp: add PP to damage
            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "+PP damage";
            skill.attribute = Attr.Damage;
            skill.manaCost = 4;
            
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Percent;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.PhysPower);
            skill.value = 100;

            skill.addCondition(SkillConditionType.OnBattlePlace);
            
            return skill;
        }
        // buff self to increase PP every round
        if (skillPosType == SkillPositionType.Battle) {
            
            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "+1 PP every round";
            skill.manaCost = 8;

            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 1;    

            skill.buffType = BuffType.AddAttrEveryRound;
            skill.attribute = Attr.PhysPower;
            skill.duration = 0;
            skill.durationType = DurationType.Always;
            
            return skill;
        }
        return null;
    }

    private static Skill getNecromancerSkill(SkillPositionType skillPosType) {
        // attack both battle and active heroes
        if (skillPosType == SkillPositionType.Active) {
            
            Skill skill = new Skill();
            skill.name = "attack A+B heroes";
            skill.manaCost = 5;
            skill.type = SkillType.Combined;

            // 1. attack battle hero
            AttackSkill skill1 = new AttackSkill();
            skill1.name = "attack B";
            skill1.manaCost = 0;
            skill1.attackType = HeroDamageType.Magic;
            skill1.target = SkillTargetType.BattleEnemy;                 
            skill1.valueType = SkillValueType.Combined;
            skill1.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill1.value = 100;
            skill1.value2 = 14;
            skill.addCombinedSkill(skill1);

            // 2. attack active hero
            AttackSkill skill2 = new AttackSkill();
            skill2.name = "attack A";
            skill2.manaCost = 0;
            skill2.attackType = HeroDamageType.Magic;
            skill2.target = SkillTargetType.ActiveEnemy;                 
            skill2.valueType = SkillValueType.Combined;
            skill2.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill2.value = 100;
            skill2.value2 = 14;
            skill.addCombinedSkill(skill2);
            // ------------------------------------------
            return skill;
        }
        // summon skeleton and move self back to active
        if (skillPosType == SkillPositionType.Battle) {

            Skill skill = new Skill();
            skill.name = "summon skeleton and move back";
            skill.manaCost = 10;
            skill.type = SkillType.Combined;

            // 1. move self back to active
            MoveSkill skill1 = new MoveSkill();
            skill1.name = "move back";
            skill1.manaCost = 0;
            
            skill1.target = SkillTargetType.Self;
            skill1.fromPlace = PlaceType.AllyBattle;
            skill1.targetPlace = PlaceType.AllyActive;

            skill.addCombinedSkill(skill1);
            
            // 2. summon
            SummonSkill skill2 = new SummonSkill();
            skill2.name = "summon skeleton";
            skill2.manaCost = 0;
            skill2.place = PlaceType.AllyBattle;
            // ------------- summon ---------------------
            Hero summonHero = Hero.copy(HeroFactory.createHero(30, 0, 0, 0, 0, 0, 0, 0, HeroDamageType.Physical, 14, 10, 0, 0, 0));
            summonHero.isSummon = true;
            summonHero.name = "skeleton";
            summonHero.activeSkill = null;
            summonHero.battleSkill = null;
            skill2.summon = summonHero;
            // ------------- summon ---------------------
            skill.addCombinedSkill(skill2);

            // ------------------------------------------
            
            return skill;
        }
        return null;
    }

    private static Skill getKnightSkill(SkillPositionType skillPosType) {
        // swap self with battle hero
        if (skillPosType == SkillPositionType.Active) {
            
            MoveSkill skill = new MoveSkill();
            skill.name = "swap with battle";
            skill.manaCost = 10;
            skill.type = SkillType.Swap;
            skill.target = SkillTargetType.Self;
            skill.fromPlace = PlaceType.AllyActive;
            skill.targetPlace = PlaceType.AllyBattle;
            skill.addCondition(SkillConditionType.OnActivePlace);
            return skill;
        }
        // get 20 shield and 2 armor
        if (skillPosType == SkillPositionType.Battle) {

            Skill skill = new Skill();
            skill.name = "summon skeleton and move back";
            skill.manaCost = 10;
            skill.type = SkillType.Combined;

            // 1. move self back to active
            AddAttrSkill skill1 = new AddAttrSkill();
            skill1.name = "+20 shield";
            skill1.attribute = Attr.Shield;
            skill1.manaCost = 0;
            
            skill1.target = SkillTargetType.Self;
            skill1.valueType = SkillValueType.Absolute;
            // skill1.valueFrom = new CalculateFrom(TargetType.Self, Attr.PhysPower);
            skill1.value = 20;
            
            skill.addCombinedSkill(skill1);
            
            // 2. summon
            AddAttrSkill skill2 = new AddAttrSkill();
            skill2.name = "+2 armor";
            skill2.attribute = Attr.Armor;
            skill2.manaCost = 0;
            
            skill2.target = SkillTargetType.Self;
            skill2.valueType = SkillValueType.Absolute;
            // skill1.valueFrom = new CalculateFrom(TargetType.Self, Attr.PhysPower);
            skill2.value = 2;

            skill.addCombinedSkill(skill2);

            // ------------------------------------------
            
            return skill;
        }
        return null;
    }

    private static Skill getWarlockSkill(SkillPositionType skillPosType) {
        // poison active hero
        if (skillPosType == SkillPositionType.Active) {
            
            StatusSkill skill = new StatusSkill();
            skill.name = "poison A";
            skill.manaCost = 4;
            
            skill.target = SkillTargetType.ActiveEnemy;
            skill.statusType = StatusType.Poison;
            skill.durationType = DurationType.Round;
            skill.duration = 2;

            skill.valueType = SkillValueType.Percent;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill.value = 100;
            
            return skill;
        }
        // debuff B and A for DA misses
        if (skillPosType == SkillPositionType.Battle) {

            Skill skill = new Skill();
            skill.name = "debuff B and A DA misses";
            skill.manaCost = 10;
            skill.type = SkillType.Combined;

            // 1. debuff B for DA misses
            BuffSkill skill1 = new BuffSkill(BuffKind.Debuff);
            skill1.name = "-50% damage";
            skill1.attribute = Attr.Damage;
            skill1.manaCost = 0;
            skill1.durationType = DurationType.Round;
            skill1.duration = 4;
            
            skill1.target = SkillTargetType.BattleEnemy;
            skill1.valueType = SkillValueType.Percent;
            skill1.valueFrom = new CalculateFrom(TargetType.SkillTarget, Attr.Damage);
            skill1.value = -50;
            
            skill.addCombinedSkill(skill1);
            
            // 2. debuff A for DA misses
            BuffSkill skill2 = new BuffSkill(BuffKind.Debuff);
            skill2.name = "-50% damage";
            skill2.attribute = Attr.Damage;
            skill2.manaCost = 0;
            skill2.durationType = DurationType.Round;
            skill2.duration = 4;
            
            skill2.target = SkillTargetType.ActiveEnemy;
            skill2.valueType = SkillValueType.Percent;
            skill2.valueFrom = new CalculateFrom(TargetType.SkillTarget, Attr.Damage);
            skill2.value = -50;

            skill.addCombinedSkill(skill2);

            // ------------------------------------------
            
            return skill;
        }
        return null;
    }
    
    private static Skill getDruidSkill(SkillPositionType skillPosType) {
        // descrese armor by 3+MP
        if (skillPosType == SkillPositionType.Active) {

            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "-(3+MP) armor";
            skill.attribute = Attr.Armor;
            skill.manaCost = 4;
            
            skill.target = SkillTargetType.BattleEnemy;
            skill.valueType = SkillValueType.Combined;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill.value = -100;
            skill.value2 = -3;
            
            return skill;
        }
        // swap enemy B and A heroes
        if (skillPosType == SkillPositionType.Battle) {
            
            MoveSkill skill = new MoveSkill();
            skill.name = "swap enemy B and A";
            skill.manaCost = 10;
            skill.type = SkillType.Swap;
            skill.target = SkillTargetType.BattleEnemy;
            skill.fromPlace = PlaceType.EnemyBattle;
            skill.targetPlace = PlaceType.EnemyActive;
            return skill;
        }
        return null;
    }

    private static Skill getHermitSkill(SkillPositionType skillPosType) {
        // clear debuff from B and A
        if (skillPosType == SkillPositionType.Active) {

            Skill skill = new Skill();
            skill.name = "clear debuff AB";
            skill.manaCost = 5;
            skill.type = SkillType.Combined;

            // 1. clear debuff from B
            Skill skill1 = new Skill();
            skill1.name = "clear debuff";
            skill1.type = SkillType.RemoveDebuffs;
            skill1.target = SkillTargetType.BattleAlly;
            skill.addCombinedSkill(skill1);

            // 2. clear debuff from A
            Skill skill2 = new Skill();
            skill2.name = "clear debuff";
            skill2.type = SkillType.RemoveDebuffs;
            skill2.target = SkillTargetType.ActiveAlly;
            skill.addCombinedSkill(skill2);
            // ------------------------------------------
            return skill;
        }
        // buff hpRegen self
        if (skillPosType == SkillPositionType.Battle) {
            
            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "buff 4+PP HPreg";
            skill.manaCost = 8;
            skill.buffType = BuffType.Attribute;
            skill.attribute = Attr.HealthRegen;
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Combined;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.PhysPower);
            skill.value = 100;
            skill.value2 = 4;
            skill.duration = 4;
            skill.durationType = DurationType.Round;
            return skill;
        }
        return null;
    }

    private static Skill getInquisitorSkill(SkillPositionType skillPosType) {
        // remove mana from magic dmg type hero
        if (skillPosType == SkillPositionType.Active) {

            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "-(2+PP) mana";
            skill.attribute = Attr.Mana;
            skill.manaCost = 2;
            
            skill.target = SkillTargetType.BattleEnemy;
            skill.valueType = SkillValueType.Combined;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.PhysPower);
            skill.value = -100;
            skill.value2 = -2;
            skill.addCondition(SkillConditionType.TargetMagicDamageType);
            return skill;
        }
        // add all remaining mana to default attack
        if (skillPosType == SkillPositionType.Battle) {
            
            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "+Mana damage";
            skill.attribute = Attr.Damage;
            skill.manaCost = 8;
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Percent;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.Mana);
            skill.value = 100;
            return skill;
        }
        return null;
    }

    private static Skill getAssasinSkill(SkillPositionType skillPosType) {
        // enable passive attack each turn
        if (skillPosType == SkillPositionType.Active) {

            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "passive attack";
            skill.manaCost = 10;
            skill.buffType = BuffType.AttackEveryRound;
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 1;
            skill.durationType = DurationType.Always;
            return skill;
        }
        // buff evasion and evasion power
        if (skillPosType == SkillPositionType.Battle) {
            
            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "+10eva +10evaP";
            skill.manaCost = 8;
            skill.type = SkillType.CombinedBuff;
            skill.buffType = BuffType.Combined;
            skill.duration = 4;
            skill.durationType = DurationType.Round;
            skill.target = SkillTargetType.Self;

            // 1. buff 10 eva
            BuffSkill skill1 = new BuffSkill(BuffKind.Buff);
            skill1.name = "buff +10 eva";
            skill1.manaCost = 0;
            skill1.buffType = BuffType.Attribute;
            skill1.attribute = Attr.EvasionChance;
            skill1.valueType = SkillValueType.Absolute;
            skill1.value = 10;
            skill.addCombinedSkill(skill1);

            // 2. buff 10 evasion power
            BuffSkill skill2 = new BuffSkill(BuffKind.Buff);
            skill2.name = "buff 10 evaPower";
            skill2.manaCost = 0;
            skill2.buffType = BuffType.Attribute;
            skill2.attribute = Attr.EvasionPower;
            skill2.valueType = SkillValueType.Absolute;
            skill2.value = 10;
            skill.addCombinedSkill(skill2);
            //
            return skill;
        }
        return null;
    }

    private static Skill getBeastmasterSkill(SkillPositionType skillPosType) {
        // 
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // summons crow <totem>, attacks every turn (physic)
        if (skillPosType == SkillPositionType.Battle) {
            
            TotemSkill skill = new TotemSkill();
            skill.name = "summon crow";
            skill.manaCost = 11;

            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 0;
            // totem
            Totem totem = new Totem();
            totem.name = "Black crow";
            totem.durationType = DurationType.Always;
            // skill for totem 
            Skill totemSkill = new Skill();
            totemSkill.name = "attack 2 dmg, 2 times";
            totemSkill.manaCost = 0;
            totemSkill.type = SkillType.Combined;
            
            AttackSkill totemSkill1 = new AttackSkill();
            totemSkill1.attackType = HeroDamageType.Physical;
            totemSkill1.target = SkillTargetType.BattleEnemy;                 
            totemSkill1.valueType = SkillValueType.Percent;
            totemSkill1.valueFrom = new CalculateFrom(TargetType.Summoner, Attr.MagicPower);
            totemSkill1.value = 100;
            totemSkill.addCombinedSkill(totemSkill1);

            AttackSkill totemSkill2 = new AttackSkill();
            totemSkill2.attackType = HeroDamageType.Physical;
            totemSkill2.target = SkillTargetType.BattleEnemy;                 
            totemSkill2.valueType = SkillValueType.Percent;
            totemSkill2.valueFrom = new CalculateFrom(TargetType.Summoner, Attr.MagicPower);
            totemSkill2.value = 100;
            totemSkill.addCombinedSkill(totemSkill2);

            totem.skill = totemSkill;
            // ----
            skill.totem = totem;
             // ----

            return skill;
        }
        return null;
    }

    private static Skill getZealotSkill(SkillPositionType skillPosType) {
        // enemy totem with debuff
        if (skillPosType == SkillPositionType.Active) {
            TotemSkill skill = new TotemSkill();
            skill.name = "summon rift";
            skill.manaCost = 12;
            skill.side = TotemSide.Enemy;
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 0;
            // totem
            Totem totem = new Totem();
            totem.name = "Rift";
            totem.durationType = DurationType.Always;
            // skill for totem 
            Skill totemSkill = new Skill();
            totemSkill.name = "-1 PR, -1MR";
            totemSkill.manaCost = 0;
            totemSkill.type = SkillType.Combined;
            
            AddAttrSkill totemSkill1 = new AddAttrSkill();
            totemSkill1.name = "-1 PR";
            totemSkill1.target = SkillTargetType.BattleAlly; 
            totemSkill1.attribute = Attr.PhysResist;         
            totemSkill1.valueType = SkillValueType.Absolute;
            totemSkill1.value = -1;
            totemSkill.addCombinedSkill(totemSkill1);

            AddAttrSkill totemSkill2 = new AddAttrSkill();
            totemSkill2.name = "-1 MR";
            totemSkill2.target = SkillTargetType.BattleAlly; 
            totemSkill1.attribute = Attr.MagicResist;                 
            totemSkill2.valueType = SkillValueType.Absolute;
            totemSkill2.value = -1;
            totemSkill.addCombinedSkill(totemSkill2);

            totem.skill = totemSkill;
            // ----
            skill.totem = totem;
             // ----
            return skill;
        }
        // descrese all enemy hpReg (can be negative)
        if (skillPosType == SkillPositionType.Battle) {
            
            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "hpReg -MP";
            skill.manaCost = 4;
            skill.target = SkillTargetType.AllEnemies; 
            skill.attribute = Attr.HealthRegen;                 
            skill.valueType = SkillValueType.Percent;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill.value = -100;
            return skill;
        }
        return null;
    }
    
    private static Skill getHeraldSkill(SkillPositionType skillPosType) {
        // place flag(totem) on battle that gives buff
        if (skillPosType == SkillPositionType.Active) {
            TotemSkill skill = new TotemSkill();
            skill.name = "place Flag";
            skill.manaCost = 10;
            skill.side = TotemSide.Ally;
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 0;
            // totem
            Totem totem = new Totem();
            totem.name = "Flag";
            totem.durationType = DurationType.Always;
            // skill for totem 
            AddAttrSkill totemSkill = new AddAttrSkill();
            totemSkill.name = "+10% Damage";
            totemSkill.manaCost = 0;
            totemSkill.target = SkillTargetType.BattleAlly; 
            totemSkill.attribute = Attr.Damage;         
            totemSkill.valueType = SkillValueType.Percent;
            totemSkill.valueFrom = new CalculateFrom(TargetType.SkillTarget, Attr.Damage);
            totemSkill.value = 10;
            totem.skill = totemSkill;
            // ----
            skill.totem = totem;
            // ----
            return skill;
        }
        // add armor to every ally
        if (skillPosType == SkillPositionType.Battle) {
            
            AddAttrSkill skill = new AddAttrSkill();
            skill.name = "+1 armor all";
            skill.manaCost = 4;
            skill.target = SkillTargetType.AllAllies; 
            skill.attribute = Attr.Armor;                 
            skill.valueType = SkillValueType.Absolute;
            skill.value = 1;
            return skill;
        }
        return null;
    }

    private static Skill getSorcererSkill(SkillPositionType skillPosType) {
        // -10 MR debuff on enemy, +10% MP self
        if (skillPosType == SkillPositionType.Active) {

            Skill skill = new Skill();
            skill.name = "-10 MR B, +10% MP";
            skill.manaCost = 6;
            skill.type = SkillType.Combined;

            AddAttrSkill skill1 = new AddAttrSkill();
            skill1.name = "-10 magic resist";
            skill1.manaCost = 0;
            skill1.attribute = Attr.MagicResist;
            skill1.target = SkillTargetType.BattleEnemy;
            skill1.valueType = SkillValueType.Absolute;
            skill1.value = -10;
            skill.addCombinedSkill(skill1);

            AddAttrSkill skill2 = new AddAttrSkill();
            skill2.name = "+20% MP";
            skill2.manaCost = 0;
            skill2.target = SkillTargetType.Self;
            skill2.valueType = SkillValueType.Percent;
            skill2.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill2.value = 20;
            skill.addCombinedSkill(skill2);

            return skill;
        }
        // magic shield self
        if (skillPosType == SkillPositionType.Battle) {
            
            StatusSkill skill = new StatusSkill();
            skill.name = "magic shield";
            skill.manaCost = 8;
            
            skill.target = SkillTargetType.Self;
            skill.statusType = StatusType.MagicShield;
            skill.durationType = DurationType.Always;
            
            return skill;
        }
        return null;
    }

    private static Skill getHunterSkill(SkillPositionType skillPosType) {
        // mark (debuff) enemy to increase crit on him
        if (skillPosType == SkillPositionType.Active) {

            BuffSkill skill = new BuffSkill(BuffKind.Debuff);
            skill.name = "Hunters mark";
            skill.manaCost = 6;
            skill.target = SkillTargetType.BattleEnemy;
            skill.buffType = BuffType.HunterMark;
            skill.valueType = SkillValueType.Combined;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.PhysPower);
            skill.value = 100;
            skill.value2 = 10;
            skill.durationType = DurationType.Round;
            skill.duration = 8;
            return skill;
        }
        // 
        if (skillPosType == SkillPositionType.Battle) {
            
            return null;
        }
        return null;
    }

    private static Skill getCommanderSkill(SkillPositionType skillPosType) {
        // swap self with battle hero (and copy one buff?)
        if (skillPosType == SkillPositionType.Active) {

            MoveSkill skill = new MoveSkill();
            skill.name = "swap with B";
            skill.manaCost = 10;
            skill.type = SkillType.Swap;
            skill.target = SkillTargetType.Self;
            skill.fromPlace = PlaceType.AllyActive;
            skill.targetPlace = PlaceType.AllyBattle;
            skill.addCondition(SkillConditionType.OnActivePlace);
            return skill;
        }
        // summon soldier
        if (skillPosType == SkillPositionType.Battle) {
            
            Skill skill = new Skill();
            skill.name = "summon soldier and move back";
            skill.manaCost = 10;
            skill.type = SkillType.Combined;

            // 1. move self back to active
            MoveSkill skill1 = new MoveSkill();
            skill1.name = "move back";
            skill1.manaCost = 0;
            
            skill1.target = SkillTargetType.Self;
            skill1.fromPlace = PlaceType.AllyBattle;
            skill1.targetPlace = PlaceType.AllyActive;

            skill.addCombinedSkill(skill1);
            
            // 2. summon soldier
            SummonSkill skill2 = new SummonSkill();
            skill2.name = "recruit swordsman";
            skill2.manaCost = 0;
            skill2.place = PlaceType.AllyBattle;
            // ------------- summon ---------------------
            Hero summonHero = Hero.copy(HeroFactory.createHero(20, 0, 0, 0, 0, 0, 0, 0, HeroDamageType.Physical, 20, 10, 0, 0, 0));
            summonHero.isSummon = true;
            summonHero.name = "swordsman";
            summonHero.activeSkill = null;
            summonHero.battleSkill = null;
            skill2.summon = summonHero;
            // ------------- summon ---------------------
            skill.addCombinedSkill(skill2);

            // ------------------------------------------
            
            return skill;
        }
        return null;
    }

    private static Skill getExorcistSkill(SkillPositionType skillPosType) {
        // remove buffs from B and A
        if (skillPosType == SkillPositionType.Active) {

            Skill skill = new Skill();
            skill.name = "remove buffs from B and A";
            skill.manaCost = 9;
            skill.type = SkillType.Combined;
            
            Skill skill1 = new Skill();
            skill1.name = "clear buffs";
            skill1.type = SkillType.RemoveBuffs;
            skill1.target = SkillTargetType.BattleEnemy;
            skill.addCombinedSkill(skill1);

            Skill skill2 = new Skill();
            skill2.name = "clear buffs";
            skill2.type = SkillType.RemoveBuffs;
            skill2.target = SkillTargetType.ActiveEnemy;
            skill.addCombinedSkill(skill2);

            return skill;
        }
        // remove totem from enemy battle
        if (skillPosType == SkillPositionType.Battle) {
            
            TotemSkill skill = new TotemSkill();
            skill.name = "remove totem";
            skill.manaCost = 8;
            skill.type = SkillType.RemoveTotem;
            skill.side = TotemSide.Enemy;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 0;
            return skill;
        }
        return null;
    }
    
    private static Skill getForestProtectorSkill(SkillPositionType skillPosType) {
        // 
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // summon Tree Fighter on Active position
        if (skillPosType == SkillPositionType.Battle) {
            
            SummonSkill skill = new SummonSkill();
            skill.name = "summon treant";
            skill.manaCost = 12;
            skill.place = PlaceType.AllyActive;
            skill.summonType = SummonType.Preset;
            // ------------- summon ---------------------
            Hero summonHero = Hero.copy(HeroFactory.createHero(10, 0, 0, 0, 0, 0, 0, 0, HeroDamageType.Physical, 30, 0, 0, 0, 0));
            summonHero.isSummon = true;
            summonHero.name = "treant";
            summonHero.activeSkill = null;
            summonHero.battleSkill = null;
            skill.summon = summonHero;
            return skill;
        }
        return null;
    }

    private static Skill getPriestMasterSkill(SkillPositionType skillPosType) {
        // buff B to be healed each time he is damaged
        if (skillPosType == SkillPositionType.Active) {
            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "heal 5+MP";
            skill.manaCost = 7;
            skill.buffType = BuffType.HealAfterAttacked;
            skill.target = SkillTargetType.BattleAlly;
            skill.valueType = SkillValueType.Combined;
            skill.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill.value = 100;
            skill.value2 = 5;
            skill.duration = 2;
            skill.durationType = DurationType.Round;
            return skill;
        }
        //  buff B to be healed each time he is damaged. increase MP
        if (skillPosType == SkillPositionType.Battle) {

            Skill skill = new Skill();
            skill.name = "buff heal, inc MP";
            skill.manaCost = 9;
            skill.type = SkillType.Combined;
            
            AddAttrSkill skill1 = new AddAttrSkill();
            skill1.name = "+30% MP";
            skill1.manaCost = 0;
            skill1.attribute = Attr.MagicPower;
            skill1.target = SkillTargetType.Self;
            skill1.valueType = SkillValueType.Percent;
            skill1.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill1.value = 30;
            skill.addCombinedSkill(skill1);

            BuffSkill skill2 = new BuffSkill(BuffKind.Buff);
            skill2.name = "heal 5+MP";
            skill2.manaCost = 0;
            skill2.buffType = BuffType.HealAfterAttacked;
            skill2.target = SkillTargetType.BattleAlly;
            skill2.valueType = SkillValueType.Combined;
            skill2.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill2.value = 100;
            skill2.value2 = 5;
            skill2.duration = 3;
            skill2.durationType = DurationType.Round;
            skill.addCombinedSkill(skill2);

            return skill;
        }
        return null;
    }

    private static Skill getMagicBardSkill(SkillPositionType skillPosType) {
        // buff all for MP and mana
        if (skillPosType == SkillPositionType.Active) {
            
            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "+2 Mana, +MP/2 MP";
            skill.manaCost = 10;
            skill.type = SkillType.CombinedBuff;
            skill.buffType = BuffType.Combined;
            skill.duration = 8;
            skill.durationType = DurationType.Round;
            skill.target = SkillTargetType.AllAllies;

            // 1. buff Mana all
            BuffSkill skill1 = new BuffSkill(BuffKind.Buff);
            skill1.name = "buff +2 Mana";
            skill1.manaCost = 0;
            skill1.buffType = BuffType.Attribute;
            skill1.attribute = Attr.Mana;
            skill1.valueType = SkillValueType.Absolute;
            skill1.value = 2;
            skill.addCombinedSkill(skill1);

            // 2. buff MP all
            BuffSkill skill2 = new BuffSkill(BuffKind.Buff);
            skill2.name = "buff +MP/2 MP";
            skill2.manaCost = 0;
            skill2.buffType = BuffType.Attribute;
            skill2.attribute = Attr.MagicPower;
            skill2.valueType = SkillValueType.Percent;
            skill2.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill2.value = 50;
            skill.addCombinedSkill(skill2);
            //
            return skill;
        }
        //  debuff all enemies for high mana cost of next skill
        if (skillPosType == SkillPositionType.Battle) {

            BuffSkill skill = new BuffSkill(BuffKind.Debuff);
            skill.name = "debuff mana cost all";
            skill.manaCost = 10;
            skill.buffType = BuffType.AddNextSkillManaCost;
            skill.target = SkillTargetType.AllEnemies;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 2;
            skill.durationType = DurationType.Always;
            return skill;
        }
        return null;
    }
    
    private static Skill getAlchemistSkill(SkillPositionType skillPosType) {
        // if battle ally health lower swap hp with him and heal self for x
        if (skillPosType == SkillPositionType.Active) {
            
            Skill skill = new Skill();
            skill.name = "swap hp, heal self";
            skill.manaCost = 9;
            skill.type = SkillType.Combined;
            skill.addCondition(SkillConditionType.OnActivePlace);
            skill.addCondition(SkillConditionType.TargetHealthLower);

            // 1. swap hp
            Skill skill1 = new Skill();
            skill1.name = "swap HP with B";
            skill1.manaCost = 0;
            skill1.type = SkillType.SwapHealth;
            skill1.target = SkillTargetType.BattleAlly;
            skill1.valueType = SkillValueType.Absolute;
            skill1.value = 0;
            skill.addCombinedSkill(skill1);

            // 2. heal self for 1xMP
            HealSkill skill2 = new HealSkill();
            skill2.name = "heal MP";
            skill2.manaCost = 0;
            skill2.target = SkillTargetType.Self;
            skill2.valueType = SkillValueType.Percent;
            skill2.valueFrom = new CalculateFrom(TargetType.Self, Attr.MagicPower);
            skill2.value = 100;
            skill.addCombinedSkill(skill2);

            return skill;
        }
        // attack battle on 30% of remaining hp
        if (skillPosType == SkillPositionType.Battle) {

            AttackSkill skill = new AttackSkill();
            skill.name = "attack on % hp";
            skill.manaCost = 10;
            skill.attackType = HeroDamageType.Magic;
            skill.target = SkillTargetType.BattleEnemy;                 
            skill.valueType = SkillValueType.Percent;
            skill.valueFrom = new CalculateFrom(TargetType.SkillTarget, Attr.Health);
            skill.value = 30;
            return skill;
        }
        return null;
    }

    private static Skill getBladedancerSkill(SkillPositionType skillPosType) {
        // buff self on each success attack on same target
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // buff next attack ignore shield and armor
        if (skillPosType == SkillPositionType.Battle) {

            BuffSkill skill = new BuffSkill(BuffKind.Buff);
            skill.name = "ignore SHI, AR";
            skill.manaCost = 6;
            skill.buffType = BuffType.IgnoreShieldArmor;
            skill.target = SkillTargetType.Self;
            skill.valueType = SkillValueType.Absolute;
            skill.value = 0;
            skill.durationType = DurationType.Round;
            skill.duration = 1;
            return skill;
        }
        return null;
    }

    private static Skill getPredatorSkill(SkillPositionType skillPosType) {
        // apply bleed
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // mark lowest % hp enemy (attack marked enemy with default attack)
        if (skillPosType == SkillPositionType.Battle) {
            return null;
        }
        return null;
    }

    private static Skill getWitchSkill(SkillPositionType skillPosType) {
        // add MP to next default attack (passive: get MP for each debuff on enemies)
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // debuff battle hero to take more dmg after he is attacked
        if (skillPosType == SkillPositionType.Battle) {
            return null;
        }
        return null;
    }

    private static Skill getMagicMimicSkill(SkillPositionType skillPosType) {
        // use enemy active skill of enemy active hero (passive: buff MP,PP for each unique skill used)
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // copy armor,shield, buffs from enemy battle hero
        if (skillPosType == SkillPositionType.Battle) {
            return null;
        }
        return null;
    }

    private static Skill getOracleSkill(SkillPositionType skillPosType) {
        // 
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // buff to get PP every time fully shield dmg
        if (skillPosType == SkillPositionType.Battle) {
            return null;
        }
        return null;
    }

    private static Skill getBlackKnightSkill(SkillPositionType skillPosType) {
        // 
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // buff to get PP every time fully shield dmg
        if (skillPosType == SkillPositionType.Battle) {
            return null;
        }
        return null;
    }

    private static Skill getDoomsayerSkill(SkillPositionType skillPosType) {
        // 
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // 
        if (skillPosType == SkillPositionType.Battle) {
            return null;
        }
        return null;
    }

    private static Skill getShadowMasterSkill(SkillPositionType skillPosType) {
        // 
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // 
        if (skillPosType == SkillPositionType.Battle) {
            return null;
        }
        return null;
    }

    private static Skill getBardMasterSkill(SkillPositionType skillPosType) {
        // 
        if (skillPosType == SkillPositionType.Active) {
            return null;
        }
        // 
        if (skillPosType == SkillPositionType.Battle) {
            return null;
        }
        return null;
    }

    
    
}
