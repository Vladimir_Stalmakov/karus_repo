using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealSkill : Skill
{
    public HealSkill() : base()
    {
        type = SkillType.Heal;
    }

    public override Skill copy() {
        HealSkill copy = new HealSkill();
        Skill.copyAll(this, copy);
        // copy.buffType = buffType;
        // copy.duration = duration;
        // copy.durationType = durationType;
        // copy.attribute = attribute;
        return copy;
    }
}
