using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddAttrSkill : Skill
{
    public Attr attribute { get; set; }
    
    public AddAttrSkill() : base()
    {
        type = SkillType.AddAttribute;
    }
}
