using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UI_PlayerPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Player player { get; set; }
    
    public Text nameText;
    public Text healthText;
    public List<Text> heroNameTexts;
    public GameObject heroesPanel;

    public void init(Player _player, bool isOpponent) {
        player = _player;
        nameText.text = player.name;// + " vs " + player.opponentIndex;
        healthText.text = player.health > 0 ? ""+player.health : "DEAD";
        if (isOpponent) {
           nameText.color = Color.red; 
        }
        initHeroesPanel();
    }

    private void initHeroesPanel() {
       for(int i=0; i<4; i++) {
          heroNameTexts[i].text = player.heroCount <= i ? "" : player.heroCount <= i ? "" : player.heroes[i].name;
       }
    }

    public void OnPointerEnter(PointerEventData eventData) {
        heroesPanel.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        heroesPanel.gameObject.SetActive(false);
    }

    void Start() {}

    void Update() {}
}
