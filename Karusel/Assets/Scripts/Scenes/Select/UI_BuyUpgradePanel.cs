using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BuyUpgradePanel : MonoBehaviour
{
    private SelectSceneManager selectManager;

    public SelectUpgrade upgrade { get; set; }

    // UI
    [Header("UI")]
    public Text text;
    
    public void init(SelectUpgrade _upgrade, SelectSceneManager _selectManager) {
        upgrade = _upgrade;
        selectManager = _selectManager;
        text.text = upgrade.descr;
        gameObject.SetActive(true);
    }

    public void onBuyButtonClick() {
        selectManager.buyUpgrade(upgrade, this);
    }

    public void hide() {
        gameObject.SetActive(false);
    }
    
    void Start()
    {
        
    }

    void Update()
    {
        
    }
}
