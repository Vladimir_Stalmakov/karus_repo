using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_HeroPanel : MonoBehaviour
{
    private SelectSceneManager selectManager;
    public BasicHero hero { get; set; }

    public bool isActive;

    // UI
    [Header("UI")]
    public Text text;
    public Button applyButton;
    public Button moveButton;

    public Button toPackButton;
    public Button toActiveButton;
    public Button upgradeButton;

    public GameObject mainPanel;
    public Text statusText;

    [Header("ID")]
    public int id;
    [Header("Attributes")]
    public Text healthText;
    public Text manaText;
    public Text regenText;
    public Text powerText;
    public Text resistText;
    public Text shieldText;
    public Text damageText;
    public Text critText;
    public Text evasionText;

    // -----------------------------
    
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void init(BasicHero _hero, SelectSceneManager _selectManager, bool isActive) {  
        hero = _hero;
        
        if (hero == null) {
            setEmpty(_selectManager);
            return;
        }

        text.text = hero.name;
        setAttributes(hero);
        selectManager = _selectManager;
        mainPanel.gameObject.SetActive(true);
        statusText.gameObject.SetActive(false);
        toPackButton.gameObject.SetActive(isActive);
        toActiveButton.gameObject.SetActive(!isActive);

        if (!hero.heroClass.isMulticlass && selectManager.player.isMulticlassEnabled) {
            upgradeButton.gameObject.SetActive(true);
        }
    }

    public void setEmpty(SelectSceneManager _selectManager) {
        //Debug.Log("setEmpty");
        selectManager = _selectManager;
        setEmpty();
    }

    public void setEmpty() {
        hero = null;
        mainPanel.gameObject.SetActive(false);
        statusText.gameObject.SetActive(true);
        statusText.text = "EMPTY";
    }

    public void onApplyButtonClick() {
        selectManager.applyOnHero(hero, this);
    }

    public void onToPackButtonClick() {
        selectManager.moveHeroToPack(id);
        //toActiveButton.gameObject.SetActive(true);
        //toPackButton.gameObject.SetActive(false);
    }

    public void onToActiveButtonClick() {
        selectManager.moveHeroToActive(id);
        //toActiveButton.gameObject.SetActive(false);
        //toPackButton.gameObject.SetActive(true);
    }

    public void onMoveButtonClick() {
        selectManager.moveHero(id);
    }

     public void onUpgradeButtonClick() {
        selectManager.upgradeHero(hero, this);
    }

    public void enableApplyMode() {
        if (gameObject.activeSelf) {
            applyButton.gameObject.SetActive(true);
            moveButton.gameObject.SetActive(false);
        }
    }

    public void disableApplyMode() {
        if (gameObject.activeSelf) {
            applyButton.gameObject.SetActive(false);
            moveButton.gameObject.SetActive(true);
        }
    }

    public void enableHeroApplyModeForUpgrade(BasicHero heroToUpgrade) {
        if (hero != null 
        && !hero.heroClass.isMulticlass 
        && heroToUpgrade.heroClass.basicClass != hero.heroClass.basicClass) {
            enableApplyMode();
        }
    }

    private void setAttributes(BasicHero hero) {
        healthText.text = "HP: " + hero.health;
        manaText.text = "MANA: " + hero.mana;
        regenText.text = "REG: " + hero.healthRegen + "/" + hero.manaRegen;
        powerText.text = "POWER: " + hero.physPower + "/" + hero.magicPower;
        resistText.text = "RES: " + hero.physResist + "/" + hero.magicResist;
        shieldText.text = "SH/AR: " + hero.shield + "/" + hero.armor;
        damageText.text = "DMG: " + hero.damage;
        critText.text = "CRIT: " + hero.critChance;
        evasionText.text = "EVA: " + hero.evasionChance;
    }
}
