using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BuyPanelType { CLASS, ATTR, PERK }

public class UI_BuyPanel : MonoBehaviour
{   
    private BuyPanelType type;
    private BasicClass basicClass;
    private HeroAttribute attribute;
    private Perk perk;

    private SelectSceneManager selectManager;

    // UI

    public Text text;
    
    public void init(BasicClass _basicClass, SelectSceneManager _selectManager) {
        type = BuyPanelType.CLASS;
        basicClass = _basicClass;
        selectManager = _selectManager;
        text.text = "" + basicClass;
        gameObject.SetActive(true);
    }

    public void init(HeroAttribute _attribute, SelectSceneManager _selectManager) {
        type = BuyPanelType.ATTR;
        attribute = _attribute;
        selectManager = _selectManager;
        text.text = "+" + attribute.value + " " + attribute.attribute;
        gameObject.SetActive(true);
    }

    public void init(Perk _perk, SelectSceneManager _selectManager) {
        type = BuyPanelType.PERK;
        perk = _perk;
        selectManager = _selectManager;
        text.text = perk.name;
        gameObject.SetActive(true);
    }
    
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void onBuyButtonClick() {
        
        switch (type) {
            case BuyPanelType.CLASS: {
                //Debug.Log("BUY CLASS " + basicClass);
                selectManager.buyBasicClass(basicClass, this);
            }
            break;
            case BuyPanelType.ATTR: {
                //Debug.Log("BUY ATTR " + attribute.value + " " + attribute.attribute);
                selectManager.buyAttribute(attribute, this);
            }
            break;
            case BuyPanelType.PERK: {
                //Debug.Log("BUY PERK " + perk.name);
                selectManager.buyPerk(perk, this);
            }
            break;

        }
    }

    public void hide() {
        gameObject.SetActive(false);
    }
}
