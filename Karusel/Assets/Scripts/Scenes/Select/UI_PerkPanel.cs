using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_PerkPanel : MonoBehaviour
{
    private SelectSceneManager selectManager;
    private Perk perk;
    
    // UI
    
    public Text text;
    public Text targetText;
    public Button targetButton;

    public void init(Perk _perk, SelectSceneManager _selectManager) {
        perk = _perk;
        text.text = perk.name;
        if (perk.targetHero != null) {
            targetText.text = perk.targetHero.name;
        } else if (perk.targetType == PerkTargetType.AllAllyHeroes) {
            targetText.text = "ALL";
            targetButton.gameObject.SetActive(false);
        } else {
            targetText.text = "NONE";
        }
        selectManager = _selectManager;
        gameObject.SetActive(true);
    }

    public void onTargetButtonClick() {
        selectManager.applyPerk(perk);
    }
    
    
    void Start()
    {
        
    }

    
    void Update()
    {
        
    }
}
