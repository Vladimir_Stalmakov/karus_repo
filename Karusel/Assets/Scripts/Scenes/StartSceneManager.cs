using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneManager : MonoBehaviour
{
    public SessionManager sessionManager;
    
    void Start() {   
    }

    void Update() {   
    }

    public void onStartButtonClick() {
        sessionManager.startNewSession();
    }
}
