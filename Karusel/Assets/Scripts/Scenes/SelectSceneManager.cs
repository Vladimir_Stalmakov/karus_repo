using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectSceneManager : MonoBehaviour
{
    private SessionManager sessionManager;
    public Player player { get; set; }

    private int roundGold { get; set; }     // gold at the start of the round
    private int currentGold { get; set; }
    // COSTS
    private int heroClassCost { get; set; }
    private int attributeCost { get; set; }
    private int perkCost { get; set; }
    private int upgradeCost { get; set; }

    private int heroClassRollCost { get; set; }
    private int attributeRollCost { get; set; }
    private int perkRollCost { get; set; }
    // ----
    public int selectHeroCount { get; set; }
    public List<BasicClass> heroClassList { get; set; }
    
    public int selectAttrCount { get; set; }
    public List<HeroAttribute> attributeList { get; set; }
    
    public int selectPerkCount { get; set; }
    public List<Perk> perkList { get; set; }

    public HeroAttribute attrToApply { get; set; }
    public Perk perkToApply { get; set; }
    public UI_HeroPanel heroPanelToApply { get; set; }

    // --------------------- UI -----------------------

    public Text mainText;
    public Text goldText;
    public Text healthText;

    public List<UI_BuyPanel> heroBuyPanels;
    public List<UI_BuyPanel> attributeBuyPanels;
    public List<UI_BuyPanel> perkBuyPanels;

    public List<UI_HeroPanel> heroPanels;
    public List<UI_HeroPanel> packHeroPanels;
    public List<UI_PerkPanel> perkPanels;

    public List<UI_BuyUpgradePanel> upgradePanels;
    public GameObject upgradesPanel;

    public Button showUpgradesButton;

    public List<UI_PlayerPanel> playerPanels;

    // ----- BUTTONS ----------------------------
    
    public void onDuelButtonClick() {
        sessionManager.changeToDuelScene();
    }

    public void onShowUpgradesButtonClick() {
        upgradesPanel.gameObject.SetActive(true);
        showUpgradesButton.gameObject.SetActive(false);
    }

    public void onHideUpgradesButtonClick() {
        upgradesPanel.gameObject.SetActive(false);
        showUpgradesButton.gameObject.SetActive(true);
    }

    // -------------------------------------------

    void Start()
    {
        sessionManager = FindObjectOfType<SessionManager>();
        player = sessionManager.player;
        init();
        initUI();
    }

    void Update()
    {
        
    }

    private void init() {

        selectHeroCount = player.selectHeroCount;
        selectAttrCount = player.selectAttrCount;
        selectPerkCount = player.selectPerkCount;
        
        heroClassCost = Constants.HERO_CLASS_COST;
        attributeCost = Constants.ATTRIBUTE_COST;
        perkCost = Constants.PERK_COST;
        upgradeCost = Constants.UPGRADE_COST;

        heroClassRollCost = Constants.ROLL_COST;
        attributeRollCost = Constants.ROLL_COST;
        perkRollCost = Constants.ROLL_COST;

        roundGold = Constants.START_GOLD + (sessionManager.roundNumber - 1);
        currentGold = roundGold;

        // 1. init hero - set all heroes, perks


        // 2. get random cards on all 3 lists
        List<BasicClass> classes = new List<BasicClass>();
        for (int i=0; i<selectHeroCount; i++) {
            float random = Random.Range(0.0f, 0.9f);
            int heroClassNumber = (int)(random * 10);
            while (classes.Contains((BasicClass)heroClassNumber)) {
                random = Random.Range(0.0f, 0.9f);
                heroClassNumber = (int)(random * 10);
            }
            BasicClass basicClass = (BasicClass)heroClassNumber;
            classes.Add(basicClass);
            heroBuyPanels[i].init(basicClass, this);
        }

        for (int i=0; i<selectAttrCount; i++) {
            HeroAttribute heroAttr = HeroAttributeCollection.getRandomHeroAttr();
            attributeBuyPanels[i].init(heroAttr, this);
        }

         for (int i=0; i<selectPerkCount; i++) {
            Perk perk = PerkCollection.getRandomPerk();
            perkBuyPanels[i].init(perk, this);
        }
    }

    private void initUI() {
        mainText.text = "SELECT. ROUND " + sessionManager.roundNumber;
        goldText.text = roundGold + "/" + roundGold;
        healthText.text = "" + player.health;
        initPlayerPanels();
        initHeroPanels();
        initPerkPanels();
        initUpgradePanels();
    }

    private void initHeroPanels() {
        
        Debug.Log(">>>>> initHeroPanels active heroes: " + player.heroCount + " , pack heroes: " + player.packHeroCount);
        Debug.Log(">>>>> packHeroPlaceCount: " + player.packHeroPlaceCount);
        // init active hero panels
        for(int i=0; i<player.heroPlaceCount; i++) {
            //if (i<player.heroCount) {
             if  (i<player.heroes.Count) { 
                if (player.heroes[i] == null) {
                   Debug.Log("heroes["+i+"] : NULL");
                } else {
                    Debug.Log("heroes["+i+"] : " + player.heroes[i].name);
                }
                heroPanels[i].init(player.heroes[i], this, true);
            } else {
                heroPanels[i].setEmpty(this);
            }
        }
        // init pack hero panels
        for(int i=0; i<player.packHeroPlaceCount; i++) {
            //if (i<player.packHeroCount) {
            if (i<player.packHeroes.Count) {
                packHeroPanels[i].init(player.packHeroes[i], this, false);
            } else {
                packHeroPanels[i].setEmpty(this);
            }
        }
    }

    private void initPlayerPanels() {
        setDuelPairs();

        //int randomIndex = (int)Random.Range(1.0f, 7.0f);
        //player.opponentIndex = randomIndex;
        //player.opponentName = sessionManager.players[randomIndex].name;
        //sessionManager.players[randomIndex].opponentIndex = 0;
        //Debug.Log(">>>> OPPONENT: "+randomIndex + " - " + player.opponentName);
        playerPanels[0].init(player, false);
        // generate heroes for AI players
        for(int i=1; i<playerPanels.Count; i++) {
            Player playerAI = sessionManager.players[i];
            OpponentManager.select(playerAI, sessionManager.roundNumber);
            playerPanels[i].init(playerAI, player.opponentIndex == i);
        }
    }

    private void setDuelPairs() {
        // TODO: opponent choose system
        List<Player> players = sessionManager.players;
        for (int i=0; i<8; i++) {
            players[i].opponentIndex = -1;
        }
        for (int i=0; i<8; i++) {
            if (players[i].opponentIndex == -1) {
                int randomIndex = (int)Random.Range(1.0f, 7.0f);
                while(randomIndex == i || players[randomIndex].opponentIndex != -1) {
                    randomIndex = (int)Random.Range(0.0f, 7.99f);
                }
                players[i].opponentIndex = randomIndex;
                players[randomIndex].opponentIndex = i;
            }
        }
    }

    private void initPerkPanels() {
        for(int i=0; i<player.perks.Count; i++) {
            perkPanels[i].init(player.perks[i], this);
        }
    }

    private void initUpgradePanels() {
        List<SelectUpgrade> upgrades = UpgradeCollection.getUpgrades();
        int i = 0;
        foreach(SelectUpgrade upgrade in upgrades) {
            if (player.upgrades.Find(upg => upg.id == upgrade.id) == null) {
                upgradePanels[i].init(upgrade, this);
                i++;
            }
        }
    }

    private void refreshHeroPanels() {
        initHeroPanels();
    }

    private void refreshPerkPanels() {
        initPerkPanels();
    }

    public void buyBasicClass(BasicClass basicClass, UI_BuyPanel buyPanel) {
        // 0. check if enough space for new hero
        
        
        // 1. check if enough gold
        if (currentGold < heroClassCost) {
            Debug.Log("Not enough gold");
            return;
        }
        // 2. pay gold
        currentGold -= heroClassCost;
        goldText.text = currentGold + "/" + roundGold;
        // 3. increase next buy cost +1
        heroClassCost += 1;
        // 4. add new hero to player
        player.addHero(HeroFactory.createHero(basicClass));
        buyPanel.hide();
        refreshHeroPanels();
        // 5. add new category to perks
        PerkCollection.addCategory(basicClass);
    }

    public void buyAttribute(HeroAttribute attribute, UI_BuyPanel buyPanel) {
        // 1. check if enought gold
        if (currentGold < attributeCost) {
            Debug.Log("Not enough gold");
            return;
        }
        // 2. pay gold
        currentGold -= attributeCost;
        goldText.text = currentGold + "/" + roundGold;
        // 3. increase next buy cost +1
        attributeCost += 1;
        // 4. select hero for attr and apply
        attrToApply = attribute;
        buyPanel.hide();
        enableHeroApplyMode();
    }

    public void buyPerk(Perk perk, UI_BuyPanel buyPanel) {
        // 1. check if enought gold
        if (currentGold < perkCost) {
            Debug.Log("Not enough gold");
            return;
        }
        // 2. pay gold
        currentGold -= perkCost;
        goldText.text = currentGold + "/" + roundGold;
        // 3. increase next buy cost +1
        perkCost += 1;
        // 4. select hero for attr and apply
        //attrToApply = attribute;
        buyPanel.hide();
        player.addPerk(perk.copy());
        refreshPerkPanels();
        //enableHeroApplyMode();
    }

    public void buyUpgrade(SelectUpgrade upgrade, UI_BuyUpgradePanel upgradePanel) {
        // 1. check if enought gold
        if (currentGold < upgradeCost) {
            Debug.Log("Not enough gold");
            return;
        }
        // 2. pay gold
        currentGold -= upgradeCost;
        goldText.text = currentGold + "/" + roundGold;
        // 3. apply upgrade
        upgrade.execute(player);
        refreshHeroPanels();
        // 4. hide panel
        upgradePanel.hide();
    }

    public void applyPerk(Perk perk) {
        perkToApply = perk;
        enableHeroApplyMode();
    }

    public void applyOnHero(BasicHero hero, UI_HeroPanel heroPanel) {
        if (attrToApply != null) {
            hero.addHeroAttribute(attrToApply);
            attrToApply = null;
            disableHeroApplyMode();
            refreshHeroPanels();
        } else if (perkToApply != null) {
            hero.addPerk(perkToApply);
            perkToApply.targetHero = hero;
            // remove perk from another hero if it was already applied
            perkToApply = null;
            disableHeroApplyMode();
            refreshPerkPanels();
        } else if (heroPanelToApply != null) {
            Debug.Log("Combine " + hero.name + " with " + heroPanelToApply.hero.name);
            // create multiclass
            //hero.upgradeToMultiClass(heroPanelToApply.hero);
            BasicHero newHero = HeroFactory.createMultiClassHero(heroPanel.hero, heroPanelToApply.hero);
            player.changeHero(heroPanel.id, heroPanel.isActive, newHero);
            //heroPanel.hero = 
            // destroy heroPanelToApply hero
            player.changeHero(heroPanelToApply.id, heroPanelToApply.isActive, null);
            heroPanelToApply.setEmpty();
            // if (heroPanelToApply.isActive) {
            //     player.heroCount--;
            // } else {
            //     player.packHeroCount--;
            // }
            // heroPanelToApply = null;

            // 
            disableHeroApplyMode();
            refreshHeroPanels();
        }
    }

    public void moveHero(int heroIndex) {
        player.moveHeroForward(heroIndex);
        refreshHeroPanels();
    }

    public void moveHeroToPack(int heroIndex) {
        if (player.packHeroPlaceCount > 0) {
           player.moveHeroToPack(heroIndex);
           refreshHeroPanels();
        }
    }

    public void moveHeroToActive(int heroIndex) {
        player.moveHeroToActive(heroIndex);
        refreshHeroPanels();
    }

    public void upgradeHero(BasicHero hero, UI_HeroPanel heroPanel) { //(int heroIndex) {
        heroPanelToApply = heroPanel;
        enableHeroApplyModeForUpgrade(hero);
        heroPanel.disableApplyMode();
        refreshHeroPanels();
    }

    private void enableHeroApplyMode() {
        foreach(UI_HeroPanel heroPanel in heroPanels) {
            heroPanel.enableApplyMode();
        }
        for(int i=0; i<player.packHeroCount; i++) {
            packHeroPanels[i].enableApplyMode();
        }

    }

    private void enableHeroApplyModeForUpgrade(BasicHero hero) {
        foreach(UI_HeroPanel heroPanel in heroPanels) {
            heroPanel.enableHeroApplyModeForUpgrade(hero);
            
        }
        for(int i=0; i<player.packHeroCount; i++) {
            packHeroPanels[i].enableHeroApplyModeForUpgrade(hero);
        }

    }

    private void disableHeroApplyMode() {
        foreach(UI_HeroPanel heroPanel in heroPanels) {
            heroPanel.disableApplyMode();
        }
        for(int i=0; i<player.packHeroCount; i++) {
            packHeroPanels[i].disableApplyMode();
        }
    }
      

    private void rollBasicClassList() {
        // TODO:
        // 1. check if enought gold
        // 2. pay gold
        // 3. change list
    }

    private void rollAttributeList() {
        // TODO:
        // 1. check if enought gold
        // 2. pay gold
        // 3. change list
    }

    private void rollPerkList() {
        // TODO:
        // 1. check if enought gold
        // 2. pay gold
        // 3. change list
    }
    
}
