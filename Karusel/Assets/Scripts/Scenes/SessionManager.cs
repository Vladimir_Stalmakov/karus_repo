using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SessionManager : MonoBehaviour
{
    private static SessionManager instance;

    public Player player { get; set; }

    public int playersCount = 8;
    public List<Player> players { get; set; }
    
    
    public int roundNumber { get; set; }
    
    // public void init() {
    //     roundNumber = 1;
    // }

    private void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void startNewSession() {
        roundNumber = 0;
        initPlayer();
        initLobby();
        initCollections();
        changeToSelectScene();
    }

    public void changeToDuelScene() {
        // playerToDuel = PlayerDuelSelectManager.selectPlayerForDuel(players);
        // TODO: select Player for duel
        SceneManager.LoadScene("DuelScene");
    }

    public void changeToSelectScene() {
        roundNumber++;
        SceneManager.LoadScene("SelectScene");
    }

    private void initPlayer() {
        player = new Player();
        player.name = "Vova";
        //player.addHero(HeroFactory.createMultiClassHero(MultiClass.Bladedancer));
        //player.addHero(HeroFactory.createHero(BasicClass.Magic));
    }

    private void initLobby() {
        players = new List<Player>();
        for(int i=0; i<playersCount; i++) {
            if (i == 0) {
                 players.Add(player);
            } else {
                Player player = new Player();
                player.name = "AI " + i;
                players.Add(player);
            }
            
        }
    }

    
    private void initCollections() {
        HeroAttributeCollection.init();
        PerkCollection.init();
        UpgradeCollection.init();
    }
    
    void Start()
    {
        // init();
    }

    void Update()
    {
        
    }
}
