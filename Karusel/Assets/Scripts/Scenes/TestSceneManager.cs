using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestSceneManager : MonoBehaviour
{
    public Text text;
    
    void Start()
    {
        // 1. create 2 players with heroes
        // 2. move them in battle
        // 3. show results

        Player player1 = new Player();
        player1.heroCount = 3;
        player1.heroPlaceCount = 3;
        
        BasicHero hero11 = HeroFactory.createHero(BasicClass.Wild);
        player1.addHero(hero11);

        BasicHero hero12 = HeroFactory.createHero(BasicClass.Priest);
        player1.addHero(hero12);

        BasicHero hero13 = HeroFactory.createHero(BasicClass.Magic);
        player1.addHero(hero13);
       
        // ------------------------------------------

        Player player2 = new Player();
        player2.heroCount = 3;
        player2.heroPlaceCount = 3;

        BasicHero hero21 = HeroFactory.createHero(BasicClass.Dark);
        player2.addHero(hero21);

        BasicHero hero22 = HeroFactory.createHero(BasicClass.Order);
        player2.addHero(hero22);

        BasicHero hero23 = HeroFactory.createHero(BasicClass.Master);
        player2.addHero(hero23);

        // ------------------------------------------

        BattleManager.setPlayers(player1, player2);
        BattleResult battleResult = BattleManager.executeBattle();

        string battleDescr = "";
        battleDescr += "BATTLE STARTED:\n\n" + battleResult.descr + "";
        foreach(RoundResult roundResult in battleResult.rounds) {
            battleDescr += "\n\n" + roundResult.descr;
        }

        text.text = battleDescr;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
