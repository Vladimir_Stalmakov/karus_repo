using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DuelCirclePanel : MonoBehaviour
{
    public UI_DuelCirclePanel enemyCircle;
    [Header("UI")]
    public List<UI_DuelHeroPanel> heroPanels;
    public UI_DuelHeroPanel battleHeroPanel;
    public UI_DuelHeroPanel activeHeroPanel;
    public Text logText;

    private int placeCount { get; set; }

    public void initHeroPanels(int _placeCount, List<Hero> heroes) { 
        placeCount = _placeCount;
        for(int i=0; i<heroPanels.Count; i++) {
            if (i < placeCount) {
                heroPanels[i].init(heroes[i]);
                //if (i < heroes.Count) {
                    //heroPanels[i].init(heroes[i]);
                //} else {
                    //heroPanels[i].init(null);
                //}
            } else {
                // hide panels which are yet not available 
                heroPanels[i].hide();
            }
        }
        battleHeroPanel.init(null);
    }

    public void moveToBattlePlace() {
        Hero hero = activeHeroPanel.hero;
        // Debug.Log(">>>> moveToBattlePlace ");
        // if (hero == null) {
        //     Debug.Log(">>>> HERO NULL !!");
        // } else {
        //     Debug.Log("hero: " + hero.name);
        // }
        battleHeroPanel.setHero(hero);
        activeHeroPanel.setHero(null);
    }

    public void rotate(int places) {     
       StartCoroutine(waitAndRotate(0.5f, 0, places));
    }

    public void executeAction(RoundAction action) {
        //Debug.Log("executes Action " + action.type);
        if (action.type == RoundActionType.Summon) {
            UI_DuelHeroPanel hPanel = findHeroPanel(action.targetPlaceId);
            hPanel.executeAction(action);
            return;
        }

        if (action.type == RoundActionType.SummonTotem || action.type == RoundActionType.RemoveTotem) {
            UI_DuelHeroPanel hPanel = findHeroPanel(action.targetPlaceId);
            hPanel.executeAction(action);
            return;
        }

        if (action.type == RoundActionType.HeroMove) {
            UI_DuelHeroPanel fromPanel = findHeroPanel(action.fromPlaceId);
            UI_DuelHeroPanel targetPanel = findHeroPanel(action.targetPlaceId);
            Hero targetPlaceHero = targetPanel.hero;
            targetPanel.setHero(fromPanel.hero);
            fromPanel.setHero(targetPlaceHero);
            //refreshHeroPanels();
            return;
        }
        
        string heroName = action.actionHeroName;

        UI_DuelHeroPanel heroPanel;
        if (heroName == Constants.HERO_NAME_TOTEM) {
            Debug.Log("heroName == totem >>> heroPanel -> battleHeroPanel");
            heroPanel = battleHeroPanel;
        } else if (heroName == Constants.HERO_NAME_NONE) {
            Debug.Log("heroName == totem >>> heroPanel -> battleHeroPanel");
            heroPanel = battleHeroPanel;
        } else {
            heroPanel = findHeroPanel(heroName);
        }
        
        if (heroPanel == null) {
            return;
        }

        heroPanel.executeAction(action);
    }

    public void executeBuffRemoveAction(RoundAction action) {
        UI_DuelHeroPanel heroPanel  = findHeroPanel(action.actionHeroName);
        heroPanel.hero.removeBuff(action.buff);
        heroPanel.refresh();
    }

    public void updateHeroPanels(List<Hero> heroes) {
        // Debug.Log(">>>>>>>>>>>> updateHeroPanels");
        // foreach (Hero hero in heroes) {
        //     // Debug.Log(">>>> " + hero.name);
        //     foreach(Buff buff in hero.buffs) {
        //         Debug.Log(buff.name + " C: " + buff.duration);
        //     }
        // }
        foreach (UI_DuelHeroPanel heroPanel in heroPanels) {
            updateHeroPanel(heroPanel, heroes);
        }
        updateHeroPanel(battleHeroPanel, heroes);
    }

    private void updateHeroPanel(UI_DuelHeroPanel heroPanel, List<Hero> heroes) {
        if (heroPanel.hero != null) {
            // Debug.Log(">>>>> update " + heroPanel.hero.name + " buffs.C: " + heroPanel.hero.buffs.Count);
            Hero updatedHero = heroes.Find(hero => hero.name == heroPanel.hero.name);
            heroPanel.setHero(updatedHero);
            //Debug.Log("updateHeroPanel " + updatedHero.name);
            //Debug.Log("statuses:");
            // foreach(Status status in updatedHero.statuses) {
            //     Debug.Log(status.name + " >>: " + status.duration);
            // }
        }
    }

    private UI_DuelHeroPanel findHeroPanel(int id) {
        if (id == 8) {
            return battleHeroPanel;
        } else {
            return heroPanels[id];
        }
    }

    private UI_DuelHeroPanel findHeroPanel(string heroName) {
        if (battleHeroPanel.hero != null && battleHeroPanel.hero.name == heroName) {
            return battleHeroPanel;
        } else if (activeHeroPanel.hero != null && activeHeroPanel.hero.name == heroName) {
            return activeHeroPanel;
        } else {
            UI_DuelHeroPanel heroPanel = heroPanels.Find(heroPanel => heroPanel.hero != null && heroPanel.hero.name == heroName);
            if (heroPanel == null) {
                Debug.Log("ERROR! findHeroPanel > battleHero and activeHero dont match name " + heroName);
            }
            return heroPanel;
        }
    } 

    private IEnumerator waitAndRotate(float time, int i, int places)
    {
        yield return new WaitForSeconds(time);
        rotate();
        i++;
        if (i<places) {
            StartCoroutine(waitAndRotate(0.5f, i, places));
        }
    }

    public void rotate() {
        Hero activeHero = activeHeroPanel.hero;
        // for(int i=1; i<placeCount; i++) {
        //     heroPanels[i]
        // }
        for (int i = placeCount - 1; i > 0; i--) {
            int targetIndex = i == placeCount - 1 ? 0 : i + 1;
            heroPanels[targetIndex].setHero(heroPanels[i].hero);
            //moveToNextHeroPanel(heroPanels[i]);
        }
        heroPanels[1].setHero(activeHero);
        refreshHeroPanels();
    }

    public void refreshHeroPanels() {
        for(int i=0; i<placeCount; i++) {
            heroPanels[i].refresh();
        }
    }

    public void log(string text) {
        logText.text = text;
    }

    public void executeAttack(RoundAction action) {
        enemyCircle.receiveAttack(action);
    }

    public void executeHeal(RoundAction action) {
        UI_DuelHeroPanel targetHeroPanel = findTargetPanel(action);
        targetHeroPanel.executeHealAction(action);
    }

    public void executeBuff(RoundAction action) {
        UI_DuelHeroPanel targetHeroPanel = findTargetPanel(action);
        targetHeroPanel.executeBuffAction(action);
    }

    public void executeStatus(RoundAction action) {
        UI_DuelHeroPanel targetHeroPanel = findTargetPanel(action);
        targetHeroPanel.executeStatusAction(action);
    }

    public void executeSwapHealth(RoundAction action) {
        UI_DuelHeroPanel targetHeroPanel = findTargetPanel(action);
        targetHeroPanel.executeSwapHealth(action);
    } 

    private UI_DuelHeroPanel findTargetPanel(RoundAction action) {
        UI_DuelHeroPanel targetHeroPanel = (action.targetCircleId == 0) ? findHeroPanel(action.targetHeroName) : enemyCircle.findHeroPanel(action.targetHeroName);
        return targetHeroPanel;
    }
    

    public void receiveAttack(RoundAction action) {
        //Debug.Log("receiveAttack on <" + action.targetHeroName + ">");
        UI_DuelHeroPanel heroPanel = findHeroPanel(action.targetHeroName);
        if (heroPanel == null) {
            return;
        }
        heroPanel.handleAttack(action);
    }
    
    void Start()
    {
        
    }

    void Update()
    {
        
    }
}
