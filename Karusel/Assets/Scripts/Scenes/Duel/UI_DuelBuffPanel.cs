using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DuelBuffPanel : MonoBehaviour
{
    public Text text;
    public Text durationText;

    public Buff buff { get; set; }

    public void init(Buff _buff) {
        // Debug.Log("BuffPanel init " + ((_buff == null) ? "<null>" : _buff.name + ": " + _buff.duration));
        buff = _buff;
        if (buff != null) {
            text.color = buff.buffKind == BuffKind.Buff ? Color.green : Color.red;
            text.text = buff.name;
            durationText.text = ""+buff.duration;
        }
        gameObject.SetActive(buff != null);
    } 

    void Start()
    {
        
    }

    
    void Update()
    {
        
    }
}
