using UnityEngine;
using UnityEngine.UI;

public class UI_DuelStatusPanel : MonoBehaviour
{
    public Text text;
    public Text durationText;

    public Status status { get; set; }

    public void init(Status _status) {
        status = _status;
        if (status != null) {
            text.color = status.kind == BuffKind.Buff ? new Color32(15, 94, 29, 255) : new Color32(138, 22, 32, 255);
            text.text = status.name + " " + status.value;
            durationText.text = ""+status.duration;
        }
        gameObject.SetActive(status != null);
    } 

    void Start() {}

    
    void Update() {}
}
