using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleVisualizer : MonoBehaviour 
{
    public UI_DuelCirclePanel circlePanel1;
    public UI_DuelCirclePanel circlePanel2;

    public Text roundNumberText;

    private int lastRoundNumber;

    private BattleResult battleResult;

    // --------------

    private int roundNumber;                            // current round number
    private RoundResult roundResult;                    // current round result
    private UI_DuelCirclePanel circle { get; set; }     // current circle panel
    private CircleRotation circleR { get; set; }        // current circle rotation

    public void visualize(BattleResult _battleResult) {
        battleResult = _battleResult;
        roundNumber = 0;
        lastRoundNumber = battleResult.rounds.Count - 1;
        //foreach(RoundResult roundResult in battleResult.rounds) {
            //visualize(roundResult);
        //}
        
        circlePanel1.initHeroPanels(battleResult.heroPlaceCount1, battleResult.heroes1);
        circlePanel2.initHeroPanels(battleResult.heroPlaceCount2, battleResult.heroes2);


       StartCoroutine(startBattle());
    }

    private IEnumerator startBattle() {
        yield return new WaitForSeconds(1f);
        visualizeRound();
    }

    public void visualizeNextRound() {
        roundNumber++;
        if (roundNumber <= lastRoundNumber) {
            visualizeRound();
        }   
    }
    
    public void visualizeRound() {
        roundResult = battleResult.rounds[roundNumber];
        // VISUALIZE HERE
        roundNumberText.text = "ROUND " + (roundNumber + 1);
        Debug.Log("ROUND " + (roundNumber + 1));
        
        circle = roundResult.turn == 0 ? circlePanel1 : circlePanel2;
        circleR = roundResult.circleRotation;

        // STEPS: 
        // 1. rotate circle
        // 2. move hero to battle place
        // 3. execute skill
        // 4. execute default attack
        // 5. remove buffs
        // 6. go to next round


        // START FROM ROTATION AND GO ALL STEPS
        rotateCircle();

        if (circleR.isRotated) {
            float time = circleR.value * 0.6f; 
            StartCoroutine(waitAndToBattlePlace(time));
        } else {
            toBattlePlace();
        }

        // --------------------
        
    }

    private IEnumerator waitAndToBattlePlace(float time) {
        yield return new WaitForSeconds(time);
        toBattlePlace(); 
    }

    private IEnumerator waitAndExecuteActions(float time) {
        yield return new WaitForSeconds(time);
        executeActions(); 
    }

    private void rotateCircle() {
        // rotate circle
        if (circleR.isRotated) {
            circle.log("ROTATE " + circleR.value);
            circle.rotate(circleR.value);
        }
    }

    private void toBattlePlace() {
       // move to battle place
        if (circleR.isMovedToBattlePlace) {
            //circle.log("MOVE TO BATTLE");
            circle.moveToBattlePlace();
        }
        StartCoroutine(waitAndExecuteActions(0.8f));
    }

    private void executeActions() {
        executeAction(0);
    }

    private void updateHeroPanels() {
        // foreach(RoundAction action in roundResult.buffRemoveActions) {
        //     circle.executeBuffRemoveAction(action);
        // }

        circle.updateHeroPanels(roundResult.heroes);
        circle.enemyCircle.updateHeroPanels(roundResult.enemyHeroes);


        StartCoroutine(waitAndNextRound(0.8f));
    }

    private void executeAction(int actionIndex) {
        if (actionIndex < roundResult.actions.Count) {
            RoundAction action = roundResult.actions[actionIndex];
            if (action.circleId == CircleId.Ally) {
                circle.executeAction(action);
            } else {
                circle.enemyCircle.executeAction(action);
            }
            StartCoroutine(waitAndExecuteNextAction(1f, actionIndex));
        } else {
            //StartCoroutine(waitAndNextRound(0.8f));
            updateHeroPanels();
        }
    }

    private IEnumerator waitAndExecuteNextAction(float time, int actionIndex) {
        yield return new WaitForSeconds(time);
        actionIndex++;
        executeAction(actionIndex); 
    }

    IEnumerator waitAndNextRound(float time)
    {
        yield return new WaitForSeconds(time);
        visualizeNextRound(); 
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
