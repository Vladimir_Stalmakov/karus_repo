using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DuelHeroPanel : MonoBehaviour
{
    public Hero hero { get; set; }
    public Totem totem { get; set; }
    
    [Header("Managers")]
    public UI_DuelCirclePanel circlePanel;
    
    [Header("UI")]
    public Text nameText;
    public Text attrText;
    public Text actionText;
    public Text totemText;
    public List<UI_DuelBuffPanel> buffPanels;
    public GameObject buffsPanel;
    public List<UI_DuelStatusPanel> statusPanels;
    public GameObject statusesPanel;

    // -----------------

    private Color32 defaultColor { get; set; }

    public void init(Hero hero) {
        setHero(hero);
        actionText.text = "";
        initBuffPanels();
        initStatusPanels();
        gameObject.SetActive(true);
        defaultColor = actionText.color;
    }

    public void setHero(Hero _hero) {
        hero = _hero;
        refresh();
    }

    public void setTotem(Totem _totem) {
        totem = _totem;
        refresh();
    }

    public void initBuffPanels() {
        buffsPanel.gameObject.SetActive(hero != null);
        if (hero != null) {
            for(int i=0; i<buffPanels.Count; i++) {
                Buff buff = (i < hero.buffs.Count) ? hero.buffs[i] : null;
                buffPanels[i].init(buff);
            }
        }
    }

    public void initStatusPanels() {
        statusesPanel.gameObject.SetActive(hero != null);
        if (hero != null) {
            for(int i=0; i<statusPanels.Count; i++) {
                Status status = (i < hero.statuses.Count) ? hero.statuses[i] : null;
                statusPanels[i].init(status);
            }
        }
    }    

    public void refreshBuffPanels() {
        initBuffPanels();
    }

     public void refreshStatusPanels() {
        initStatusPanels();
    }

    public void executeAction(RoundAction action) {
        Debug.Log(">>>> ----- executeAction " + action.type + " by " + (hero == null ? "<null>" : hero.name));
        string text = "";
        
        switch (action.type) {
            case RoundActionType.Skill : {
                Debug.Log("skill name: " + action.skillName);
                text = action.skillName;
            }
            break;
            case RoundActionType.Heal : {
                text = "HEAL";
                circlePanel.executeHeal(action);
            }
            break;
            case RoundActionType.Buff : {
                Debug.Log("buff on " + action.targetHeroName);
                text = "BUFF " + action.skillName;
                circlePanel.executeBuff(action);
                // hero.addBuff(action.buff);
                // refreshBuffPanels();
            }
            break;
            case RoundActionType.Status : {
                Debug.Log("status on " + action.targetHeroName);
                text = "STATUS " + action.skillName;
                circlePanel.executeStatus(action);
            }
            break;
            case RoundActionType.DefaultAttack: {
                text = executeAttackAction(action);
                circlePanel.executeAttack(action);
            }
            break;
            case RoundActionType.AttackSkill: {
                text = executeAttackAction(action);
                circlePanel.executeAttack(action);
            }
            break;
            case RoundActionType.HeroDeath: {
                text = "DEAD";
                actionText.color = Color.black;
            }
            break;
            case RoundActionType.Summon: {
                text = "SUMMON";
                actionText.color = Color.green;
                setHero(action.summonHero);
                refresh();
            }
            break;
            case RoundActionType.SummonTotem: {
                text = "TOTEM";
                actionText.color = Color.green;
                setTotem(action.totem);
                refresh();
            }
            break;
            case RoundActionType.RemoveTotem: {
                text = "TOTEM REMOVED";
                actionText.color = Color.green;
                setTotem(null);
                refresh();
            }
            break;
            case RoundActionType.SwapHealth : {
                text = "SWAP HEATH";
                hero.changeAttribute(Attr.Health, action.fromHealth);
                circlePanel.executeSwapHealth(action);
            }
            break;
            default: {
                Debug.Log("ERROR! executeAction > no case found for type: " + (action.type));
                return;
            }
        }
        actionText.text = text;
        StartCoroutine(waitAndHideAction(0.8f));
    }

    public void handleAttack(RoundAction action) {
        string evadeText = action.isEvasion ? " EVADE!": ""; 
        actionText.text = action.damage + " DMG!";
        if (action.isEvasion) {
            actionText.color = Color.cyan;
        } else {
            actionText.color = Color.red;
        }
        hero.addAttribute(Attr.Health, -action.damage);
        refresh();
        StartCoroutine(waitAndHideAction(0.9f));
    }

    public void refresh() {
        if (hero != null) {
            nameText.text = hero.name;
            attrText.text = hero.health + " HP " + hero.mana + " MA\n" +
                            (hero.shield > 0 ? ("SH " + hero.shield) : " ") + 
                            (hero.armor != 0 ? ("AR " + hero.armor) : "") + "\n" +
                            "DMG " + hero.damage;
        } else {
            nameText.text = "";
            attrText.text = "";
        }

        if (totem != null) {
            totemText.text = totem.name;
        } else {
           totemText.text = ""; 
        }
        
        refreshBuffPanels();
        refreshStatusPanels();
    }

    private string executeAttackAction(RoundAction action) {
        string criticalText = action.isCritical ? " CRIT!" : "";
        string text = "ATTACK!" + criticalText;
        if (action.isCritical) {
            actionText.color = Color.yellow;
        }
        return text;
    }

    public void executeHealAction(RoundAction action) {
        actionText.text = "Healed " + action.value;
        hero.addAttribute(Attr.Health, action.value);
        refresh();
        StartCoroutine(waitAndHideAction(0.8f));
    }

    public void executeBuffAction(RoundAction action) {
        actionText.text = "Buffed!";
        hero.addBuff(action.buff);
        refresh();
        StartCoroutine(waitAndHideAction(0.8f));
    }

    public void executeStatusAction(RoundAction action) {
        actionText.text = "Statused!";
        hero.addStatus(action.status);
        refresh();
        StartCoroutine(waitAndHideAction(0.8f));
    }

    public void executeSwapHealth(RoundAction action) {
        hero.changeAttribute(Attr.Health, action.targetHealth);
        refresh();
    }

    ////////////////

    private IEnumerator waitAndHideAction(float time) {
        yield return new WaitForSeconds(time);
        hideAction();
        actionText.color = defaultColor;
    }

    private void hideAction() {
       actionText.text = "";
    }

    public void hide() {
        gameObject.SetActive(false);
    }


    void Start()
    {
        
    }

    void Update()
    {
        
    }
}
