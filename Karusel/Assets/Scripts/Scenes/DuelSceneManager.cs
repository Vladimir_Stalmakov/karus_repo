using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DuelSceneManager : MonoBehaviour
{
    private SessionManager sessionManager;
    private Player player;

    // UI

    public BattleVisualizer battleVisualizer;
    public Text logText;

    public Button textBattleButton;
    public Button cardBattleButton;

    public GameObject textBattlePanel;

    public void onSelectButtonClick() {
        sessionManager.changeToSelectScene();
    }
    
    void Start()
    {
        sessionManager = FindObjectOfType<SessionManager>();
        player = sessionManager.player;
        executeDuel();
    }

    void Update()
    {
        
    }

    public void executeDuel() {

         // 1. create 2 players with heroes
        // 2. move them in battle
        // 3. show results

        //Player player1 = player;
        // player1.heroCount = 3;
        // player1.heroPlaceCount = 3;
        
        //BasicHero hero11 = HeroFactory.createHero(BasicClass.Wild);
        //player1.addHero(hero11);

        //BasicHero hero12 = HeroFactory.createHero(BasicClass.Priest);
        //player1.addHero(hero12);

        //BasicHero hero13 = HeroFactory.createHero(BasicClass.Magic);
        //player1.addHero(hero13);
       
        // ------------------------------------------

        //

        //Debug.Log("DUEL: opponent for round " + sessionManager.roundNumber);
        //Player player2 = OpponentManager.getPlayer(sessionManager.roundNumber);
        //Player player2 = sessionManager.players[player.opponentIndex];
        //Debug.Log("DUEL: opponent for round " + sessionManager.players[player.opponentIndex].name);

        // Player player2 = new Player();
        // player2.heroCount = 0;
        // player2.heroPlaceCount = player1.heroPlaceCount;

        // for (int i=0; i<player1.heroCount; i++) {
        //     float random = Random.Range(0.0f, 0.9f);
        //     int heroClassNumber = (int)(random * 10);
        //     BasicHero hero = HeroFactory.createHero((BasicClass)heroClassNumber);
        //     player2.addHero(hero);
        // }

        //

        // BasicHero hero21 = HeroFactory.createHero(BasicClass.Master);
        // player2.addHero(hero21);

        //BasicHero hero22 = HeroFactory.createHero(BasicClass.Order);
        //player2.addHero(hero22);

        //BasicHero hero23 = HeroFactory.createHero(BasicClass.Master);
        //player2.addHero(hero23);

        // all duels
        BattleResult playerBattleResult = null;
        List<Player> players = sessionManager.players;
        for(int i=0; i<players.Count; i++) {
            if (players[i].opponentIndex != -1) {
                Player p1 = players[i];
                Player p2 = players[players[i].opponentIndex];
                p1.opponentIndex = -1;
                p2.opponentIndex = -1;
                BattleResult battleResult = executeBattle(p1, p2);
                if (i == 0) {
                    playerBattleResult = battleResult;
                }
            }
        }

        // ------------------------------------------

        //BattleManager.setPlayers(player1, player2);
        //BattleResult battleResult = BattleManager.executeBattle();

        // if (battleResult.playerWins == 2) { // enemy wins
        //     player.health -= battleResult.healthLost;
        // } else if (battleResult.playerWins == 1) { // player wins
        //     player2.health -= battleResult.healthLost;
        // } 
        
        // BATTLE IN VISUAL

        battleVisualizer.visualize(playerBattleResult);

        // BATTLE IN TEXT

        string battleDescr = "";
        battleDescr += "BATTLE STARTED:\n\n" + playerBattleResult.descr + "";
        foreach(RoundResult roundResult in playerBattleResult.rounds) {
             battleDescr += "\n\n" + roundResult.descr;
        }
        logText.text = battleDescr;
    }

    public void onTextBattleButtonClick() {
        textBattleButton.gameObject.SetActive(false);
        cardBattleButton.gameObject.SetActive(true);

        battleVisualizer.gameObject.SetActive(false);
        textBattlePanel.gameObject.SetActive(true);
    }

    public void onCardBattleButtonClick() {
        textBattleButton.gameObject.SetActive(true);
        cardBattleButton.gameObject.SetActive(false);

        battleVisualizer.gameObject.SetActive(true);
        textBattlePanel.gameObject.SetActive(false);
    }

    private BattleResult executeBattle(Player player1, Player player2) {
        Debug.Log("executeBattle");
        Debug.Log("HEROES1: "); 
        for(int i=0; i<player1.heroCount; i++) {
           Debug.Log(player1.heroes[i].name); 
        }
        Debug.Log("VS HEROES2: "); 
        for(int i=0; i<player2.heroCount; i++) {
           Debug.Log(player2.heroes[i].name); 
        }
        BattleManager.setPlayers(player1, player2);
        BattleResult battleResult = BattleManager.executeBattle();
        if (battleResult.playerWins == 2) { // player2 wins
            player1.health -= battleResult.healthLost;
        } else if (battleResult.playerWins == 1) { // player1 wins
            player2.health -= battleResult.healthLost;
        }
        return battleResult;
    }
}
